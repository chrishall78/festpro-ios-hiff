//
//  BrowseFilmsViewController.h
//  FestPro
//
//  Created by Christopher Hall on 1/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "FilterMenuViewController.h"
#import "BrowseFilmsCell.h"

@interface BrowseFilmsViewController : UITableViewController <NSFetchedResultsControllerDelegate, FilterMenuViewControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UIView *filterView;
@property (strong, nonatomic) NSArray *filmProgram;
@property (strong, nonatomic) NSString *filmProgramName;
@property (strong, nonatomic) NSString *filmSectionName;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *filterButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (strong, nonatomic) NSArray *imageURLs;
@property (strong, nonatomic) NSMutableArray *imageUrlBySection;
@property (strong, nonatomic) NSMutableArray *imageUrlList;


- (NSString *)processTitle:(NSString *)title;
- (void)configureCell:(BrowseFilmsCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (NSMutableArray *)setImageUrlBySection;
- (void)addOrRemoveFilterView;

@end
