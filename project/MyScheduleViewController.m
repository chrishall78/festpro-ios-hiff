//
//  MyScheduleViewController.m
//  FestPro
//
//  Created by Christopher Hall on 1/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "MyScheduleViewController.h"
#import "FPNavigationController.h"
#import "FilmDetailViewController.h"
#import "HomeViewController.h"
#import "BrowseFilmsViewController.h"
#import "Screenings.h"
#import "CustomColoredAccessory.h"

@implementation MyScheduleViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Gesture actions

- (IBAction)moveRightTab:(id)sender
{
    NSUInteger selectedIndex = [self.tabBarController selectedIndex];
    [self.tabBarController setSelectedIndex:selectedIndex - 3];
}

- (IBAction)moveLeftTab:(id)sender
{
    NSUInteger selectedIndex = [self.tabBarController selectedIndex];
    [self.tabBarController setSelectedIndex:selectedIndex - 1];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
		[self.tableView setSeparatorInset:UIEdgeInsetsZero];
	}
    self.fetchedResultsController.delegate = self;
    
	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeftTab:)];
		[swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
		[self.view addGestureRecognizer:swipeLeft];
		
		UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveRightTab:)];
		[swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
		[self.view addGestureRecognizer:swipeRight];
	}
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.fetchedResultsController = nil;
    self.managedObjectContext = nil;
    self.myFestArray = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.fetchedResultsController = nil;
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	     */
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    [self.tableView reloadData];    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return YES;
	} else {
		return NO;
	}
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return (UIInterfaceOrientationMaskPortrait |
				UIInterfaceOrientationMaskLandscapeLeft |
				UIInterfaceOrientationMaskLandscapeRight);
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	[super preferredInterfaceOrientationForPresentation];
	return UIInterfaceOrientationPortrait;
}


#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"scheduleCell";
    
    MyScheduleCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MyScheduleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];

	// kill insets for iOS 8
	if ([[UIDevice currentDevice].systemVersion floatValue] >= 8) {
		cell.preservesSuperviewLayoutMargins = NO;
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
	// iOS 7 and earlier
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}

    return cell;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setBackgroundColor:UI_COLOR_05];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSInteger count = [[self.fetchedResultsController sections] count];
	if (count == 0) { count = 1; }
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
	
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    
    return numberOfRows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	// Display the dates of the films as section headings.
	if (self.fetchedResultsController.sections.count > 0) {
		id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
		return [sectionInfo name];
	} else {
        return nil;
    }
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Remove the screening id from the MyFest Array
        if (self.fetchedResultsController.sections.count > 0) {
            id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:indexPath.section];
            Screenings *thisScreening = [[sectionInfo objects] objectAtIndex:indexPath.row];
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSMutableArray *myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];         
            NSMutableArray *myNewFestArray = [[NSMutableArray alloc] init];         
            for (NSNumber *screening_id in myFestArray) {
                if (![screening_id isEqualToNumber:thisScreening.screening_id]) {
                    [myNewFestArray addObject:screening_id];
                }
            }
            
            NSArray *myFestArrayPrefs = myNewFestArray;
            [prefs setObject:myFestArrayPrefs forKey:@"myFestScreenings"];
            [prefs synchronize];
        }
        
        _fetchedResultsController = nil;
        
        NSError *error;
        if (![[self fetchedResultsController] performFetch:&error]) {
            // Error handling
        }
        
        [self.tableView reloadData];
        
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


#pragma mark - Table view delegate

- (void)configureCell:(MyScheduleCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Screenings *currentCell = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    _filmProgram = [[NSArray alloc] init];
    _filmProgram = [[currentCell film_ids] componentsSeparatedByString:@","];
    
    if([_filmProgram count] > 1) {
        // Distinguish programs of films from single films
		[cell setAccessoryType:UITableViewCellAccessoryNone];
		CustomColoredAccessory *accessory = [CustomColoredAccessory accessoryWithColor:UI_COLOR_01];
		accessory.highlightedColor = UI_COLOR_03;
		cell.accessoryView = accessory;
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        CustomColoredAccessory *accessory = [CustomColoredAccessory accessoryWithColor:UI_COLOR_03];
        accessory.highlightedColor = UI_COLOR_01;
        cell.accessoryView = accessory;
    }
    
    if ([[currentCell programName] isEqualToString:@""]) {
        cell.filmTitleLabel.text = [currentCell films];
    } else {
        cell.filmTitleLabel.text = [currentCell programName];
    }
    cell.venueLabel.text = [currentCell location];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *currentTimeZone = [NSTimeZone timeZoneWithName: kBaseTimezone];
    [dateFormatter setTimeZone:currentTimeZone];
    [dateFormatter setDateFormat: @"h:mm a"];
    cell.timeLabel.text = [dateFormatter stringFromDate:[currentCell dateTime]];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect myRect = CGRectMake(0.0, 0.0, 320.0, 22.0);
    CGRect myLabelRect = CGRectMake(10.0, 0.0, 300.0, 22.0);
    
    UIView *headerView = [[UIView alloc] initWithFrame:myRect];
    [headerView setBackgroundColor:UI_COLOR_02];
    
    UILabel * label1 = [[UILabel alloc] initWithFrame:myLabelRect];
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = UI_COLOR_05;
    
	if (self.fetchedResultsController.sections.count > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        NSString *rawDateStr = [sectionInfo name];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:kBaseTimezone]];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZ"];
        NSDate *date = [formatter dateFromString:rawDateStr];
        [formatter setDateFormat:@"EEEE, MMMM d, yyyy"];
        
        label1.text = [formatter stringFromDate:date];
        label1.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        [headerView addSubview:label1];
    } else {
        [headerView setFrame:CGRectMake(0.0, 0.0, 320.0, 66.0)];
        [label1 setFrame:CGRectMake(10.0, 0.0, 300.0, 66.0)];
        label1.text = @"No MyFest entries. Tap a star from the Schedule tab or any film detail page to build your personal festival schedule.";
        label1.font = [UIFont fontWithName:@"Helvetica" size:14];
        label1.lineBreakMode = NSLineBreakByWordWrapping;
        label1.numberOfLines = 0;
        [headerView addSubview:label1];
    }
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if (self.fetchedResultsController.sections.count > 0) {
        return 22.0;
    } else {
        return 66.0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    Screenings *currentCell = [self.fetchedResultsController objectAtIndexPath:indexPath];
    _filmProgram = [[NSArray alloc] init];
    _filmProgram = [[currentCell film_ids] componentsSeparatedByString:@","];
    
    if ([[currentCell programName] isEqualToString:@""]) {
        _filmProgramName = [currentCell films];
    } else {
        _filmProgramName = [currentCell programName];
    }

    // Send user to Film Detail if it is a single film, or to Browse Films if there are multiple.
    if([_filmProgram count] > 1) {
        [self performSegueWithIdentifier: @"FilmProgram" sender:self];
    } else {
        [self performSegueWithIdentifier: @"FilmDetail" sender:self];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {    
    Screenings *currentCell = [self.fetchedResultsController objectAtIndexPath:indexPath];
    _filmProgram = [[NSArray alloc] init];
    _filmProgram = [[currentCell film_ids] componentsSeparatedByString:@","];
    
    if ([[currentCell programName] isEqualToString:@""]) {
        _filmProgramName = [currentCell films];
    } else {
        _filmProgramName = [currentCell programName];
    }
    
    // Send user to Film Detail if it is a single film, or to Browse Films if there are multiple.
    if([_filmProgram count] > 1) {
        [self performSegueWithIdentifier: @"FilmProgram" sender:self];
    } else {
        [self performSegueWithIdentifier: @"FilmDetail" sender:self];
    }
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Screenings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateTime" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Add a Predicate to filter the schedule results to only the screening ids in the current MyFest array.
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    _myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"screening_id IN(%@)", _myFestArray];
    [fetchRequest setPredicate:predicate];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"dateString" cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	     */
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    
    return _fetchedResultsController;
}

#pragma mark - Fetched results controller delegate methods

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

		default:
			break;
    }
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(MyScheduleCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{    
    if ([[segue identifier] isEqualToString:@"FilmDetail"]) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            FPNavigationController *navigationController = segue.destinationViewController;
            FilmDetailViewController *filmDetail = [[navigationController viewControllers] objectAtIndex:0];
            filmDetail.managedObjectContext = _managedObjectContext;
            Screenings *curScreening = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
            NSSet *screeningFilms = [curScreening screenings_films];
            filmDetail.currentFilm = [[screeningFilms allObjects] objectAtIndex:0];

			HomeViewController *homeViewController;
			if ([[self.splitViewController.viewControllers lastObject] isKindOfClass:[UINavigationController class]]) {
				homeViewController = (HomeViewController *) ((UINavigationController *)[self.splitViewController.viewControllers lastObject]).topViewController;
			} else {
				homeViewController = [self.splitViewController.viewControllers lastObject];
			}
			if (homeViewController.masterPopoverController != nil) {
				[homeViewController.masterPopoverController dismissPopoverAnimated:YES];
			}
			if ([filmDetail conformsToProtocol:@protocol(UISplitViewControllerDelegate)]) {
                self.splitViewController.delegate = filmDetail;
				filmDetail.popoverButton = homeViewController.popoverButton;
				filmDetail.masterPopoverController = homeViewController.masterPopoverController;
            } else {
                self.splitViewController.delegate = nil;
            }
        } else {
            FilmDetailViewController *filmDetail = (FilmDetailViewController *)[segue destinationViewController];
            filmDetail.managedObjectContext = _managedObjectContext;
            Screenings *curScreening = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
            NSSet *screeningFilms = [curScreening screenings_films];
            filmDetail.currentFilm = [[screeningFilms allObjects] objectAtIndex:0];
        }
    }    
    
    if ([[segue identifier] isEqualToString:@"FilmProgram"]) {
		BrowseFilmsViewController *browseFilms = (BrowseFilmsViewController *)[segue destinationViewController];
		browseFilms.managedObjectContext = _managedObjectContext;
		browseFilms.filmProgram = _filmProgram;
		browseFilms.filmProgramName = _filmProgramName;
    }
}

- (void)showFilmProgram:(id)sender {
    [self performSegueWithIdentifier: @"FilmProgram" sender:self];
}

@end
