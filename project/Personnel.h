//
//  Personnel.h
//  FestPro
//
//  Created by Christopher Hall on 2/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Films;

@interface Personnel : NSManagedObject

@property (nonatomic, retain) NSNumber * film_id;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * role;
@property (nonatomic, retain) Films *personnel_films;

@end
