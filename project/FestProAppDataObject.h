//
//  FestProAppDataObject.h
//  FestPro
//
//  Created by Christopher Hall on 3/26/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDataObject.h"

@interface FestProAppDataObject : AppDataObject 

@property (strong, nonatomic) NSString *sectionFilterValue;
@property (strong, nonatomic) NSString *countryFilterValue;
@property (strong, nonatomic) NSString *languageFilterValue;
@property (strong, nonatomic) NSString *genreFilterValue;
@property (strong, nonatomic) NSString *eventTypeFilterValue;

@end
