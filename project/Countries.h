//
//  Countries.h
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Films;

@interface Countries : NSManagedObject

@property (nonatomic, retain) NSNumber * countryId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *countries_films;

@end

@interface Countries (CoreDataGeneratedAccessors)

- (void)addCountries_filmsObject:(Films *)value;
- (void)removeCountries_filmsObject:(Films *)value;
- (void)addCountries_films:(NSSet *)values;
- (void)removeCountries_films:(NSSet *)values;

@end
