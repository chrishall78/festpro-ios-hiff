//
//  FilterMenuViewController.h
//  FestPro
//
//  Created by Christopher Hall on 1/19/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

@class FilterMenuViewController;

@protocol FilterMenuViewControllerDelegate <NSObject>

- (void)filterMenuViewControllerDidCancel:(FilterMenuViewController *)controller;
- (void)filterMenuViewControllerDidSave:(FilterMenuViewController *)controller;

@end

@interface FilterMenuViewController : UITableViewController

@property (nonatomic, weak) id <FilterMenuViewControllerDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, assign) NSInteger sectionSelIndex;
@property (nonatomic, assign) NSInteger eventTypeSelIndex;
@property (nonatomic, assign) NSInteger countrySelIndex;
@property (nonatomic, assign) NSInteger languageSelIndex;
@property (nonatomic, assign) NSInteger genreSelIndex;

@property (nonatomic, strong) NSArray *sectionArray;
@property (nonatomic, strong) NSArray *eventTypeArray;
@property (nonatomic, strong) NSArray *countryArray;
@property (nonatomic, strong) NSArray *languageArray;
@property (nonatomic, strong) NSArray *genreArray;

@property (nonatomic, strong) IBOutlet UILabel *sectionLabel;
@property (nonatomic, strong) IBOutlet UILabel *eventTypeLabel;
@property (nonatomic, strong) IBOutlet UILabel *countryLabel;
@property (nonatomic, strong) IBOutlet UILabel *languageLabel;
@property (nonatomic, strong) IBOutlet UILabel *genreLabel;

- (IBAction)selectFilter:(UITableViewCell *)sender withIndexPath:(NSInteger)indexPath andTitle:(NSString *)title andRows:(NSArray *)array andSelIndex: (NSInteger)index;
- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end
