//
//  SectionViewController.h
//  FestPro
//
//  Created by Christopher Hall on 6/11/14.
//  Copyright (c) 2014 Christopher Hall Design. All rights reserved.
//

#import "MPParallaxLayout.h"
#import "MPParallaxCollectionViewCell.h"

@interface SectionViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout, UICollectionViewDelegate,UICollectionViewDataSource, UIScrollViewDelegate, MPParallaxCellDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) IBOutlet UICollectionView *sectionCollectionView;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSArray *sectionList;
@property (strong, nonatomic) NSDictionary *sectionDict;
@property (assign, nonatomic) CGRect labelFrame;


@end
