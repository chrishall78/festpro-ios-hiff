//
//  AppDelegate.h
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "AppDelegateProtocol.h"

@class FestProAppDataObject;

@interface AppDelegate : UIResponder <UIApplicationDelegate, AppDelegateProtocol>

extern NSString *const FBSessionStateChangedNotification;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UINavigationController *homeController;
@property (nonatomic, retain) FestProAppDataObject *theAppDataObject;
@property (nonatomic, assign) BOOL fullScreenVideoIsPlaying;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

