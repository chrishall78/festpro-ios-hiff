//
//  GridPopoverContentViewController.h
//  hiff2012
//
//  Created by Christopher Hall on 6/3/14.
//  Copyright (c) 2014 Christopher Hall Design. All rights reserved.
//

#import "Films.h"
#import "Screenings.h"

@interface GridPopoverContentViewController : UIViewController <UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) Films *currentFilm;
@property (strong, nonatomic) Screenings *currentScreening;
@property (strong, nonatomic) NSString *ticketLink;
@property (assign, nonatomic) CGSize finalViewSize;

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *filmName;
@property (strong, nonatomic) IBOutlet UILabel *filmEventType;
@property (strong, nonatomic) IBOutlet UILabel *filmInformation;
@property (strong, nonatomic) IBOutlet UIWebView *synopsisView;
@property (strong, nonatomic) IBOutlet UILabel *screeningDateTime;
@property (strong, nonatomic) IBOutlet UIView *buttonsContainer;
@property (strong, nonatomic) IBOutlet UIImageView *myFestButton;
@property (strong, nonatomic) IBOutlet UIImageView *buyTicketsButton;
@property (strong, nonatomic) IBOutlet UIImageView *shoppingCartButton;

- (IBAction)toggleMyFest:(id)sender;
- (NSString *)processTitle:(NSString *)title;
- (IBAction)openWebView:(id)sender;
- (void) dismissWebView;

@end
