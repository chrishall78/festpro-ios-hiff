//
//  CustomColoredAccessory.h
//  HIFF 2012
//
//  Created by Christopher Hall on 4/2/12.
//  Copyright (c) 2012 Christopher Hall Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomColoredAccessory : UIControl
{
	UIColor *_accessoryColor;
	UIColor *_highlightedColor;
}

@property (nonatomic, retain) UIColor *accessoryColor;
@property (nonatomic, retain) UIColor *highlightedColor;

+ (CustomColoredAccessory *)accessoryWithColor:(UIColor *)color;

@end