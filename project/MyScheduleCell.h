//
//  MyScheduleCell.h
//  FestPro
//
//  Created by Christopher Hall on 3/11/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyScheduleCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *filmTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *venueLabel;
@property (nonatomic, strong) IBOutlet UILabel *timeLabel;

@end
