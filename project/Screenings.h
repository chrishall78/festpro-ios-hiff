//
//  Screenings.h
//  FestPro
//
//  Created by Christopher Hall on 2/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Films;

@interface Screenings : NSManagedObject

@property (nonatomic, retain) NSDate * dateString;
@property (nonatomic, retain) NSDate * dateTime;
@property (nonatomic, retain) NSNumber * film_id;
@property (nonatomic, retain) NSString * film_ids;
@property (nonatomic, retain) NSString * films;
@property (nonatomic, retain) NSNumber * free;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSNumber * private;
@property (nonatomic, retain) NSString * programName;
@property (nonatomic, retain) NSNumber * rush;
@property (nonatomic, retain) NSNumber * screening_id;
@property (nonatomic, retain) NSString * ticketLink;
@property (nonatomic, retain) NSNumber * totalRuntime;
@property (nonatomic, retain) NSSet *screenings_films;

@end

@interface Screenings (CoreDataGeneratedAccessors)

- (void)addScreenings_filmsObject:(Films *)value;
- (void)removeScreenings_filmsObject:(Films *)value;
- (void)addScreenings_films:(NSSet *)values;
- (void)removeScreenings_films:(NSSet *)values;

@end
