//
//  ScheduleCell.h
//  FestPro
//
//  Created by Christopher Hall on 1/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *filmTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *venueLabel;
@property (nonatomic, strong) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) IBOutlet UIButton *myFestButton;

@end
