//
//  Sponsors.m
//  FestPro
//
//  Created by Christopher Hall on 2/19/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Sponsors.h"
#import "Films.h"


@implementation Sponsors

@dynamic film_id;
@dynamic sponsorName;
@dynamic sponsorDesc;
@dynamic sponsorSiteUrl;
@dynamic sponsorLogoUrl;
@dynamic sponsors_films;


@end
