//
//  PhotoVideo.h
//  FestPro
//
//  Created by Christopher Hall on 2/19/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Films;

@interface PhotoVideo : NSManagedObject

@property (nonatomic, retain) NSNumber * film_id;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * videoUrl;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSString * videoService;
@property (nonatomic, retain) NSString * photoSmallUrl;
@property (nonatomic, retain) NSString * photoLargeUrl;
@property (nonatomic, retain) NSString * photoXLargeUrl;
@property (nonatomic, retain) Films *photos_films;

@end
