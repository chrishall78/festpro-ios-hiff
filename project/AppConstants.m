//
//  AppConstants.m
//  FestPro
//
//  Created by Christopher Hall on 2/6/14.
//  Copyright (c) 2014 FestPro LLC. All rights reserved.
//

#import "AppConstants.h"

NSString* const kBaseDataURL = @"http://program.hiff.org/api/v2/2016_hiff_fall_festival/";
NSString* const kBaseWebURL = @"http://program.hiff.org";
NSString* const kBaseDefaultPhoto = @"http://program.hiff.org/assets/images/2016_hiff_fall_festival/001-default-photo.jpg";
NSString* const kBaseTimezone = @"Pacific/Honolulu";
NSString* const kShoppingCartURL = @"http://hiff.tix.com/Account.asp";

NSString* const kiPhoneDetailsStyles = @"<style type='text/css'> HTML { background-color:#FFFFFF; color:#2A1C48; font-family: Helvetica, Arial, sans-serif; font-size: 0.9em; } a { text-decoration:none; } .table { display:table; } .row { display: table-row; } .row div { display: table-cell; width:50%; text-align:center; vertical-align:top; } img.sponsorLogo { float:left; margin: 10px 10px 0 0; } h3 { font-size: 16px; margin: 15px 0 0; color:#F4823D; text-transform:uppercase; border-bottom: 1px solid #000000; } h4 { font-size: 14px; border-bottom: 1px solid #000000; margin: 15px 0 0; color:#F4823D; text-transform:uppercase; } p { margin: 0; } p.title { font-size: 16px; font-weight:bold; color:#F4823D; } p.personnel { margin: 5px 0; } p.personnel span { font-weight:bold; color: #000000; } </style>";
NSString* const kiPhoneSynopsisStyles = @"<style type='text/css'> HTML { background-color:#FFFFFF; color:#2A1C48; font-family: Helvetica, Arial, sans-serif; font-size: 0.9em; } a { color: #000000; } h3 { font-size: 16px; margin: 0 0 10px 0; color:#F4823D; text-transform:uppercase; border-bottom: 1px solid #000000; } h4 { font-size: 14px; border-bottom: 1px solid #F4823D; margin: 15px 0 0; color:#FA58CE; text-transform:uppercase; } </style>";
NSString* const kiPhoneScreeningStyles = @"<style type='text/css'> HTML { background-color:#FFFFFF; color:#2A1C48; font-family: Helvetica, Arial, sans-serif; font-size: 0.9em; } h3 { font-size: 16px; margin: 0 0 10px 0; color:#F4823D; text-transform:uppercase; border-bottom: 1px solid #000000; } h4 { font-size: 14px; margin: 0; color:#000000; text-transform:uppercase; } p { margin: 0; } div.btn { width: 100px; height: 30px; margin-right:10px; float:left; background-size: 100px 30px; -webkit-background-size:100px 30px; } div.btn a { display:block; width: 100px; height:30px; text-indent: -999px; background-size: 100px 30px; -webkit-background-size:100px 30px; } .add-myfest { background: url('btn-add-myfest.png') no-repeat scroll 0 0 transparent; } .rem-myfest { background: url('btn-rem-myfest.png') no-repeat scroll 0 0 transparent; } .buy-tickets { background: url('btn-buy-tickets.png') no-repeat scroll 0 0 transparent; } .free-scrn { background: url('btn-free-scrn.png') no-repeat scroll 0 0 transparent; } .standby-line { background: url('btn-standby-line.png') no-repeat scroll 0 0 transparent; } .shop-cart { background: url('btn-shop-cart.png') no-repeat scroll 0 0 transparent; float: none !important; width:100%; margin-top:10px; } .clearfix:after, .clearfix:before { content: ' '; display: table; } .clearfix:after { clear: both; } @media screen and (-webkit-min-device-pixel-ratio: 1.5) { div.btn { width: 100px; height: 30px; margin-right:10px; float:left; background-size: 100px 30px; -webkit-background-size:100px 30px; } div.btn a { display:block; width: 100px; height:30px; text-indent: -999px; background-size: 100px 30px; -webkit-background-size:100px 30px; } .add-myfest { background: url('btn-add-myfest@2x.png') no-repeat scroll 0 0 transparent; } .rem-myfest { background: url('btn-rem-myfest@2x.png') no-repeat scroll 0 0 transparent; } .buy-tickets { background: url('btn-buy-tickets@2x.png') no-repeat scroll 0 0 transparent; } .free-scrn { background: url('btn-free-scrn@2x.png') no-repeat scroll 0 0 transparent; } .standby-line { background: url('btn-standby-line@2x.png') no-repeat scroll 0 0 transparent; } .shop-cart { background: url('btn-shop-cart@2x.png') no-repeat scroll 0 0 transparent; float: none !important; width:100%; margin-top:10px; } } </style><script> function toggleMyFest(elem) { elem.parentNode.classList.toggle('add-myfest'); elem.parentNode.classList.toggle('rem-myfest'); }</script>";
NSString* const kiPadDetailsStyles = @"<style type='text/css'> </style>";
NSString* const kiPadSynopsisStyles = @"<style type='text/css'> </style>";
NSString* const kiPadScreeningStyles = @"<style type='text/css'> </style>";

@implementation AppConstants

@end
