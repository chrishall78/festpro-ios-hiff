//
//  Personnel.m
//  FestPro
//
//  Created by Christopher Hall on 2/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Personnel.h"
#import "Films.h"

@implementation Personnel

@dynamic film_id;
@dynamic firstName;
@dynamic lastName;
@dynamic role;
@dynamic personnel_films;

@end
