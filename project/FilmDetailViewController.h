//
//  FilmDetailViewController.h
//  FestPro
//
//  Created by Christopher Hall on 1/30/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <Twitter/Twitter.h>
#import "Films.h"
#import "Screenings.h"

@interface FilmDetailViewController : UIViewController <UIScrollViewDelegate, MFMailComposeViewControllerDelegate, NSFetchedResultsControllerDelegate, UIWebViewDelegate, UISplitViewControllerDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *popoverButton;

@property (strong, nonatomic) NSSet *screenings;
@property (strong, nonatomic) Films *currentFilm;
@property (strong, nonatomic) NSMutableDictionary *currentScreenings;
@property (strong, nonatomic) NSMutableDictionary *currentPersonnel;
@property (strong, nonatomic) NSMutableDictionary *currentPhotos;
@property (strong, nonatomic) NSNumber *filmId;
@property (strong, nonatomic) NSMutableData *receivedData;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (strong, nonatomic) IBOutlet UIWebView *trailerView;
@property (strong, nonatomic) IBOutlet UIPageControl *imagePageControl;
@property (strong, nonatomic) IBOutlet UIView *pageControlView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *shareButton;

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImage *firstPhoto;
@property (strong, nonatomic) IBOutlet UIWebView *detailsView;
@property (strong, nonatomic) IBOutlet UIWebView *synopsisView;
@property (strong, nonatomic) IBOutlet UIWebView *screeningView;
@property (strong, nonatomic) UIStoryboardSegue *ticketsSegue;
@property (strong, nonatomic) NSString *ticketsUrl;
@property (strong, nonatomic) NSString *videoUrl;
@property (assign, nonatomic) BOOL hasVideo;
@property (assign, nonatomic) BOOL imagePageControlBeingUsed;

//- (void)retrieveFirstPhoto:(NSNotification *)notification;
- (IBAction)openSplitViewPopoverController:(id)sender;
- (IBAction)changePage:(id)sender;
- (NSString *)processTitle:(NSString *)title;
- (IBAction)openShareView:(id)sender;
- (void)changeSubview:(id)sender;
- (IBAction)launchEmailView;
- (void)videoStarted:(NSNotification *)notification;
- (void)videoFinished:(NSNotification *)notification;

@end
