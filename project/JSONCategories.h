//
//  JSONCategories.h
//  FestPro
//
//  Created by Christopher Hall on 1/23/12.
//  Copyright (c) 2012 Christopher Hall Design. All rights reserved.
//

@interface NSDictionary(JSONCategories)
+(NSDictionary*)dictionaryWithContentsOfJSONURLString: (NSString*)urlAddress;
-(NSData*)toJSON;
@end

@interface NSMutableDictionary(JSONCategories)
+(NSMutableDictionary*)dictionaryWithContentsOfJSONURLString: (NSString*)urlAddress;
-(NSData*)toJSON;
@end
