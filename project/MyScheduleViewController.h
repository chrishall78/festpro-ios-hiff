//
//  MyScheduleViewController.h
//  FestPro
//
//  Created by Christopher Hall on 1/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "MyScheduleCell.h"

@interface MyScheduleViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (strong, nonatomic) NSArray *filmProgram;
@property (strong, nonatomic) NSString *filmProgramName;
@property (strong, nonatomic) NSMutableArray *myFestArray;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;

- (void)configureCell:(MyScheduleCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (void)showFilmProgram:(id)sender;

@end