//
//  SectionViewController.m
//  FestPro
//
//  Created by Christopher Hall on 6/11/14.
//  Copyright (c) 2014 Christopher Hall Design. All rights reserved.
//

#import "SectionViewController.h"
#import "BrowseFilmsViewController.h"
#import "Films.h"
#import "PhotoVideo.h"
#import "UIImageView+AFNetworking.h"

static NSString *kCell = @"sectionCell";

@interface SectionViewController ()

@end

@implementation SectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.fetchedResultsController.delegate = self;
	_labelFrame = CGRectNull;
	
    //MPParallaxLayout *layout = [[MPParallaxLayout alloc] init];
    //_sectionCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    _sectionCollectionView.delegate = self;
    _sectionCollectionView.dataSource = self;
    [_sectionCollectionView registerClass: [MPParallaxCollectionViewCell class] forCellWithReuseIdentifier: kCell];

	// Remove the "ALL" value at index 0 from our section list before proceeding
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSMutableArray *tempSectionList = [[prefs arrayForKey:@"sectionList"] mutableCopy];
	if ([[tempSectionList objectAtIndex:0] isEqualToString:@"ALL"]) {
		[tempSectionList removeObjectAtIndex:0];
	}
	_sectionList = tempSectionList;

    self.fetchedResultsController = nil;
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	     */
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
	[self.collectionView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	// Find image URLs for each film in each section, then choose a random one to display
	NSArray *results = [_fetchedResultsController fetchedObjects];
	NSMutableDictionary *sectionPhotos = [NSMutableDictionary dictionary];
	NSMutableArray *sectionArray = [[NSMutableArray alloc] init];
	
	for (NSString *thisSection in _sectionList) {
		[sectionArray removeAllObjects];
		for (Films *thisFilm in results) {
			if ([[thisFilm section] isEqualToString:thisSection]) {
				NSSet *filmPhotos = [thisFilm films_photos];
				for (PhotoVideo *thisPhoto in filmPhotos) {
					// Get Extra Large photo, fall back to Large or Small if they do not exist
					NSString *photoURL = [thisPhoto photoXLargeUrl];
					if ([photoURL isEqualToString:@""]) { photoURL = [thisPhoto photoLargeUrl]; }
					if ([photoURL isEqualToString:@""]) { photoURL = [thisPhoto photoSmallUrl]; }
					if (photoURL != nil) {
						[sectionArray addObject:photoURL];
					}
				}
			}
		}
		[sectionPhotos setObject:[[NSArray alloc] initWithArray:sectionArray] forKey:thisSection];
	}
	_sectionDict = (NSDictionary *)sectionPhotos;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return YES;
	} else {
		return NO;
	}
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return (UIInterfaceOrientationMaskPortrait |
				UIInterfaceOrientationMaskLandscapeLeft |
				UIInterfaceOrientationMaskLandscapeRight);
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	[super preferredInterfaceOrientationForPresentation];
	return UIInterfaceOrientationPortrait;
}

#pragma mark - UICollectionView delegate / datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_sectionList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MPParallaxCollectionViewCell *cell = (MPParallaxCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCell forIndexPath:indexPath];
	
	if (CGRectIsNull(_labelFrame)) { _labelFrame = cell.textLabel.frame; }
	
	NSString *sectionName = _sectionList[indexPath.row];
	NSArray *sectionArray = _sectionDict[sectionName];

	//set placeholder image or cell won't update when image is loaded
	[cell.imageView setImage:[UIImage imageNamed:@"default-photo.png"]];
	
	if ([sectionArray count] > 0) {
		NSUInteger randomIndex = arc4random_uniform((uint32_t)[sectionArray count]);

		//load the image
		NSString *filmUrl = sectionArray[randomIndex];
		NSURLComponents *urlString = [[NSURLComponents alloc] initWithString: filmUrl];
		if (urlString.URL) {
			[cell.imageView setImageWithURL: urlString.URL];
		} else {
			NSString *encodedString = [filmUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			NSURL *encodedURL = [NSURL URLWithString:encodedString];
			if (encodedURL) {
				[cell.imageView setImageWithURL: encodedURL];
			} else {
				NSLog(@"'%@' is not a valid URL", filmUrl);
			}
		}
	}

	cell.textLabel.frame = _labelFrame;
	cell.textLabel.text = sectionName;
	cell.textLabel.textColor = [UIColor whiteColor];
	cell.textLabel.shadowColor = [UIColor blackColor];
	cell.textLabel.shadowOffset = CGSizeMake(1.0, 2.0);
	[cell.textLabel sizeToFit];
	cell.delegate = self;

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	_selectedIndexPath = indexPath;
	[self performSegueWithIdentifier:@"FilmSections" sender:self];
}

- (void)cell:(MPParallaxCollectionViewCell *)cell changeParallaxValueTo:(CGFloat)value{
    //NSLog(@"%f",value);
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Films" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];

    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"titleEnglish" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
	aFetchedResultsController.delegate = self;
	self.fetchedResultsController = aFetchedResultsController;

	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	     */
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    
    return _fetchedResultsController;
}


#pragma mark - Segue

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"FilmSections"] && _selectedIndexPath != nil) {
		BrowseFilmsViewController *browseFilms = (BrowseFilmsViewController *)[segue destinationViewController];
		browseFilms.managedObjectContext = _managedObjectContext;
		browseFilms.filmSectionName = _sectionList[_selectedIndexPath.row];
	}
}

@end
