//
//  FPNavigationController.h
//  FestPro
//
//  Created by Christopher Hall on 11/14/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FPNavigationController : UINavigationController

@end
