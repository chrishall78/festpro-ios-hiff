//
//  FPTabBarController.m
//  laapff2012
//
//  Created by Christopher Hall on 3/28/15.
//  Copyright (c) 2015 Christopher Hall Design. All rights reserved.
//

#import "FPTabBarController.h"

@interface FPTabBarController ()

@end

@implementation FPTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	// Change color of Tab Bar icons - change this in AppDelegate.m
	[UITabBar appearance].tintColor = UI_COLOR_04;
	
	/*
	// Change color of tab bar label text (unselected)
	[UITabBarItem.appearance setTitleTextAttributes:
	 @{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
	
	// Change color of tab bar label text (selected)
	[UITabBarItem.appearance setTitleTextAttributes:
	 @{NSForegroundColorAttributeName : UI_COLOR_04} forState:UIControlStateSelected];
	 */
	
	// generate a tinted unselected image based on image passed via the storyboard
	for(UITabBarItem *item in self.tabBar.items) {
		item.image = [item.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
	}
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
