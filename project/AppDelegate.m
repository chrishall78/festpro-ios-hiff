//
//  AppDelegate.m
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "AppDelegate.h"
#import "FestProAppDataObject.h"
#import "FPNavigationController.h"
#import "FPTabBarController.h"

// View Controllers
#import "HomeViewController.h"
#import "BrowseFilmsViewController.h"
#import "ScheduleViewController.h"
#import "MyScheduleViewController.h"
#import "SectionViewController.h"

@implementation AppDelegate

@synthesize window = _window;

#pragma mark -
#pragma mark Application Lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// Override point for customization after application launch.
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;

		// Master View
		FPTabBarController *masterTabBarController = splitViewController.viewControllers[0];
		FPNavigationController *masterNavController = [[masterTabBarController viewControllers] objectAtIndex:0];
		BrowseFilmsViewController *filmsView = [[masterNavController viewControllers] objectAtIndex:0];
		filmsView.managedObjectContext = self.managedObjectContext;
		
		masterNavController = [[masterTabBarController viewControllers] objectAtIndex:1];
		ScheduleViewController *scheduleView = [[masterNavController viewControllers] objectAtIndex:0];
		scheduleView.managedObjectContext = self.managedObjectContext;
		
		masterNavController = [[masterTabBarController viewControllers] objectAtIndex:2];
		MyScheduleViewController *myfestView = [[masterNavController viewControllers] objectAtIndex:0];
		myfestView.managedObjectContext = self.managedObjectContext;
		
		masterNavController = [[masterTabBarController viewControllers] objectAtIndex:3];
		SectionViewController *sectionView = [[masterNavController viewControllers] objectAtIndex:0];
		sectionView.managedObjectContext = self.managedObjectContext;

		// Detail View
		FPNavigationController *detailNavController = [splitViewController.viewControllers lastObject];
		splitViewController.delegate = (id)detailNavController.topViewController;

		HomeViewController *homeView = [[detailNavController viewControllers] objectAtIndex:0];
		homeView.managedObjectContext = self.managedObjectContext;
	} else {
		FPTabBarController *tabBarController = (FPTabBarController *)self.window.rootViewController;
		tabBarController.tabBar.tintColor = [UIColor whiteColor];
		
		FPNavigationController *navController = [[tabBarController viewControllers] objectAtIndex:0];
		HomeViewController *homeView = [[navController viewControllers] objectAtIndex:0];
		homeView.managedObjectContext = self.managedObjectContext;

		navController = [[tabBarController viewControllers] objectAtIndex:1];
		BrowseFilmsViewController *filmsView = [[navController viewControllers] objectAtIndex:0];
		filmsView.managedObjectContext = self.managedObjectContext;

		navController = [[tabBarController viewControllers] objectAtIndex:2];
		ScheduleViewController *scheduleView = [[navController viewControllers] objectAtIndex:0];
		scheduleView.managedObjectContext = self.managedObjectContext;

		navController = [[tabBarController viewControllers] objectAtIndex:3];
		MyScheduleViewController *myfestView = [[navController viewControllers] objectAtIndex:0];
		myfestView.managedObjectContext = self.managedObjectContext;
	}

	// Change color of Tab Bar icons
	[UITabBar appearance].tintColor = [UIColor whiteColor];
	
	// Change color of tab bar label text (unselected)
	[UITabBarItem.appearance setTitleTextAttributes:
	 @{NSForegroundColorAttributeName : [UIColor lightGrayColor]} forState:UIControlStateNormal];
	
	// Change color of tab bar label text (selected)
	[UITabBarItem.appearance setTitleTextAttributes:
	 @{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];

	// Change color of Segmented control
	[[UISegmentedControl appearance] setTintColor:UI_COLOR_02];
	[[UISegmentedControl appearance] setTitleTextAttributes:
	 @{NSForegroundColorAttributeName: UI_COLOR_03} forState:UIControlStateNormal];
	[[UISegmentedControl appearance] setTitleTextAttributes:
	 @{NSForegroundColorAttributeName: UI_COLOR_05} forState:UIControlStateSelected];

	_theAppDataObject = [[FestProAppDataObject alloc] init];
	_theAppDataObject.sectionFilterValue = @"ALL";
	_theAppDataObject.countryFilterValue = @"ALL";
	_theAppDataObject.languageFilterValue = @"ALL";
	_theAppDataObject.genreFilterValue = @"ALL";
	_theAppDataObject.eventTypeFilterValue = @"ALL";

	return YES;
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft |
				UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskPortraitUpsideDown);
	} else {
		// Allow rotations in iPhone app only when full screen video is playing.
		if (self.fullScreenVideoIsPlaying == YES) {
			return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft |
					UIInterfaceOrientationMaskLandscapeRight);
		} else {
			return UIInterfaceOrientationMaskPortrait;
		}
	}
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	/*
	 Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	 Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	 */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	/*
	 Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	 If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	 */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	/*
	 Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	 */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{    
	/*
	 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	 */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Saves changes in the application's managed object context before the application terminates.
	[self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
	// The directory the application uses to store the Core Data store file.
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
	// The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
	if (_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"hiff2012" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	// The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
	
	// Create the coordinator and store
	
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"hiff2012.sqlite"];
	NSError *error = nil;
	NSString *failureReason = @"There was an error creating or loading the application's saved data.";
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
		// Report any error we got.
		NSMutableDictionary *dict = [NSMutableDictionary dictionary];
		dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
		dict[NSLocalizedFailureReasonErrorKey] = failureReason;
		dict[NSUnderlyingErrorKey] = error;
		//error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
		// Replace this with code to handle the error appropriately.
		// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
		//NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		//abort();
	}
	
	return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
	// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
	if (_managedObjectContext != nil) {
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (!coordinator) {
		return nil;
	}
	_managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if (managedObjectContext != nil) {
		NSError *error = nil;
		if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			//NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			//abort();
		}
	}
}

@end
