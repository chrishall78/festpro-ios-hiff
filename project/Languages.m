//
//  Languages.m
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Languages.h"
#import "Films.h"

@implementation Languages

@dynamic languageId;
@dynamic name;
@dynamic language_films;

@end
