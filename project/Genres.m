//
//  Genres.m
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Genres.h"
#import "Films.h"


@implementation Genres

@dynamic genreId;
@dynamic name;
@dynamic genres_films;

@end
