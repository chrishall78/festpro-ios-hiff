//
//  Countries.m
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Countries.h"
#import "Films.h"


@implementation Countries

@dynamic countryId;
@dynamic name;
@dynamic countries_films;

@end
