//
//  Films.h
//  FestPro
//
//  Created by Christopher Hall on 2/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Countries, Genres, Languages, Personnel, PhotoVideo, Screenings, Sponsors;

@interface Films : NSManagedObject

@property (nonatomic, retain) NSString * countries;
@property (nonatomic, retain) NSString * directorName;
@property (nonatomic, retain) NSString * eventType;
@property (nonatomic, retain) NSNumber * film_id;
@property (nonatomic, retain) NSString * genres;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSString * languages;
@property (nonatomic, retain) NSString * premiereType;
@property (nonatomic, retain) NSNumber * runtime;
@property (nonatomic, retain) NSString * section;
@property (nonatomic, retain) NSString * slug;
@property (nonatomic, retain) NSString * synopsisLong;
@property (nonatomic, retain) NSString * synopsisShort;
@property (nonatomic, retain) NSString * synopsisOriginal;
@property (nonatomic, retain) NSString * titleAlpha;
@property (nonatomic, retain) NSString * titleEnglish;
@property (nonatomic, retain) NSString * titleRomanized;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSSet *films_countries;
@property (nonatomic, retain) NSSet *films_genres;
@property (nonatomic, retain) NSSet *films_languages;
@property (nonatomic, retain) NSSet *films_personnel;
@property (nonatomic, retain) NSSet *films_photos;
@property (nonatomic, retain) NSSet *films_screenings;
@property (nonatomic, retain) NSSet *films_sponsors;

@end

@interface Films (CoreDataGeneratedAccessors)

- (void)addFilms_countriesObject:(Countries *)value;
- (void)removeFilms_countriesObject:(Countries *)value;
- (void)addFilms_countries:(NSSet *)values;
- (void)removeFilms_countries:(NSSet *)values;

- (void)addFilms_genresObject:(Genres *)value;
- (void)removeFilms_genresObject:(Genres *)value;
- (void)addFilms_genres:(NSSet *)values;
- (void)removeFilms_genres:(NSSet *)values;

- (void)addFilms_languagesObject:(Languages *)value;
- (void)removeFilms_languagesObject:(Languages *)value;
- (void)addFilms_languages:(NSSet *)values;
- (void)removeFilms_languages:(NSSet *)values;

- (void)addFilms_personnelObject:(Personnel *)value;
- (void)removeFilms_personnelObject:(Personnel *)value;
- (void)addFilms_personnel:(NSSet *)values;
- (void)removeFilms_personnel:(NSSet *)values;

- (void)addFilms_photosObject:(PhotoVideo *)value;
- (void)removeFilms_photosObject:(PhotoVideo *)value;
- (void)addFilms_photos:(NSSet *)values;
- (void)removeFilms_photos:(NSSet *)values;

- (void)addFilms_screeningsObject:(Screenings *)value;
- (void)removeFilms_screeningsObject:(Screenings *)value;
- (void)addFilms_screenings:(NSSet *)values;
- (void)removeFilms_screenings:(NSSet *)values;

- (void)addFilms_sponsorsObject:(Sponsors *)value;
- (void)removeFilms_sponsorsObject:(Sponsors *)value;
- (void)addFilms_sponsors:(NSSet *)values;
- (void)removeFilms_sponsors:(NSSet *)values;

@end
