//
//  Languages.h
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Films;

@interface Languages : NSManagedObject

@property (nonatomic, retain) NSNumber * languageId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *language_films;

@end

@interface Languages (CoreDataGeneratedAccessors)

- (void)addLanguage_filmsObject:(Films *)value;
- (void)removeLanguage_filmsObject:(Films *)value;
- (void)addLanguage_films:(NSSet *)values;
- (void)removeLanguage_films:(NSSet *)values;

@end
