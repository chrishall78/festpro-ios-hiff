//
//  GridScheduleViewController.h
//  FestPro
//
//  Created by Christopher Hall on 11/26/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Films.h"
#import "Screenings.h"

@protocol GridScheduleViewController
@end

@interface GridScheduleViewController : UIViewController <UIScrollViewDelegate, UIPopoverControllerDelegate, UIGestureRecognizerDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSArray *festivalDates;
@property (strong, nonatomic) NSArray *festivalLocations;
@property (strong, nonatomic) NSDate  *startDate;
@property (assign, nonatomic) CGFloat lastContentOffsetX;
@property (assign, nonatomic) CGFloat lastContentOffsetY;
@property (assign, nonatomic) CGFloat hourViewWidth;
@property (assign, nonatomic) CGFloat locationHeight;
@property (strong, nonatomic) UIPopoverController *filmDetailPopover;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (strong, nonatomic) Films *currentFilm;
@property (strong, nonatomic) Screenings *currentScreening;

@property (strong, nonatomic) IBOutlet UIScrollView *dateTimeScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *screeningLocationScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *gridScrollView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *legendButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *dateButton;
@property (nonatomic, retain) id delegate;

- (void) addFilmstoGrid: (UIView *)view forLocations:(NSArray *)locations withStartDate:(NSDate *)date;
- (NSArray *) findScreeningDaysInFestival;
- (NSArray *) findScreeningLocationsInFestival;
- (NSDate *) getForDays:(int)days fromDate:(NSDate *)date;
- (IBAction)legendButtonTapped:(UIBarButtonItem *)sender;
- (IBAction)dateButtonTapped:(UIBarButtonItem *)sender;
- (IBAction)done: (id)sender;

@end
