//
//  PhotoVideo.m
//  FestPro
//
//  Created by Christopher Hall on 2/19/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "PhotoVideo.h"
#import "Films.h"

@implementation PhotoVideo

@dynamic film_id;
@dynamic type;
@dynamic videoUrl;
@dynamic order;
@dynamic videoService;
@dynamic photoSmallUrl;
@dynamic photoLargeUrl;
@dynamic photoXLargeUrl;
@dynamic photos_films;

@end
