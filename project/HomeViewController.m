//
//  HomeViewController.m
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "JSONCategories.h"
#import "FPNavigationController.h"
#import "HomeViewController.h"
#import "BrowseFilmsViewController.h"
#import "ScheduleViewController.h"
#import "MyScheduleViewController.h"
#import "WebViewController.h"

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController
	willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode {
	if (displayMode == UISplitViewControllerDisplayModePrimaryHidden) {
		[self.navigationItem setLeftBarButtonItem: splitController.displayModeButtonItem];
		[self.navigationItem.leftBarButtonItem setTitle:NSLocalizedString(@"Open List", @"Open List")];
	}
}

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Open Films", @"Open Films");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
	// Called when the view is shown again in the split view, invalidating the button and popover controller.
	[self.navigationItem setLeftBarButtonItem:nil animated:YES];
	self.masterPopoverController = nil;
}

#pragma mark - Gesture actions

- (IBAction)moveRightTab:(id)sender
{
	NSUInteger selectedIndex = [self.tabBarController selectedIndex];
	[self.tabBarController setSelectedIndex:selectedIndex + 1];
}

- (IBAction)moveLeftTab:(id)sender
{
	NSUInteger selectedIndex = [self.tabBarController selectedIndex];
	[self.tabBarController setSelectedIndex:selectedIndex + 3];
}


#pragma mark - View lifecycle

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeftTab:)];
		[swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
		[self.view addGestureRecognizer:swipeLeft];
		
		UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveRightTab:)];
		[swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
		[self.view addGestureRecognizer:swipeRight];
		
		UITapGestureRecognizer *tapBkgImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveRightTab:)];
		[tapBkgImage setNumberOfTapsRequired:1];
		[self.view addGestureRecognizer:tapBkgImage];
	}

	[[AFNetworkReachabilityManager sharedManager] startMonitoring];
	
	_filterArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];    

	// Get a reference to the standard user defaults
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	// Set up empty "MyFest" array the first time the app runs
	if ([prefs boolForKey:@"hasRunBefore"] != YES) {
		NSArray *emptyArray = [[NSArray alloc] init];
		[prefs setObject:emptyArray forKey:@"myFestScreenings"];
		[prefs setBool:NO forKey:@"updatedRecently"];
		[prefs synchronize];
	}

	// Set the app version the first time the app runs
	if (![prefs floatForKey:@"appVersion"]) {
		[prefs setFloat:[[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] floatValue] forKey:@"appVersion"];
		[prefs setBool:NO forKey:@"allDataDownloaded"];
		[prefs synchronize];
	}
	
	BOOL doesContain = [self.view.subviews containsObject:_cornersView];
	if (doesContain == false) {
		if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
			_cornersView = [[UIView alloc] initWithFrame:CGRectMake(244.0, 140.0, 280.0, 145.0)];
		} else {
			CGRect screenRect = [[UIScreen mainScreen] bounds];
			CGFloat screenWidth = screenRect.size.width;
			int leftMargin = (screenWidth - 280.0) / 2;
			_cornersView = [[UIView alloc] initWithFrame:CGRectMake(leftMargin, 40.0, 280.0, 145.0)];
		}
		[_cornersView setBackgroundColor:[UIColor whiteColor]];
		[_cornersView.layer setCornerRadius:15.0];
		
		_progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(24.0, 47.0, 233.0, 11.0)];
		[_progressView setTintColor:UI_COLOR_03];
		[_progressView setProgressViewStyle:UIProgressViewStyleBar];
		[_progressView setProgress:0.0 animated:NO];
		
		_progressTitle = [[UILabel alloc] initWithFrame:CGRectMake(24.0, 15.0, 233.0, 24.0)];
		[_progressTitle setBackgroundColor:[UIColor clearColor]];
		[_progressTitle setText:@"Downloading Film Data"];
		[_progressTitle setTextAlignment:NSTextAlignmentCenter];
		[_progressTitle setTextColor:[UIColor blackColor]];
		[_progressTitle setFont:[UIFont boldSystemFontOfSize:20.0]];
		
		_progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(24.0, 65.0, 233.0, 21.0)];
		[_progressLabel setBackgroundColor:[UIColor clearColor]];
		[_progressLabel setFont:[UIFont systemFontOfSize:17.0]];
		[_progressLabel setText:@"0% Completed..."];
		[_progressLabel setTextColor:[UIColor blackColor]];
		[_progressLabel setTextAlignment:NSTextAlignmentCenter];
		
		_downloadButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		[_downloadButton setFrame:CGRectMake(40.0, 94.0, 201.0, 37.0)];
		[_downloadButton setTitle:@"Try Downloading Again?" forState:UIControlStateNormal];
		[_downloadButton setTitleColor:UI_COLOR_03 forState:UIControlStateNormal];
		[_downloadButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
		[_downloadButton addTarget:self action:@selector(tryDownloadingData:) forControlEvents:UIControlEventTouchUpInside];
		[_downloadButton setHidden:TRUE];

		_downloadUpdateButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		[_downloadUpdateButton setFrame:CGRectMake(25.0, 94.0, 110.0, 37.0)];
		[_downloadUpdateButton setTitle:@"Update Now?" forState:UIControlStateNormal];
		[_downloadUpdateButton setTitleColor:UI_COLOR_03 forState:UIControlStateNormal];
		[_downloadUpdateButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
		[_downloadUpdateButton addTarget:self action:@selector(tryDownloadingData:) forControlEvents:UIControlEventTouchUpInside];
		[_downloadUpdateButton setHidden:TRUE];

		_closeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		[_closeButton setFrame:CGRectMake(145.0, 94.0, 110.0, 37.0)];
		[_closeButton setTitle:@"Cancel" forState:UIControlStateNormal];
		[_closeButton setTitleColor:UI_COLOR_03 forState:UIControlStateNormal];
		[_closeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
		[_closeButton addTarget:self action:@selector(dismissView:) forControlEvents:UIControlEventTouchUpInside];
		[_closeButton setHidden:TRUE];
	}
	
	// Initialize downloading data view if all data has not been downloaded
	float currentVersion = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] floatValue];
	if ([prefs boolForKey:@"allDataDownloaded"] != YES || [prefs floatForKey:@"appVersion"] < currentVersion) {
		[self.view addSubview:_cornersView];
		[_cornersView addSubview:_progressView];
		[_cornersView addSubview:_progressTitle];
		[_cornersView addSubview:_progressLabel];
		[_cornersView addSubview:_downloadButton];
		[_cornersView addSubview:_downloadUpdateButton];
		[_cornersView addSubview:_closeButton];
	} else {
		// Check for update here & display dialog to update
		//[self tryCheckingForUpdates:self];
	}
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		[_backgroundScrollView setContentMode:UIViewContentModeScaleAspectFit];

		//NSLog(@"width: %f height: %f",screenWidth, screenHeight);
		//NSLog(@"scroll view: %@",_backgroundScrollView);
		//NSLog(@"image view: %@",_backgroundImageView);

		CGRect screenRect = [[UIScreen mainScreen] bounds];
		if (IS_PORTRAIT) {
			CGSize portrait;
			CGSize portraitIpad = CGSizeMake(768.0, 960.0);
			CGSize portraitIpadPro = CGSizeMake(1024.0, 1366.0);
			
			if (screenRect.size.width == 1024.0){
				portrait = portraitIpadPro;
			} else {
				portrait = portraitIpad;
			}

			[_backgroundScrollView setFrame:CGRectMake(0, 0, portrait.width, portrait.height)];
			[_backgroundScrollView setContentSize:portrait];
			[_backgroundImageView setFrame:CGRectMake(0, 0, portrait.width, portrait.height)];
		} else {
			CGSize landscape;
			CGSize landscapeIpad = CGSizeMake(704.0, 880.0);
			CGSize landscapeIpadPro = CGSizeMake(1046.0, 1024.0);

			if (screenRect.size.height == 1024.0){
				landscape = landscapeIpadPro;
			} else {
				landscape = landscapeIpad;
			}

			[_backgroundScrollView setFrame:CGRectMake(0, 0, landscape.width, landscape.height)];
			[_backgroundScrollView setContentSize:landscape];
			[_backgroundImageView setFrame:CGRectMake(0, 0, landscape.width, landscape.height)];
		}
	}

	// Get a reference to the standard user defaults
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	// If data has not been downloaded, try downloading it
	if ([prefs boolForKey:@"allDataDownloaded"] != YES) {
		[self tryDownloadingData:self];
	}
	
	float currentVersion = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] floatValue];
	//NSLog(@"Current: %f | Stored: %f",currentVersion, [prefs floatForKey:@"appVersion"]);
	if ([prefs floatForKey:@"appVersion"] < currentVersion) {
		[prefs setBool:NO forKey:@"allDataDownloaded"];
		[prefs synchronize];

		[self tryDownloadingData:self];
	}
}

- (void)viewDidUnload
{
	[super viewDidUnload];
	//[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return YES;
	} else {
		return NO;
	}
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		CGRect screenRect = [[UIScreen mainScreen] bounds];
		if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
			CGSize portrait;
			CGSize portraitIpad = CGSizeMake(768.0, 960.0);
			CGSize portraitIpadPro = CGSizeMake(1024.0, 1366.0);
			
			if (screenRect.size.height == 1024.0){
				portrait = portraitIpadPro;
			} else {
				portrait = portraitIpad;
			}

			[_backgroundScrollView setFrame:CGRectMake(0, 0, portrait.width, portrait.height)];
			[_backgroundScrollView setContentSize:portrait];
			[_backgroundImageView setFrame:CGRectMake(0, 0, portrait.width, portrait.height)];
		} else {
			CGSize landscape;
			CGSize landscapeIpad = CGSizeMake(704.0, 880.0);
			CGSize landscapeIpadPro = CGSizeMake(1046.0, 1024.0);
			
			if (screenRect.size.width == 1024.0){
				landscape = landscapeIpadPro;
			} else {
				landscape = landscapeIpad;
			}

			[_backgroundScrollView setFrame:CGRectMake(0, 0, landscape.width, landscape.height)];
			[_backgroundScrollView setContentSize:landscape];
			[_backgroundImageView setFrame:CGRectMake(0, 0, landscape.width, landscape.height)];
		}
	}
}


//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//  [super preferredInterfaceOrientationForPresentation];
//  return UIInterfaceOrientationPortrait;
//}

#pragma mark - Checking for updates

- (IBAction)showAlert:(id)sender {
	UIAlertController *message = [UIAlertController alertControllerWithTitle:@"Checking for Updates"
																	 message:@"No updates were found."
															  preferredStyle:UIAlertControllerStyleAlert];
	[message addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
											  handler:^(UIAlertAction *action) {}]];
	[self presentViewController:message animated:YES completion:nil];
}

- (IBAction)tryCheckingForUpdates:(id)sender
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		NSString *result = nil;
		result = [self checkForUpdates];
		
		if (result != nil ) {
			BOOL doesContain = [self.view.subviews containsObject:_cornersView];
			
			if (doesContain == TRUE) {
				[self.cornersView setHidden:false];
				[self.cornersView setAlpha:1.0f];

				[self.progressTitle setText:@"Updates Found"];
				[self.progressLabel setText:result];
				[self.downloadUpdateButton setHidden:false];
				[self.closeButton setHidden:false];
			} else {
				[self.view addSubview:_cornersView];
				[_cornersView addSubview:_progressView];
				[_cornersView addSubview:_progressTitle];
				[_cornersView addSubview:_progressLabel];
				[_cornersView addSubview:_downloadButton];
				[_cornersView addSubview:_downloadUpdateButton];
				[_cornersView addSubview:_closeButton];

				[self.progressTitle setText:@"Updates Found"];
				[self.progressLabel setText:result];
				[self.downloadUpdateButton setHidden:false];
				[self.closeButton setHidden:false];
			}
		} else {
			[self performSelectorOnMainThread:@selector(showAlert:) withObject:nil waitUntilDone:NO];
		}
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	});
}

- (NSString *)checkForUpdates
{
	__block NSString *updatesFound = nil;
	
	// Get a reference to the standard user defaults
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *baseUrl = kBaseDataURL;
	
	if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
		if ( [prefs objectForKey:@"lastUpdatedDate"] != nil) {
			//NSLog(@"last updated date: %@",[prefs objectForKey:@"lastUpdatedDate"]);
			//NSString *timestamp = @"1335070800"; // April 22, 2012
			//NSDictionary *jsonUpdates = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@%@",baseUrl, @"updates_since/",timestamp]];
			
			// Get updates data from FestPro
			NSDictionary *jsonUpdates = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@%@",baseUrl, @"updates_since/",[prefs objectForKey:@"lastUpdatedDate"]]];
			NSArray *updates = [jsonUpdates objectForKey:@"api_data"];
			
			// Recommend update if 1 or more films or screenings have been updated
			int numFilms = [[updates valueForKey:@"films"] intValue];
			int numScreenings = [[updates valueForKey:@"schedule"] intValue];
			if (numFilms > 0 && numScreenings > 0) {
				updatesFound = [NSString stringWithFormat:@"%@ Films, %@ Screenings", [updates valueForKey:@"films"], [updates valueForKey:@"schedule"]];
			} else if (numFilms > 0) {
				updatesFound = [NSString stringWithFormat:@"%@ Films", [updates valueForKey:@"films"]];
			} else if (numScreenings > 0) {
				updatesFound = [NSString stringWithFormat:@"%@ Screenings", [updates valueForKey:@"schedule"]];
			}
		}
	}
	return updatesFound;
}

#pragma mark - Initial Data Download

- (IBAction)tryDownloadingData:(id)sender
{
	// Disable tab bar buttons until we can confirm that all data has been downloaded
	UITabBar *tabBar = [[self tabBarController] tabBar];
	UITabBarItem *tabItem2 = [[tabBar items] objectAtIndex:1];
	UITabBarItem *tabItem3 = [[tabBar items] objectAtIndex:2];
	UITabBarItem *tabItem4 = [[tabBar items] objectAtIndex:3];
	[tabItem2 setEnabled:FALSE];
	[tabItem3 setEnabled:FALSE];
	[tabItem4 setEnabled:FALSE];    
	
	// Get a reference to the standard user defaults
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

	[self.downloadButton setHidden:TRUE];
	
	if (![self.downloadUpdateButton isHidden]) {
		[prefs setBool:NO forKey:@"allDataDownloaded"];
		[prefs synchronize];
		[_progressTitle setText:@"Downloading Film Data"];
		[self.downloadUpdateButton setHidden:TRUE];
		[self.closeButton setHidden:TRUE];
	}
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		BOOL result = FALSE;
		result = [self getData];
				
		if (result == TRUE) {
			[prefs setBool:YES forKey:@"hasRunBefore"];
			[prefs setBool:YES forKey:@"allDataDownloaded"];
			NSString *timestamp = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
			[prefs setValue:timestamp forKey:@"lastUpdatedDate"];
			
			// Build list of values for Browse Films filters and Section view
			if ([_filterArray count] > 0) {
				if ([prefs arrayForKey:@"sectionList"] == nil) {
					NSArray *sectionList;
					sectionList = [self selectAndSortFilterItems:_filterArray forType:@"section"];
					[prefs setObject:sectionList forKey:@"sectionList"];
				}
				
				if ([prefs arrayForKey:@"countriesList"] == nil) {
					NSArray *countriesList;
					countriesList = [self selectAndSortFilterItems:_filterArray forType:@"countries"];
					[prefs setObject:countriesList forKey:@"countriesList"];
				}
				
				if ([prefs arrayForKey:@"languagesList"] == nil) {
					NSArray *languagesList;
					languagesList = [self selectAndSortFilterItems:_filterArray forType:@"languages"];
					[prefs setObject:languagesList forKey:@"languagesList"];
				}
				
				if ([prefs arrayForKey:@"genresList"] == nil) {
					NSArray *genresList;
					genresList = [self selectAndSortFilterItems:_filterArray forType:@"genres"];
					[prefs setObject:genresList forKey:@"genresList"];
				}
				
				if ([prefs arrayForKey:@"eventTypeList"] == nil) {
					NSArray *eventTypeList;
					eventTypeList = [self selectAndSortFilterItems:_filterArray forType:@"eventType"];
					[prefs setObject:eventTypeList forKey:@"eventTypeList"];
				}
				
				dispatch_async(dispatch_get_main_queue(), ^{
					UITabBarController *tabController = [[self.splitViewController viewControllers] objectAtIndex:0];
					FPNavigationController *navController = [[tabController viewControllers] objectAtIndex:0];
					BrowseFilmsViewController *browseFilms = [[navController viewControllers] objectAtIndex:0];
					
					if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
						browseFilms.fetchedResultsController = nil;
						NSError *error = nil;
						if (![browseFilms.fetchedResultsController performFetch:&error]) {
							NSLog(@"Error in refetch: %@",[error localizedDescription]);
							//abort();
						}
						browseFilms.imageUrlBySection = [browseFilms setImageUrlBySection];
						[browseFilms.tableView reloadData];
					}
				});
			}
			[prefs synchronize];
			
			[self.downloadButton setHidden:TRUE];
			
			[self performSelectorOnMainThread:@selector(dismissView:) withObject:nil waitUntilDone:NO];

			// Success, re-enable tab bar buttons
			[tabItem2 setEnabled:TRUE];
			[tabItem3 setEnabled:TRUE];
			[tabItem4 setEnabled:TRUE];
		} else {
			[prefs setBool:YES forKey:@"hasRunBefore"];
			[prefs setBool:NO forKey:@"allDataDownloaded"];
			[prefs synchronize];
			
			// Show button to try downloading again
			[self.progressLabel setText:@"Connection Error"];
			[self.downloadButton setHidden:FALSE];
		}
	});
}


- (BOOL)getData
{
	__block BOOL errorSaving = FALSE; __block BOOL errorDownloading = FALSE;
			
	// Get a reference to the standard user defaults
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	// Define the base url for all of the API calls - defined in AppConstants class
	NSString *baseUrl = kBaseDataURL;
	
	NSString * const kKeyApiData = @"api_data";
	NSString * const kKeyFilmID = @"film_id";

	float currentVersion = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] floatValue];
	if ([prefs boolForKey:@"allDataDownloaded"] != YES || [prefs floatForKey:@"appVersion"] < currentVersion) {
		[self performSelectorOnMainThread:@selector(loadingProgress:) withObject:[NSNumber numberWithFloat:0.00] waitUntilDone:NO];
		
		if ([[AFNetworkReachabilityManager sharedManager] isReachable]) {
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			
			// Get festival data from FestPro
			NSDictionary *jsonFestival = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@",baseUrl, @"festival"]];
			NSArray *festival = [jsonFestival objectForKey:kKeyApiData];
			
			[prefs setValue:[festival valueForKey:@"name"] forKey:@"festivalName"];
			[prefs setValue:[festival valueForKey:@"year"] forKey:@"festivalYear"];
			[prefs setValue:[festival valueForKey:@"startdate"] forKey:@"festivalStartDate"];
			[prefs setValue:[festival valueForKey:@"enddate"] forKey:@"festivalEndDate"];
			[prefs setValue:[festival valueForKey:@"timezone"] forKey:@"festivalTimeZone"];
			[prefs setValue:[festival valueForKey:@"updates"] forKey:@"programUpdates"];
			
			// Get film data from FestPro
			NSDictionary *jsonFilms = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@",baseUrl, @"films"]];
			NSArray *films = [jsonFilms objectForKey:kKeyApiData];

			if ([films count] > 0) {
				[self performSelectorOnMainThread:@selector(loadingProgress:) withObject:[NSNumber numberWithFloat:0.03] waitUntilDone:NO];
			} else {
				errorDownloading = TRUE;
			}
			
			// Get screening data from FestPro
			NSDictionary *jsonScreenings = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@",baseUrl, @"schedule"]];
			NSArray *screenings = [jsonScreenings objectForKey:kKeyApiData];
			if ([screenings count] > 0) { 
				[self performSelectorOnMainThread:@selector(loadingProgress:) withObject:[NSNumber numberWithFloat:0.06] waitUntilDone:NO];
			}
			
			// Get personnel data from FestPro
			NSDictionary *jsonPersonnel = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@",baseUrl, @"personnel"]];
			NSArray *personnel = [jsonPersonnel objectForKey:kKeyApiData];
			if ([personnel count] > 0) { 
				[self performSelectorOnMainThread:@selector(loadingProgress:) withObject:[NSNumber numberWithFloat:0.10] waitUntilDone:NO];
			}

			// Get sponsor data from FestPro
			NSDictionary *jsonSponsors = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@",baseUrl, @"sponsors"]];
			NSArray *sponsors = [jsonSponsors objectForKey:kKeyApiData];
			if ([sponsors count] > 0) {
				[self performSelectorOnMainThread:@selector(loadingProgress:) withObject:[NSNumber numberWithFloat:0.13] waitUntilDone:NO];
			}

			// Get photo data from FestPro
			NSDictionary *jsonPhotos = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@",baseUrl, @"photos"]];
			NSArray *photos = [jsonPhotos objectForKey:kKeyApiData];
			if ([photos count] > 0) {
				[self performSelectorOnMainThread:@selector(loadingProgress:) withObject:[NSNumber numberWithFloat:0.16] waitUntilDone:NO];
			}
			
			// Get video data from FestPro
			NSDictionary *jsonVideos = [NSDictionary dictionaryWithContentsOfJSONURLString:[NSString stringWithFormat:@"%@%@",baseUrl, @"videos"]];
			NSArray *videos = [jsonVideos objectForKey:kKeyApiData];
			if ([videos count] > 0) { 
				[self performSelectorOnMainThread:@selector(loadingProgress:) withObject:[NSNumber numberWithFloat:0.20] waitUntilDone:NO];
			}

			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			
			int filmsCount = (uint32_t)[films count];
			float incrementValue = 0.01;
			__block float startingValue = 0.20;
			if (filmsCount != 0) {
				incrementValue = (80.0 / (float)filmsCount) / 100;
			}
			
			[_managedObjectContext performBlockAndWait:^{

				// Remove all existing core data entities to eliminate any partial/duplicate results
				[self deleteAllObjects:@"Films"];
				[self deleteAllObjects:@"Screenings"];
				[self deleteAllObjects:@"Personnel"];
				[self deleteAllObjects:@"Sponsors"];
				[self deleteAllObjects:@"PhotoVideo"];
				
				// Loop through the films, screenings and personnel
				for (id x in films) {
					self.thisFilm = (Films *)[NSEntityDescription insertNewObjectForEntityForName:@"Films" inManagedObjectContext:_managedObjectContext];
					
					// Assign values from JSON query to core data managed context for each film
					[self.thisFilm setTitleAlpha:[x objectForKey:@"titleAlpha"]];
					[self.thisFilm setTitleEnglish:[x objectForKey:@"titleEnglish"]];
					[self.thisFilm setTitleRomanized:[x objectForKey:@"titleRomanized"]];
					[self.thisFilm setYear:[[NSNumber alloc] initWithInt:[[x objectForKey:@"year"]intValue]]];
					[self.thisFilm setRuntime:[[NSNumber alloc] initWithInt:[[x objectForKey:@"runtime"]intValue]]];
					[self.thisFilm setCountries:[x objectForKey:@"countries"]];
					[self.thisFilm setLanguages:[x objectForKey:@"languages"]];
					[self.thisFilm setGenres:[x objectForKey:@"genres"]];
					
					NSString *sectionType = [x objectForKey:@"section"];
					if (sectionType == (id)[NSNull null] || sectionType.length == 0) {
						[self.thisFilm setSection:@""];
					} else {
						[self.thisFilm setSection:sectionType];
					}
					NSString *eventType = [x objectForKey:@"eventType"];
					if (eventType == (id)[NSNull null] || eventType.length == 0) {
						[self.thisFilm setEventType:@""];
					} else {
						[self.thisFilm setEventType:eventType];
					}
					NSString *premiereType = [x objectForKey:@"premiereType"];
					if (premiereType == (id)[NSNull null] || premiereType.length == 0) {
						[self.thisFilm setPremiereType:@""];
					} else {
						[self.thisFilm setPremiereType:premiereType];
					}
					
					[self.thisFilm setDirectorName:[x objectForKey:@"directorName"]];
					[self.thisFilm setSynopsisLong:[x objectForKey:@"synopsisLong"]];
					[self.thisFilm setSynopsisShort:[x objectForKey:@"synopsisShort"]];
					[self.thisFilm setSynopsisOriginal:[x objectForKey:@"synopsisOriginal"]];
					[self.thisFilm setImageUrl:[x objectForKey:@"imageUrl"]];
					[self.thisFilm setSlug:[x objectForKey:@"slug"]];
					[self.thisFilm setFilm_id:[[NSNumber alloc] initWithInt:[[x objectForKey:kKeyFilmID]intValue]]];

					NSMutableSet *screeningSet = [[NSMutableSet alloc] init];
					NSMutableSet *screeningFilmSet = [[NSMutableSet alloc] init];
					for (id y in screenings) {
						if ([[x objectForKey:kKeyFilmID] isEqualToString:[y objectForKey:kKeyFilmID]]) {
							self.thisScreening = (Screenings *)[NSEntityDescription insertNewObjectForEntityForName:@"Screenings" inManagedObjectContext:_managedObjectContext];
							
							// Assign values from JSON query to core data managed context for each screening
							[self.thisScreening setProgramName:[y objectForKey:@"programName"]];
							[self.thisScreening setFilms:[y objectForKey:@"films"]];
							[self.thisScreening setFilm_ids:[y objectForKey:@"filmIds"]];
							NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
							[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZ"];
							NSString *screening = [y objectForKey:@"dateTime"];
							NSDate *date = [dateFormatter dateFromString:screening];
							[self.thisScreening setDateTime:date];
							[self.thisScreening setDateString:[dateFormatter dateFromString:[y objectForKey:@"dateString"]]];
							[self.thisScreening setTicketLink:[y objectForKey:@"ticketLink"]];
							[self.thisScreening setRush:[[NSNumber alloc] initWithInt:[[y objectForKey:@"rush"]intValue]]];
							[self.thisScreening setFree:[[NSNumber alloc] initWithInt:[[y objectForKey:@"free"]intValue]]];
							[self.thisScreening setLocation:[y objectForKey:@"location"]];
							[self.thisScreening setTotalRuntime:[[NSNumber alloc] initWithInt:[[y objectForKey:@"totalRuntime"]intValue]]];
							[self.thisScreening setScreening_id:[[NSNumber alloc] initWithInt:[[y objectForKey:@"screening_id"]intValue]]];
							[self.thisScreening setFilm_id:[[NSNumber alloc] initWithInt:[[y objectForKey:kKeyFilmID]intValue]]];

							// Create relationship between screening and film and add to Set
							[screeningFilmSet addObject:self.thisFilm];
							
							[self.thisScreening setScreenings_films:screeningFilmSet];
							[screeningSet addObject:self.thisScreening];
						}
					}
					
					NSMutableSet *personnelSet = [[NSMutableSet alloc] init];
					for (id z in personnel) {
						if ([[x objectForKey:kKeyFilmID] isEqualToString:[z objectForKey:kKeyFilmID]]) {
							self.thisPersonnel = (Personnel *)[NSEntityDescription insertNewObjectForEntityForName:@"Personnel" inManagedObjectContext:_managedObjectContext];
							
							// Assign values from JSON query to core data managed context for each personnel
							[self.thisPersonnel setFilm_id:[[NSNumber alloc] initWithInt:[[z objectForKey:kKeyFilmID]intValue]]];
							[self.thisPersonnel setFirstName:[z objectForKey:@"firstName"]];
							[self.thisPersonnel setLastName:[z objectForKey:@"lastName"]];
							[self.thisPersonnel setRole:[z objectForKey:@"role"]];
							
							// Create relationship between personnel and film and add to Set
							[self.thisPersonnel setPersonnel_films:self.thisFilm];
							[personnelSet addObject:self.thisPersonnel];
						}
					}
					
					NSMutableSet *sponsorSet = [[NSMutableSet alloc] init];
					for (id s in sponsors) {
						//NSLog(@"%@",[s objectForKey:kKeyFilmID]);
						if ([[x objectForKey:kKeyFilmID] isEqualToString:[s objectForKey:kKeyFilmID]]) {
							self.thisSponsor = (Sponsors *)[NSEntityDescription insertNewObjectForEntityForName:@"Sponsors" inManagedObjectContext:_managedObjectContext];
							
							// Assign values from JSON query to core data managed context for each sponsor
							[self.thisSponsor setFilm_id:[[NSNumber alloc] initWithInt:[[s objectForKey:kKeyFilmID]intValue]]];
							[self.thisSponsor setSponsorName:[s objectForKey:@"name"]];
							[self.thisSponsor setSponsorDesc:[s objectForKey:@"description"]];
							[self.thisSponsor setSponsorSiteUrl:[s objectForKey:@"url_website"]];
							[self.thisSponsor setSponsorLogoUrl:[s objectForKey:@"url_logo"]];
							
							// Create relationship between sponsor and film and add to Set
							[self.thisSponsor setSponsors_films:self.thisFilm];
							[sponsorSet addObject:self.thisSponsor];
						}
					}
					
					NSMutableSet *photosSet = [[NSMutableSet alloc] init];
					for (id v in videos) {
						if ([[x objectForKey:kKeyFilmID] isEqualToString:[v objectForKey:kKeyFilmID]]) {
							self.thisPhotoVideo = (PhotoVideo *)[NSEntityDescription insertNewObjectForEntityForName:@"PhotoVideo" inManagedObjectContext:_managedObjectContext];
							
							// Assign values from JSON query to core data managed context for each video
							[self.thisPhotoVideo setFilm_id:[[NSNumber alloc] initWithInt:[[v objectForKey:kKeyFilmID]intValue]]];
							[self.thisPhotoVideo setType:@"video"];
							[self.thisPhotoVideo setPhotoSmallUrl:nil];
							[self.thisPhotoVideo setPhotoLargeUrl:nil];
							[self.thisPhotoVideo setPhotoXLargeUrl:nil];
							[self.thisPhotoVideo setOrder:[[NSNumber alloc] initWithInt:0]];
							[self.thisPhotoVideo setVideoService:[v objectForKey:@"service_name"]];
							[self.thisPhotoVideo setVideoUrl:[v objectForKey:@"url_video"]];
							[photosSet addObject:self.thisPhotoVideo];
						}
					}
					
					for (id a in photos) {
						if ([[x objectForKey:kKeyFilmID] isEqualToString:[a objectForKey:kKeyFilmID]]) {
							self.thisPhotoVideo = (PhotoVideo *)[NSEntityDescription insertNewObjectForEntityForName:@"PhotoVideo" inManagedObjectContext:_managedObjectContext];
							
							// Assign values from JSON query to core data managed context for each photo
							[self.thisPhotoVideo setFilm_id:[[NSNumber alloc] initWithInt:[[a objectForKey:kKeyFilmID]intValue]]];
							[self.thisPhotoVideo setType:@"photo"];
							[self.thisPhotoVideo setPhotoSmallUrl:[a objectForKey:@"photo_small"]];
							[self.thisPhotoVideo setPhotoLargeUrl:[a objectForKey:@"photo_large"]];
							[self.thisPhotoVideo setPhotoXLargeUrl:[a objectForKey:@"photo_xlarge"]];
							[self.thisPhotoVideo setOrder:[[NSNumber alloc] initWithInt:[[a objectForKey:@"order"]intValue]]];
							[self.thisPhotoVideo setVideoService:nil];
							[self.thisPhotoVideo setVideoUrl:nil];
							
							// Create relationship between personnel and film and add to Set
							[self.thisPhotoVideo setPhotos_films:self.thisFilm];
							[photosSet addObject:self.thisPhotoVideo];
						}
					}
					
					// Create relationship between film and screening(s) / personnel
					[self.thisFilm setFilms_screenings:screeningSet];
					[self.thisFilm setFilms_personnel:personnelSet];
					[self.thisFilm setFilms_sponsors:sponsorSet];
					[self.thisFilm setFilms_photos:photosSet];
					
					// Add to filter array to determine unique values for film filters
					[_filterArray addObject:self.thisFilm];
					
					//  Commit item to core data
					NSError *error;
					if (![_managedObjectContext save:&error]) {
						NSLog(@"Failed to add new film with error: %@ %ld", [error domain], (long)[error code]);
						errorSaving = TRUE;
					}
					
					float thisValue = [[NSString stringWithFormat:@"%.2f",startingValue + incrementValue] floatValue];
					[self performSelectorOnMainThread:@selector(loadingProgress:) withObject:[NSNumber numberWithFloat:thisValue] waitUntilDone:NO];
					
					startingValue = startingValue + incrementValue;
					if (startingValue > 1.0) { startingValue = 1.0 - incrementValue;}
					
				}
			}];

			// Reset filter values so they will be updated with new values.
			[prefs setValue:nil forKey:@"sectionList"];
			[prefs setValue:nil forKey:@"eventTypeList"];
			[prefs setValue:nil forKey:@"countriesList"];
			[prefs setValue:nil forKey:@"languagesList"];
			[prefs setValue:nil forKey:@"genresList"];

			// Update preference values and app version
			[prefs setBool:YES forKey:@"hasRunBefore"];
			NSString *timestamp = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
			[prefs setValue:timestamp forKey:@"lastUpdatedDate"];
			[prefs setFloat:[[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] floatValue] forKey:@"appVersion"];
			[prefs synchronize];
		} else {
			errorDownloading = TRUE;
		}
	}

	if (errorDownloading == TRUE) {
		return FALSE;
	} else if (errorSaving == TRUE) {
		return FALSE;
	} else {
		return TRUE;
	}
}

- (IBAction)dismissView:(id)sender {
	NSTimeInterval animationDuration = 2.0;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:animationDuration];
	_cornersView.alpha = 0.0f;
	[UIView commitAnimations];
	[_cornersView setHidden:TRUE];
	
	//_updatesButton.enabled = TRUE;
	//_authButton.enabled = TRUE;
}

- (void)loadingProgress:(NSNumber *)nProgress{
	float percentage = [nProgress floatValue] * 100;
	[[self progressView] setProgress:[nProgress floatValue]];
	[[self progressLabel] setText:[NSString stringWithFormat:@"%i%% Completed...",(int) percentage]];
}

- (void) deleteAllObjects: (NSString *) entityDescription  {
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	
	NSError *error;
	NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
	
	for (NSManagedObject *managedObject in items) {
		[_managedObjectContext deleteObject:managedObject];
		//NSLog(@"%@ object deleted",entityDescription);
	}
	if (![_managedObjectContext save:&error]) {
		NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
	}
}

- (NSArray *)selectAndSortFilterItems:(NSMutableArray *)results forType:(NSString *)type {
	NSMutableArray *itemList = [[NSMutableArray alloc] init];
	NSMutableArray *itemList2 = [[NSMutableArray alloc] init];
	NSArray *subItemList;
	
	for (Films *item in results) {
		if ([type isEqualToString:@"section"]) {
			if ([itemList containsObject:item.section] == NO) {
				[itemList addObject:item.section];
			}
		}
		if ([type isEqualToString:@"countries"]) {
			subItemList = [item.countries componentsSeparatedByString:@", "];
			for (NSString *value in subItemList) {
				if ([itemList containsObject:value] == NO) {
					[itemList addObject:value];
				}
			}
		}
		if ([type isEqualToString:@"languages"]) {
			subItemList = [item.languages componentsSeparatedByString:@", "];
			for (NSString *value in subItemList) {
				if ([itemList containsObject:value] == NO) {
					[itemList addObject:value];
				}
			}
		}
		if ([type isEqualToString:@"genres"]) {
			subItemList = [item.genres componentsSeparatedByString:@", "];
			for (NSString *value in subItemList) {
				if ([itemList containsObject:value] == NO) {
					[itemList addObject:value];
				}
			}
		}
		if ([type isEqualToString:@"eventType"]) {
			if ([itemList containsObject:item.eventType] == NO) {
				[itemList addObject:item.eventType];
			}
		}
	}
	for (id x in itemList) {
		if (![x isEqualToString:@""]) {
			[itemList2 addObject:x];
		}
	}
	[itemList2 sortUsingSelector:@selector(compare:)];
	[itemList removeAllObjects];
	
	[itemList addObject:@"ALL"];
	for (id x in itemList2) { [itemList addObject:x]; }
	
	
	return itemList;
}


#pragma mark - WebViewController

- (void) dismissWebView {
	[self dismissViewControllerAnimated:YES completion:^(void){}];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"BrowseFilms"]) {
		BrowseFilmsViewController *browseFilms = (BrowseFilmsViewController *)[segue destinationViewController];
		browseFilms.managedObjectContext = _managedObjectContext;
	}
	
	if ([[segue identifier] isEqualToString:@"ViewSchedule"]) {
		ScheduleViewController *schedule = (ScheduleViewController *)[segue destinationViewController];
		schedule.managedObjectContext = _managedObjectContext;
	}
	
	if ([[segue identifier] isEqualToString:@"MySchedule"]) {
		MyScheduleViewController *myschedule = (MyScheduleViewController *)[segue destinationViewController];
		myschedule.managedObjectContext = _managedObjectContext;
	}
	
	if ([[segue identifier] isEqualToString:@"AboutFestival"]) {
		WebViewController *aboutFestival = (WebViewController *)[segue destinationViewController];
		NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"] isDirectory:NO];
		NSString *htmlFile = [url path];
		NSError *error;
		NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:&error];
		aboutFestival.urlToLoad = nil;
		aboutFestival.htmlToLoad = htmlString;
		aboutFestival.delegate = self;
	}
}

@end
