//
//  GridScheduleViewController.m
//  FestPro
//
//  Created by Christopher Hall on 11/26/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "GridScheduleViewController.h"
#import "GridPopoverContentViewController.h"
#import "ActionSheetPicker.h"
#import "Screenings.h"

@interface GridScheduleViewController ()

@end

@implementation GridScheduleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

	_lastContentOffsetX = 0;
	_lastContentOffsetY = 0;
	_hourViewWidth = 92.0;
	_locationHeight = 60.0;
	_currentFilm = nil;
	_currentScreening = nil;
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	self.title = [NSString stringWithFormat:@"Grid Schedule - %@ %@", [prefs objectForKey:@"festivalYear"], [prefs objectForKey:@"festivalName"] ];
	
	/*
	_legendButton = [[UIBarButtonItem alloc] initWithTitle:@"Legend" style:UIBarButtonItemStyleBordered target:self action:@selector(legendButtonTapped:)];
	_dateButton = [[UIBarButtonItem alloc] initWithTitle:@"Date" style:UIBarButtonItemStyleBordered target:self action:@selector(dateButtonTapped:)];
	self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:_dateButton, _legendButton, nil];
	 */
	 
	GridPopoverContentViewController *content = [self.storyboard instantiateViewControllerWithIdentifier:@"gridPopoverContent"];
	self.filmDetailPopover = [[UIPopoverController alloc] initWithContentViewController:content];
	self.filmDetailPopover.popoverContentSize = CGSizeMake(360.0, 360.0);
	self.filmDetailPopover.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

	// Set up references to views inside scrollviews
	UIView *dateTimeView = [[_dateTimeScrollView subviews] objectAtIndex:0];
	dateTimeView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"grid-hour-markers.png"]];
	UIView *locationView = [[_screeningLocationScrollView subviews] objectAtIndex:0];
	locationView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"grid-location-markers.png"]];
	UIView *gridView = [[_gridScrollView subviews] objectAtIndex:0];
	gridView.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"grid-grid-markers.png"]];
	
	// Generate labels for date/time scrollview
	_festivalDates = [[NSArray alloc] init];
	_festivalDates = [self findScreeningDaysInFestival];
	NSArray *hours = [NSArray arrayWithObjects:@"9 AM", @"10 AM", @"11 AM", @"12 PM", @"1 PM", @"2 PM", @"3 PM", @"4 PM", @"5 PM", @"6 PM", @"7 PM", @"8 PM", @"9 PM", @"10 PM", @"11 PM", nil];
	CGFloat leftMargin = 0.0;
	CGFloat daysOffset = 0.0;
	CGFloat hoursOffset = 0.0;
	CGFloat dayViewWidth = _hourViewWidth * 15;
	
	for (int x=0; x<[_festivalDates count]; x++) {
		UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(daysOffset, 0.0, dayViewWidth, 48.0)];
		[dayLabel setText: [_festivalDates objectAtIndex:x]];
		[dayLabel setFont:[UIFont fontWithName: @"Helvetica Bold" size: 17.0f]];
		[dayLabel setTextColor:UI_COLOR_03];
		[dayLabel setTextAlignment:NSTextAlignmentCenter];
		[dateTimeView addSubview:dayLabel];
		daysOffset = dayViewWidth * (x+1) + 0.0;
		
		for (int h=0; h<[hours count]; h++) {
			UILabel *hourLabel = [[UILabel alloc] initWithFrame:CGRectMake(hoursOffset, 40.0, _hourViewWidth, 24.0)];
			[hourLabel setText: [hours objectAtIndex:h]];
			[hourLabel setFont:[UIFont fontWithName: @"Helvetica" size: 13.0f]];
			[hourLabel setTextColor:UI_COLOR_03];
			[dateTimeView addSubview:hourLabel];
			hoursOffset = _hourViewWidth * (h+1) + leftMargin;
		}
		leftMargin = dayViewWidth * (x+1) + 0.0;
	}
	
	[_dateTimeScrollView setContentSize:CGSizeMake(daysOffset, 72.0)];
	CGRect dateTimeFrame = [dateTimeView frame];
	[dateTimeView setFrame:CGRectMake(dateTimeFrame.origin.x, dateTimeFrame.origin.y, daysOffset, 72.0)];
		
	// Generate labels for locations scrollview
	_festivalLocations = [[NSArray alloc] init];
	_festivalLocations = [self findScreeningLocationsInFestival];
	CGFloat locationsOffset = 0.0;
	
	for (int x=0; x<[_festivalLocations count]; x++) {
		UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0, locationsOffset, 90.0, _locationHeight)];
		[locationLabel setText: [_festivalLocations objectAtIndex:x]];
		[locationLabel setFont:[UIFont fontWithName: @"Helvetica" size: 13.0f]];
		[locationLabel setNumberOfLines:3];
		[locationLabel setTextAlignment:NSTextAlignmentRight];
		[locationLabel setTextColor:UI_COLOR_03];
		[locationView addSubview:locationLabel];
		locationsOffset = _locationHeight * (x+1) + 0.0;
	}
	
	if (locationsOffset < 1080.0) { locationsOffset = locationsOffset*2; }
	_screeningLocationScrollView.contentSize = CGSizeMake(100.0, locationsOffset);
	CGRect newLocationFrame = CGRectMake([locationView frame].origin.x, [locationView frame].origin.y, _screeningLocationScrollView.contentSize.width, _screeningLocationScrollView.contentSize.height);
	[locationView setFrame:newLocationFrame];
	
	_gridScrollView.contentSize = CGSizeMake(daysOffset, locationsOffset);
	
	CGRect gridFrame = [gridView frame];
	CGRect newGridFrame = CGRectMake(gridFrame.origin.x, gridFrame.origin.y, daysOffset, locationsOffset);
	[gridView setFrame:newGridFrame];
	[gridView setBounds:newGridFrame];
	
	// add films to grid
	[self addFilmstoGrid:gridView forLocations:(NSArray *)_festivalLocations withStartDate:(NSDate *)_startDate];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (IBAction)done: (id)sender
{
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	[super preferredInterfaceOrientationForPresentation];
	return UIInterfaceOrientationLandscapeLeft;
}

#pragma mark - GridScheduleViewController methods

- (void) addFilmstoGrid: (UIView *)view forLocations:(NSArray *)locations withStartDate:(NSDate *)date
{
	NSArray *results = [_fetchedResultsController fetchedObjects];
	CGFloat width = _hourViewWidth;
	CGFloat height = _locationHeight - 2;
	CGFloat ratio = _hourViewWidth / 60.0;
	CGFloat initialX = 0.0;
	CGFloat currentX = 0.0;
	CGFloat initialY = 0.0;
	CGFloat currentY = 0.0;
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSMutableArray *myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];
	
	int yCount = 1;
	for (NSString *thisLocation in locations) {
		for (Screenings *thisScreening in results) {
			if ([thisLocation isEqualToString:thisScreening.location]) {
				NSNumber *totalRuntime = thisScreening.totalRuntime;
				NSDate *filmStartDateTime = thisScreening.dateTime;
				
				NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
				[formatter setTimeZone:[NSTimeZone timeZoneWithName:kBaseTimezone]];
				[formatter setDateFormat:@"EEEE, MMMM d, yyyy"];
				NSString *filmDate = [formatter stringFromDate:filmStartDateTime];
				int daysCount = 0;
				for (NSString *string in _festivalDates) {
					if ([string isEqualToString:filmDate]) { break; }
					daysCount++;
				}
				
				NSTimeInterval hoursInterval = [thisScreening.dateTime timeIntervalSinceDate:thisScreening.dateString];
				int minutes = floor( hoursInterval / 60) - 540;
				currentX = initialX + ((width*15)*daysCount) + (minutes*ratio);
				
				UIView *filmView = [[UIView alloc] initWithFrame: CGRectMake(currentX+1, currentY+(2*yCount), ([totalRuntime intValue]*ratio), height)];
				[[filmView layer] setBorderColor:[[UIColor blackColor] CGColor]];
				[[filmView layer] setBorderWidth:1.0f];
				[[filmView layer] setCornerRadius: 5.0f];
				[[filmView layer] setMasksToBounds:YES];
				[filmView setTag:[[thisScreening screening_id] integerValue]];
				
				NSArray *films = [[thisScreening screenings_films] allObjects];
				NSString *eventType = [[films objectAtIndex:0] eventType];
				if ([eventType isEqualToString:@"Narrative Feature"] || [eventType isEqualToString:@"Narrative"]) {
					[filmView setBackgroundColor:UI_COLOR_NARRATIVE];
				} else if ([eventType isEqualToString:@"Documentary Feature"] || [eventType isEqualToString:@"Documentary"]) {
					[filmView setBackgroundColor:UI_COLOR_DOCUMENTARY];
				} else if ([eventType isEqualToString:@"Narrative Short"]) {
					[filmView setBackgroundColor:UI_COLOR_SEMINAR];
				} else if ([eventType isEqualToString:@"Documentary Short"]) {
					[filmView setBackgroundColor:UI_COLOR_SHORTFILM];
				} else {
					[filmView setBackgroundColor:UI_COLOR_DEFAULT];
				}
				
				UILabel *title = [[UILabel alloc] initWithFrame: CGRectMake(5.0, 5.0, (filmView.frame.size.width - 5.0), 36.0)];
				if (![thisScreening.programName isEqualToString:@""]) {
					[title setText:thisScreening.programName];
				} else {
					[title setText:thisScreening.films];
				}
				[title setFont:[UIFont fontWithName: @"Helvetica" size: 14.0f]];
				[title setNumberOfLines:0];
				[title sizeToFit];
				[filmView addSubview:title];
				
				UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPopover:)];
				[tap setNumberOfTapsRequired:1];
				[tap setDelegate:self];
				[filmView addGestureRecognizer:tap];
				
				// Add MyFest icon to appropriate film views
				for (NSNumber *screening_id in myFestArray) {
					if ([screening_id isEqualToNumber:thisScreening.screening_id]) {
						CGRect filmViewFrame = [filmView frame];
						UIImageView *myFestImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-myfest-add.png"]];
						[myFestImage setContentMode:UIViewContentModeScaleAspectFit];
						[myFestImage setFrame:CGRectMake((filmViewFrame.size.width - 24.0), (filmViewFrame.size.height - 24.0), 19.0, 19.0)];
						[filmView addSubview:myFestImage];
						[filmView bringSubviewToFront:myFestImage];
					}
				}
				[view addSubview:filmView];
			}
		}
		currentY = initialY + (height * yCount);
		yCount++;
	}
}

- (NSArray *) findScreeningDaysInFestival
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *startDateString = [NSString stringWithFormat:@"%@ %@ %@",[prefs objectForKey:@"festivalStartDate"], @"00:00:00", [prefs objectForKey:@"festivalTimeZone"]];
	NSString *endDateString = [NSString stringWithFormat:@"%@ %@ %@",[prefs objectForKey:@"festivalEndDate"], @"00:00:00", [prefs objectForKey:@"festivalTimeZone"]];
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setTimeZone:[NSTimeZone timeZoneWithName:kBaseTimezone]];
	[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZ"];
	NSDate *startDate = [formatter dateFromString:startDateString];
	NSDate *endDate = [formatter dateFromString:endDateString];
	NSDate *currentDate = startDate;
	[formatter setDateFormat:@"MMMM d, yyyy"];
	
	NSArray *test = [self.fetchedResultsController sections];
	
	NSMutableArray *festivalDates = [[NSMutableArray alloc] init];
	int day = 1; int addedDay = 1;
	while (!([currentDate compare:endDate] == NSOrderedDescending)) {
		[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZ"];
		int total = (uint32_t)[test count];
		if (addedDay-1 < total) {
			BOOL added = false;
			NSDate *sectionDate = [formatter dateFromString:[[test objectAtIndex:(addedDay-1)] name]];
			/* Eliminate dates between start and end which have no screenings. This is to keep the date picker in sync with the available sections. If there are more dates than sections, the extra ones will generate an out of bounds crash when selected, and the scrolling can become inaccurate.
			 */
			//NSLog(@"Current: %@, Section: %@, End: %@",currentDate,sectionDate,endDate);
			if ([currentDate isEqualToDate:sectionDate]) {
				[formatter setDateFormat:@"EEEE, MMMM d, yyyy"];
				[festivalDates addObject:[formatter stringFromDate:currentDate]];
				added = true;
			}
			currentDate = [self getForDays:day fromDate:startDate];
			day++;
			
			if (added) { addedDay++; }
		} else {
			currentDate = [self getForDays:day fromDate:startDate];
			day++;
		}
	}
	return [[NSArray alloc] initWithArray:festivalDates];
}

- (NSArray *) findScreeningLocationsInFestival
{
	NSMutableArray *festivalLocations = [[NSMutableArray alloc] init];
	NSArray *results = [self.fetchedResultsController fetchedObjects];
	int x = 0;
	for (Screenings *thisScreening in results) {
		if (x == 0) { _startDate = thisScreening.dateString; }
		if (![festivalLocations containsObject:thisScreening.location]) {
			[festivalLocations addObject:thisScreening.location];
		}
		x++;
	}
	[festivalLocations sortUsingSelector: @selector(localizedCaseInsensitiveCompare:)];
	return [[NSArray alloc] initWithArray:festivalLocations];
}

- (NSDate *)getForDays:(int)days fromDate:(NSDate *)date
{
    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setDay:days];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:components toDate:date options:0];
}

- (void) refreshMyFestViews
{
	UIView *gridView = [[_gridScrollView subviews] objectAtIndex:0];
	NSArray *gridSubViews = [gridView subviews];
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSMutableArray *myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];
	
	BOOL alreadyAdded, hasImageView;
	for (UIView *thisView in gridSubViews) {
		NSArray *filmSubViews = [thisView subviews];
		
		// Check to see if a view should have a MyFest icon but does not
		alreadyAdded = FALSE;
		for (NSNumber *screening_id in myFestArray) {
			if ([screening_id isEqualToNumber:[NSNumber numberWithInt:(uint32_t)thisView.tag]]) {
				//NSLog(@"%@", [thisView subviews]);
				hasImageView = FALSE;
				for (UIView *subView in filmSubViews) {
					if ([subView isKindOfClass:[UIImageView class]]) {
						hasImageView = TRUE;
					}
				}
				// Add a new MyFest icon
				if (hasImageView == FALSE) {
					CGRect filmViewFrame = [thisView frame];
					UIImageView *myFestImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-myfest-add.png"]];
					[myFestImage setContentMode:UIViewContentModeScaleAspectFit];
					[myFestImage setFrame:CGRectMake((filmViewFrame.size.width - 24.0), (filmViewFrame.size.height - 24.0), 19.0, 19.0)];
					[thisView addSubview:myFestImage];
					[thisView bringSubviewToFront:myFestImage];
				}
				alreadyAdded = TRUE;
			}
		}
		
		// View should not have a MyFest icon, remove it if it does
		if (alreadyAdded == FALSE) {
			for (UIView *subView in filmSubViews) {
				if ([subView isKindOfClass:[UIImageView class]]) {
					//hasImageView = TRUE;
					[subView removeFromSuperview];
				}
			}
		}
	}
}

#pragma mark - GridScheduleViewController Popovers

- (IBAction)showPopover:(id)sender
{
    // Set the sender (UITapGestureRecognizer) to a UIView. Convert view frame to current screen since it is in a scrollview.
    UIView *tappedView = (UIView *)[sender view];
	CGRect convertedFrame = [_gridScrollView convertRect:tappedView.frame toView:self.view];
	NSInteger tag = [tappedView tag];
	NSNumber *screeningId = [NSNumber numberWithInteger:tag];
	
	// Set the current film and current screening
	NSArray *results = [_fetchedResultsController fetchedObjects];
	for (Screenings *thisScreening in results) {
		if ([thisScreening.screening_id isEqualToNumber:screeningId]) {
			_currentScreening = thisScreening;
			NSArray *films = [[thisScreening screenings_films] allObjects];
			_currentFilm = [films objectAtIndex:0];
		}
	}

	GridPopoverContentViewController *content = [self.storyboard instantiateViewControllerWithIdentifier:@"gridPopoverContent"];
	content.currentFilm = _currentFilm;
	content.currentScreening = _currentScreening;

	// Present popover in the new way for iOS 8, the old way for iOS 7.
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
	{
		content.modalPresentationStyle = UIModalPresentationPopover;
		[self presentViewController:content animated:YES completion:nil];
		
		UIPopoverPresentationController *presentationController = [content popoverPresentationController];
		presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
		presentationController.sourceView = self.view;
		presentationController.sourceRect = convertedFrame;
	} else {
		[self.filmDetailPopover setContentViewController:content];
		[self.filmDetailPopover presentPopoverFromRect:convertedFrame
												inView:self.view
							  permittedArrowDirections:UIPopoverArrowDirectionAny
											  animated:YES];
	}
}

- (IBAction)legendButtonTapped:(UIBarButtonItem *)sender
{
}


- (IBAction)dateButtonTapped:(UIBarButtonItem *)sender
{
    [self selectFestivalDate:(UIControl *)sender];
}

- (IBAction)selectFestivalDate:(UIControl *)sender
{
    [ActionSheetStringPicker showPickerWithTitle:@"Select Date" rows:self.festivalDates initialSelection:self.selectedIndex target:self successAction:@selector(actionPickerDateSelected:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
}

- (void)actionPickerDateSelected:(id)sender
{
    //NSLog(@"Delegate has been informed that date was selected. Sender: %@", sender);
    self.selectedIndex = [sender intValue];
	
	// Scroll grid view to appropriate starting date
	int selectedIndex = [sender intValue];
	CGFloat dateX = (selectedIndex * (92.0*15.0));

	if (IS_LANDSCAPE) {
		[_gridScrollView scrollRectToVisible:CGRectMake(dateX, 0.0, 924.0, 400.0) animated:YES];
	} else {
		[_gridScrollView scrollRectToVisible:CGRectMake(dateX, 0.0, 668.0, 400.0) animated:YES];
	}
}

- (void)actionPickerCancelled:(id)sender
{
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - Popover Controller Delegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
	[self refreshMyFestViews];
}


#pragma mark - Scroll View Delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	BOOL horizontalScroll;

	if([scrollView isEqual:self.dateTimeScrollView]) {
		CGPoint offset = self.gridScrollView.contentOffset;
		offset.x = scrollView.contentOffset.x;
		[self.gridScrollView setContentOffset:offset];
	} else if ([scrollView isEqual:self.screeningLocationScrollView]) {
		CGPoint offset = self.gridScrollView.contentOffset;
		offset.y = scrollView.contentOffset.y;
		[self.gridScrollView setContentOffset:offset];
	} else {
		if (self.lastContentOffsetX == scrollView.contentOffset.x) {
			horizontalScroll = NO;
		} else {
			horizontalScroll = YES;
		}

		if (horizontalScroll) {
			CGPoint offset = self.dateTimeScrollView.contentOffset;
			offset.x = scrollView.contentOffset.x;
			[self.dateTimeScrollView setContentOffset:offset];
		} else {
			CGPoint offset = self.screeningLocationScrollView.contentOffset;
			offset.y = scrollView.contentOffset.y;
			[self.screeningLocationScrollView setContentOffset:offset];
		}
		
		self.lastContentOffsetX = scrollView.contentOffset.x;
		self.lastContentOffsetY = scrollView.contentOffset.y;
	}
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Screenings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateTime" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"dateString" cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	     */
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    
    return _fetchedResultsController;
}

@end
