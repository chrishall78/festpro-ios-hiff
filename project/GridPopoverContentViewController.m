//
//  GridPopoverContentViewController.m
//  FestPro
//
//  Created by Christopher Hall on 6/3/14.
//  Copyright (c) 2014 Christopher Hall Design. All rights reserved.
//

#import "GridPopoverContentViewController.h"
#import "WebViewController.h"
#import "PhotoVideo.h"

@interface GridPopoverContentViewController ()

@end

@implementation GridPopoverContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSMutableArray *myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];

	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	NSTimeZone *currentTimeZone = [NSTimeZone timeZoneWithName: kBaseTimezone];
	[dateFormatter setTimeZone:currentTimeZone];

	CGRect nameFrame, eventFrame, infoFrame, synopsisFrame, screeningFrame, buttonsFrame = CGRectMake(0.0, 0.0, 0.0, 0.0);
	BOOL filmProgram = NO;
	if (_currentFilm != nil) {
		if (_currentScreening != nil) {
			if (![_currentScreening.programName isEqualToString:@""]) {
				[_filmName setText:_currentScreening.programName];
				filmProgram = YES;
			} else {
				NSString *newTitle = [self processTitle:[_currentFilm titleEnglish]];
				[_filmName setText:newTitle];
			}
		}
		[_filmName sizeToFit];
		nameFrame = [_filmName frame];

		NSArray *films = [[_currentScreening screenings_films] allObjects];
		NSString *eventType = [[films objectAtIndex:0] eventType];
		UIColor *eventColor = UI_COLOR_DEFAULT;
		float eventOffset = 20.0;
		if ([eventType isEqualToString:@"Feature Narrative"] || [eventType isEqualToString:@"Narrative"]) {
			eventColor = UI_COLOR_NARRATIVE;
		} else if ([eventType isEqualToString:@"Feature Documentary"] || [eventType isEqualToString:@"Documentary"]) {
			eventColor = UI_COLOR_DOCUMENTARY;
		} else if ([eventType isEqualToString:@"Seminar"]) {
			eventColor = UI_COLOR_SEMINAR;
		} else if ([eventType isEqualToString:@"Short Film"]) {
			eventColor = UI_COLOR_SHORTFILM;
		}
		
		if (filmProgram) {
			_filmInformation.text = [NSString stringWithFormat:@"Various Directors | %@ min.", [_currentScreening totalRuntime]];
			[_filmInformation sizeToFit];
			infoFrame = [_filmInformation frame];
			[_filmInformation setFrame: CGRectMake(nameFrame.origin.x, (nameFrame.origin.y + nameFrame.size.height), infoFrame.size.width, infoFrame.size.height)];
			infoFrame = [_filmInformation frame];
			
			if ([eventColor isEqual:UI_COLOR_DEFAULT]) {
				eventOffset = 0.0;
			} else {
				UIView *eventColorBox = [[UIView alloc] initWithFrame:CGRectMake(nameFrame.origin.x, (infoFrame.origin.y + infoFrame.size.height), 16.0, 16.0)];
				[eventColorBox setBackgroundColor:eventColor];
				[[eventColorBox layer] setBorderColor:[[UIColor blackColor] CGColor]];
				[[eventColorBox layer] setBorderWidth:1.0];
				[_contentView addSubview:eventColorBox];
			}

			_filmEventType.text = eventType;
			[_filmEventType sizeToFit];
			eventFrame = [_filmEventType frame];
			[_filmEventType setFrame: CGRectMake(nameFrame.origin.x + eventOffset, (infoFrame.origin.y + infoFrame.size.height), eventFrame.size.width, eventFrame.size.height)];
			eventFrame = [_filmEventType frame];
			
			synopsisFrame = [_synopsisView frame];
			NSString *htmlSynopsis = [NSString stringWithFormat:@"%@%@%@", @"<p>Plays with: ", [_currentScreening films], @"</p><style>html { font-size: 0.8em; font-family:Helvetica, Arial, sans-serif; } html, body, p { padding: 0; } html, body { margin: 0; }</style>"];
			[_synopsisView loadHTMLString:htmlSynopsis baseURL:[[NSURL alloc] initWithString:kBaseWebURL] ];
			[_synopsisView setFrame:CGRectMake(nameFrame.origin.x, (eventFrame.origin.y + eventFrame.size.height + 5.0), synopsisFrame.size.width, synopsisFrame.size.height)];
			synopsisFrame = [_synopsisView frame];
		} else {
			_filmInformation.text = [NSString stringWithFormat:@"%@ %@ | %@ min.\nDirected by: %@", [_currentFilm countries], [_currentFilm year], [_currentFilm runtime], [_currentFilm directorName] ];
			[_filmInformation sizeToFit];
			infoFrame = [_filmInformation frame];
			[_filmInformation setFrame: CGRectMake(nameFrame.origin.x, (nameFrame.origin.y + nameFrame.size.height), infoFrame.size.width, infoFrame.size.height)];
			infoFrame = [_filmInformation frame];

			if ([eventColor isEqual:UI_COLOR_DEFAULT]) {
				eventOffset = 0.0;
			} else {
				UIView *eventColorBox = [[UIView alloc] initWithFrame:CGRectMake(nameFrame.origin.x, (infoFrame.origin.y + infoFrame.size.height), 16.0, 16.0)];
				[eventColorBox setBackgroundColor:eventColor];
				[[eventColorBox layer] setBorderColor:[[UIColor blackColor] CGColor]];
				[[eventColorBox layer] setBorderWidth:1.0];
				[_contentView addSubview:eventColorBox];
			}

			_filmEventType.text = eventType;
			[_filmEventType sizeToFit];
			eventFrame = [_filmEventType frame];
			[_filmEventType setFrame: CGRectMake(nameFrame.origin.x + eventOffset, (infoFrame.origin.y + infoFrame.size.height), eventFrame.size.width, eventFrame.size.height)];
			eventFrame = [_filmEventType frame];
			
			synopsisFrame = [_synopsisView frame];
			NSString *htmlSynopsis = [NSString stringWithFormat:@"%@%@", [_currentFilm synopsisShort], @"<style>html { font-size: 0.8em; font-family:Helvetica, Arial, sans-serif; } html, body, p { padding: 0; } html, body { margin: 0; }</style>"];
			[_synopsisView loadHTMLString:htmlSynopsis baseURL:[[NSURL alloc] initWithString:kBaseWebURL] ];
			[_synopsisView setFrame:CGRectMake(nameFrame.origin.x, (eventFrame.origin.y + eventFrame.size.height + 5.0), synopsisFrame.size.width, synopsisFrame.size.height)];
			synopsisFrame = [_synopsisView frame];
		}
	}
	
	if (_currentScreening != nil) {
		[dateFormatter setDateFormat:@"EEEE, MMMM d yyyy - h:mm a"];
		_screeningDateTime.text = [dateFormatter stringFromDate:[_currentScreening dateTime]];
		[_screeningDateTime sizeToFit];
		screeningFrame = [_screeningDateTime frame];
		[_screeningDateTime setFrame:CGRectMake(nameFrame.origin.x, (synopsisFrame.origin.y + synopsisFrame.size.height + 5.0), screeningFrame.size.width, screeningFrame.size.height)];
		screeningFrame = [_screeningDateTime frame];
		
		buttonsFrame = [_buttonsContainer frame];
		[_buttonsContainer setFrame:CGRectMake(nameFrame.origin.x, (screeningFrame.origin.y + screeningFrame.size.height + 10.0), buttonsFrame.size.width, buttonsFrame.size.height)];
		buttonsFrame = [_buttonsContainer frame];
		
		BOOL alreadyAdded = FALSE;
		for (NSNumber *screening_id in myFestArray) {
			if ([screening_id isEqualToNumber:[_currentScreening screening_id]]) {
				alreadyAdded = TRUE;
			}
		}
		if ([myFestArray count] == 0 || alreadyAdded == FALSE) {
			[_myFestButton setImage:[UIImage imageNamed:@"btn-add-myfest.png"]];
		} else {
			[_myFestButton setImage:[UIImage imageNamed:@"btn-rem-myfest.png"]];
		}

		// Add Tap recognizer to toggle MyFest status
		UITapGestureRecognizer *myFestToggle = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleMyFest:)];
		[_myFestButton addGestureRecognizer:myFestToggle];

		if ([[_currentScreening free] isEqualToNumber:[[NSNumber alloc] initWithInt:1]]) {
			[_buyTicketsButton setImage:[UIImage imageNamed:@"btn-free-scrn.png"]];
		} else if (![[_currentScreening ticketLink] isEqualToString:@""]) {
			if ([[_currentScreening rush] isEqualToNumber:[[NSNumber alloc] initWithInt:1]]) {
				[_buyTicketsButton setImage:[UIImage imageNamed:@"btn-standby-line.png"]];
			} else {
				[_buyTicketsButton setImage:[UIImage imageNamed:@"btn-buy-tickets.png"]];
			}

			// Add Tap recognizer to open ticket link
			UITapGestureRecognizer *buyTicketTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openWebView:)];
			[_buyTicketsButton addGestureRecognizer:buyTicketTap];
		}

		// Add Tap recognizer to open shopping cart
		UITapGestureRecognizer *shoppingCartTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openWebView:)];
		[_shoppingCartButton addGestureRecognizer:shoppingCartTap];
	}
	_finalViewSize = CGSizeMake((buttonsFrame.size.width + 40.0), (buttonsFrame.origin.y + buttonsFrame.size.height + 20.0));
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.preferredContentSize = _finalViewSize;

}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return (UIInterfaceOrientationMaskPortrait |
				UIInterfaceOrientationMaskLandscapeLeft |
				UIInterfaceOrientationMaskLandscapeRight);
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//	[super preferredInterfaceOrientationForPresentation];
//	return UIInterfaceOrientationPortrait;
//}

#pragma mark - GridPopoverContentViewController methods

- (IBAction)toggleMyFest:(id)sender
{
	UIImage *add = [UIImage imageNamed:@"btn-add-myfest.png"];
	UIImage *remove = [UIImage imageNamed:@"btn-rem-myfest.png"];

	if ([[_myFestButton image] isEqual:add]) {
		[_myFestButton setImage:remove];
	} else {
		[_myFestButton setImage:add];
	}
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSMutableArray *myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];
	NSMutableArray *myNewFestArray = [[NSMutableArray alloc] init];
	
	BOOL alreadyAdded = FALSE;
	if([myFestArray count] == 0) {
		// Empty array, just add it.
		[myFestArray addObject:_currentScreening.screening_id];
	} else {
		// Check to see if screening already exists to determine if it should be added or removed
		for (NSNumber *screening_id in myFestArray) {
			// Both film id & datetime match = remove existing screening
			if ([screening_id isEqualToNumber:_currentScreening.screening_id]) {
				alreadyAdded = TRUE;
				[myNewFestArray addObject:screening_id];
			}
		}
		
		// We scanned through the existing array and didn't find this screening - add it.
		if (alreadyAdded == FALSE) {
			[myFestArray addObject:_currentScreening.screening_id];
		}
	}
	for (NSNumber *screening_id in myNewFestArray) { [myFestArray removeObject:screening_id]; }
	
	// Update "MyFest" array with new version
	NSArray *myFestArrayPrefs = myFestArray;
	[prefs setObject:myFestArrayPrefs forKey:@"myFestScreenings"];
	[prefs synchronize];
}

- (NSString *)processTitle:(NSString *)title
{
    NSString *newTitle = title;
    if ([title length] >= 3) {
        if ([[title substringFromIndex:[title length] - 3] isEqualToString:@", A"]) {
            newTitle = [NSString stringWithFormat:@"%@%@",@"A ",[title substringToIndex:[title length] - 3]];
        }
    }
    if ([title length] >= 5) {
        if ([[title substringFromIndex:[title length] - 5] isEqualToString:@", THE"]) {
            newTitle = [NSString stringWithFormat:@"%@%@",@"THE ",[title substringToIndex:[title length] - 5]];
        }
    }
    return newTitle;
}

#pragma mark - WebViewController delegate

- (IBAction)openWebView:(id)sender
{
	UIView *view = [sender view];
	if ([view isEqual:_buyTicketsButton]) {
		_ticketLink = [_currentScreening ticketLink];
	} else if([view isEqual:_shoppingCartButton]) {
		_ticketLink = kShoppingCartURL;
	}
	[self performSegueWithIdentifier:@"launchWebView" sender:self];
}

- (void) dismissWebView
{
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

#pragma mark - Segue

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"launchWebView"]) {
        WebViewController *webView = (WebViewController *)[segue destinationViewController];
        webView.urlToLoad = _ticketLink;
        webView.htmlToLoad = nil;
		webView.delegate = self;
    }
}

@end
