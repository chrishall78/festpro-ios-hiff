//
//  WebViewController.h
//  iBoom
//
//  Created by Brian Stormont on 1/30/09.
//  Copyright 2009 Stormy Productions. All rights reserved.
//

@protocol WebViewController
@optional
- (void) dismissWebView;
@end

@interface WebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, retain) id delegate;
@property (nonatomic, strong) NSString *urlToLoad;
@property (nonatomic, strong) NSString *htmlToLoad;
@property (nonatomic, strong) IBOutlet UIView *uiView;
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *busyWebIcon;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *webForward;
@property (nonatomic, strong) IBOutlet UIToolbar *toolBar;

- (void) showNoNetworkAlert;
- (void) loadBrowser: (NSURL *) url;
- (IBAction) browseBack: (id) sender;
- (IBAction) browseForward: (id) sender;
- (IBAction) stopOrReLoadWeb: (id) sender;
- (IBAction) launchSafari: (id) sender;
- (IBAction) done: (id)sender;

@end
