//
//  ScheduleViewController.h
//  FestPro
//
//  Created by Christopher Hall on 1/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "ScheduleCell.h"

@interface ScheduleViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSMutableArray *festivalDates;
@property (strong, nonatomic) NSArray *filmProgram;
@property (strong, nonatomic) NSString *filmProgramName;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *dateButton;

- (IBAction)selectFestivalDate:(id)sender;
- (IBAction)dateButtonTapped:(UIBarButtonItem *)sender;
- (void)configureCell:(ScheduleCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (NSDate *)getForDays:(int)days fromDate:(NSDate *)date;
- (IBAction)toggleMyFestButton:(id)sender;
- (IBAction)showFilmProgram:(id)sender;
- (IBAction)showGridSchedule:(id)sender;

@end
