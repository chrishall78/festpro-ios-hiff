//
//  Sponsors.h
//  FestPro
//
//  Created by Christopher Hall on 2/19/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Films;

@interface Sponsors : NSManagedObject

@property (nonatomic, retain) NSNumber * film_id;
@property (nonatomic, retain) NSString * sponsorName;
@property (nonatomic, retain) NSString * sponsorDesc;
@property (nonatomic, retain) NSString * sponsorSiteUrl;
@property (nonatomic, retain) NSString * sponsorLogoUrl;
@property (nonatomic, retain) Films *sponsors_films;

@end
