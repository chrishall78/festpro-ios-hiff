//
//  Films.m
//  FestPro
//
//  Created by Christopher Hall on 2/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Films.h"
#import "Countries.h"
#import "Genres.h"
#import "Languages.h"
#import "Personnel.h"
#import "PhotoVideo.h"
#import "Screenings.h"
#import "Sponsors.h"

@implementation Films

@dynamic countries;
@dynamic directorName;
@dynamic eventType;
@dynamic film_id;
@dynamic genres;
@dynamic imagePath;
@dynamic imageUrl;
@dynamic languages;
@dynamic premiereType;
@dynamic runtime;
@dynamic section;
@dynamic slug;
@dynamic synopsisLong;
@dynamic synopsisShort;
@dynamic synopsisOriginal;
@dynamic titleAlpha;
@dynamic titleEnglish;
@dynamic titleRomanized;
@dynamic year;
@dynamic films_countries;
@dynamic films_genres;
@dynamic films_languages;
@dynamic films_personnel;
@dynamic films_photos;
@dynamic films_screenings;
@dynamic films_sponsors;

@end
