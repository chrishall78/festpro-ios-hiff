//
//  HomeViewController.h
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Films.h"
#import "Screenings.h"
#import "Personnel.h"
#import "Sponsors.h"
#import "PhotoVideo.h"

@interface HomeViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *popoverButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *aboutButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *updatesButton;

@property (strong, nonatomic) IBOutlet UIScrollView *backgroundScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *downloadUpdateButton;
@property (strong, nonatomic) IBOutlet UIButton *downloadButton;
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UILabel *progressTitle;
@property (strong, nonatomic) IBOutlet UILabel *progressLabel;
@property (strong, nonatomic) IBOutlet UIView *cornersView;

@property (strong, nonatomic) Films *thisFilm;
@property (strong, nonatomic) Films *thisScreeningFilm;
@property (strong, nonatomic) Screenings *thisScreening;
@property (strong, nonatomic) Personnel *thisPersonnel;
@property (strong, nonatomic) Sponsors *thisSponsor;
@property (strong, nonatomic) PhotoVideo *thisPhotoVideo;
@property (strong, nonatomic) NSMutableArray *filterArray;

- (IBAction)tryCheckingForUpdates:(id)sender;
- (NSString *)checkForUpdates;
- (IBAction)tryDownloadingData:(id)sender;
- (BOOL)getData;
- (IBAction)dismissView:(id)sender;
- (void)loadingProgress:(NSNumber *)nProgress;
- (void)deleteAllObjects:(NSString *)entityDescription;
- (NSArray *)selectAndSortFilterItems:(NSMutableArray *)results forType:(NSString *)type;

@end
