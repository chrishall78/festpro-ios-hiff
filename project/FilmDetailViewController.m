//
//  FilmDetailViewController.m
//  FestPro
//
//  Created by Christopher Hall on 1/30/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "FilmDetailViewController.h"
#import "FPNavigationController.h"
#import "WebViewController.h"
#import "Personnel.h"
#import "PhotoVideo.h"
#import "Sponsors.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"

@implementation FilmDetailViewController

@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;

AppDelegate *appDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController
	willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode {
	if (displayMode == UISplitViewControllerDisplayModePrimaryHidden) {
		[self.navigationItem setLeftBarButtonItem: splitController.displayModeButtonItem];
		[self.navigationItem.leftBarButtonItem setTitle:NSLocalizedString(@"Open List", @"Open List")];
	}
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
	// Called when the view is shown again in the split view, invalidating the button and popover controller.
	[self.navigationItem setLeftBarButtonItem:nil animated:YES];
	//self.masterPopoverController = nil;
}
- (IBAction)openSplitViewPopoverController:(id)sender {
	[_masterPopoverController presentPopoverFromBarButtonItem:_popoverButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - Gesture actions

- (IBAction)moveRight:(id)sender
{
    int selectedIndex = (uint32_t)[_segmentedControl selectedSegmentIndex];
    switch (selectedIndex) {
        case 0:
            [_detailsView setHidden:TRUE];
            [_synopsisView setHidden:FALSE];
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, (_synopsisView.frame.size.height + 250))];
            [_screeningView setHidden:TRUE];
            [_segmentedControl setSelectedSegmentIndex:1];
            break;
            
        case 1:
            [_detailsView setHidden:TRUE];
            [_synopsisView setHidden:TRUE];
            [_screeningView setHidden:FALSE];
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, (_screeningView.frame.size.height + 250))];
            [_segmentedControl setSelectedSegmentIndex:2];
            break;
            
        default:
            break;
    }
}

- (IBAction)moveLeft:(id)sender
{
    int selectedIndex = (uint32_t)[_segmentedControl selectedSegmentIndex];
    switch (selectedIndex) {
        case 1:
            [_detailsView setHidden:FALSE];
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, (_detailsView.frame.size.height + 250))];
            [_synopsisView setHidden:TRUE];
            [_screeningView setHidden:TRUE];
            [_segmentedControl setSelectedSegmentIndex:0];
            break;
            
        case 2:
            [_detailsView setHidden:TRUE];
            [_synopsisView setHidden:FALSE];
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, (_synopsisView.frame.size.height + 250))];
            [_screeningView setHidden:TRUE];
            [_segmentedControl setSelectedSegmentIndex:1];
            break;
            
        default:
            break;
    }
}


#pragma mark - View lifecycle
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
	self.splitViewController.delegate = self;
	
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _firstPhoto = nil;
    
	[[AFNetworkReachabilityManager sharedManager] startMonitoring];
	
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveRight:)];
        [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self.view addGestureRecognizer:swipeLeft];
        
        UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeft:)];
        [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
        [self.view addGestureRecognizer:swipeRight];
    } else {
		_detailsView.delegate = self;
		_synopsisView.delegate = self;
		_screeningView.delegate = self;
		
		_detailsView.scrollView.scrollEnabled = NO;
		_synopsisView.scrollView.scrollEnabled = NO;
		_screeningView.scrollView.scrollEnabled = NO;
	}

    // Make sure the film detail scrollView does scroll to the top, but the horizontal photos/videos one does not.
    _scrollView.scrollsToTop = YES;
    ((UIScrollView *) [[_scrollView subviews] objectAtIndex:0]).scrollsToTop = NO;
    
    // Process film title to include A or THE at the front
    self.title = [self processTitle:[_currentFilm titleEnglish]];
	
    // Image Scroll View
	CGFloat width = self.view.frame.size.width;
	CGFloat height = width * 0.56232427; // 16 x 9
    NSSet *photos = [_currentFilm films_photos]; // This includes both photo and video entries
	
	//NSLog(@"Photos: %lu | %@", (unsigned long)[photos count], photos );
	
    if ([photos count] == 0) {
        // Display static default photo
        NSArray *images = [NSArray arrayWithObjects:[UIImage imageNamed:@"default-photo.png"], nil];

		CGFloat width = self.view.frame.size.width;
		CGFloat height = width * 0.56232427; // 16 x 9
		CGRect imageRect = CGRectMake(0.0, 0.0, width, height);
		CGSize imageSize = CGSizeMake(width, height);

        UIImageView *subview = [[UIImageView alloc] initWithFrame:imageRect];
        subview.image = [images objectAtIndex:0];
        [self.imageScrollView addSubview:subview];        
        self.imageScrollView.contentSize = imageSize;
        
        // Image Page Control
        _imagePageControlBeingUsed = NO;
        [self.imagePageControl setCurrentPage:0];
        [self.imagePageControl setNumberOfPages:1];
        [self.imagePageControl setHidden:TRUE];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
			[self.imagePageControl setFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 40.0)];
			[self.pageControlView addSubview:_imagePageControl];
		} else {
			[self.imagePageControl setFrame:CGRectMake(0.0, self.imageScrollView.frame.origin.y + self.imageScrollView.frame.size.height + 8.0, self.view.frame.size.width, 20.0)];
			[self.scrollView addSubview:_imagePageControl];
		}
	} else {
        NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES];
        NSArray *sortedPhotos = [photos sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
		
		//NSLog(@"Sorted Photos: %@",sortedPhotos);
        
        // Display all videos for film
        _videoUrl = @"";
		_hasVideo = FALSE;
		BOOL videoAdded = FALSE;
        for (PhotoVideo *thisVideo in sortedPhotos) {
            if ([thisVideo.type isEqualToString:@"video"] && [thisVideo.videoService isEqualToString:@"youtube"]) {
				BOOL shortUrl = FALSE;
				NSRange textRange = [thisVideo.videoUrl rangeOfString:@"youtu.be"];
				if (textRange.location != NSNotFound) { shortUrl = TRUE; }
				
				if (shortUrl) {
					NSArray *videoTemp = [thisVideo.videoUrl componentsSeparatedByString:@"/"];
					_videoUrl = [NSString stringWithFormat:@"http://www.youtube.com/embed/%@",[videoTemp objectAtIndex:3]];
					_hasVideo = TRUE;
				} else {
					NSArray *videoTemp = [thisVideo.videoUrl componentsSeparatedByString:@"v="];
					NSArray *videoTemp2 = [[videoTemp objectAtIndex:1] componentsSeparatedByString:@"&"];
					_videoUrl = [NSString stringWithFormat:@"http://www.youtube.com/embed/%@",[videoTemp2 objectAtIndex:0]];
					_hasVideo = TRUE;
				}
            }
            if ([thisVideo.type isEqualToString:@"video"] && [thisVideo.videoService isEqualToString:@"vimeo"]) {
                NSArray *videoTemp = [thisVideo.videoUrl componentsSeparatedByString:@"/"];                
                _videoUrl = [NSString stringWithFormat:@"http://player.vimeo.com/video/%@",[videoTemp lastObject]];
                _hasVideo = TRUE;
            }

            if (_hasVideo == TRUE && videoAdded == FALSE) {
				CGFloat width = self.view.frame.size.width;
				CGFloat height = width * 0.56232427; // 16 x 9
                _trailerView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, height)];
                _trailerView.scrollView.bounces = NO;
                [_trailerView setOpaque:NO];
                [self.imageScrollView addSubview:_trailerView];
				videoAdded = TRUE;
            }
        }        
                
        // Display all photos for film
        NSMutableArray *images = [[NSMutableArray alloc] init];
		NSURL *photoURL = nil;
        for (PhotoVideo *thisPhoto in sortedPhotos) {
            if ([thisPhoto.type isEqualToString:@"photo"]) {
				NSString *photoLargeUrl = [thisPhoto.photoLargeUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				NSString *photoXLargeUrl = [thisPhoto.photoXLargeUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				
				if ([photoXLargeUrl isEqualToString:@""]) {
					photoURL = [NSURL URLWithString:photoLargeUrl];
				} else {
					photoURL = [NSURL URLWithString:photoXLargeUrl];
				}
				if (photoURL) {
					[images addObject:photoURL];
				} else {
					NSLog(@"'%@' is not a valid URL", photoLargeUrl);
				}
            }
        }

		for (int i = 0; i < images.count; i++) {
            CGRect frame;
            if (_hasVideo == TRUE) {
                frame.origin.x = (width * i) + width;
            } else {
                frame.origin.x = width * i;
            }
            frame.origin.y = 0;
            frame.size = CGSizeMake(width, height);
			
			UIImageView *subview = [[UIImageView alloc] initWithFrame:frame];
			[subview setImageWithURL: [images objectAtIndex:i]];
			[self.imageScrollView addSubview:subview];
			
//            // Set the UIImage _firstPhoto when it finished loading
//            if (i == 0) {
//                [[NSNotificationCenter defaultCenter] addObserver:self
//                                                         selector:@selector(retrieveFirstPhoto:)
//                                                             name:AsyncImageLoadDidFinish
//                                                           object:nil];
//            }
        }

        int count = 0;
        if (_hasVideo == TRUE) { count = 1; }		
		if (IS_LANDSCAPE) {
			self.imageScrollView.contentSize = CGSizeMake(704.0 * (images.count + count), 402.0);
		} else {
			self.imageScrollView.contentSize = CGSizeMake(width * (images.count + count), height);
		}

		// Add height constraint to imageScrollView once we know how tall it should be. (phone only)
		if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
			[_imageScrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
			NSString *visualFormat = [NSString stringWithFormat:@"V:[_imageScrollView(==%f)]",height];
			[_imageScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:visualFormat options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imageScrollView)]];
		}

        // Image Page Control
        _imagePageControlBeingUsed = NO;
        [self.imagePageControl setCurrentPage:0];
        [self.imagePageControl addTarget: self action: @selector(changePage:) forControlEvents: UIControlEventValueChanged];
        if (_hasVideo == TRUE) {
            [self.imagePageControl setNumberOfPages:(images.count + 1)];
        } else {
            [self.imagePageControl setNumberOfPages:images.count];
            if ([images count] == 1) {
                [self.imagePageControl setHidden:TRUE];
            }
        }
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
			[self.imagePageControl setFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 30.0)];
			//[self.pageControlView addSubview:_imagePageControl];
		} else {
			[self.imagePageControl setFrame:CGRectMake(0.0, self.imageScrollView.frame.origin.y + self.imageScrollView.frame.size.height + 8.0, self.view.frame.size.width, 20.0)];
			[self.scrollView addSubview:_imagePageControl];
			
		}
    }

	// Add height constraint to imageScrollView once we know how tall it should be. (phone only)
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		[_imageScrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
		NSString *visualFormat = [NSString stringWithFormat:@"V:[_imageScrollView(==%f)]",height];
		[_imageScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:visualFormat options:0 metrics:nil views:NSDictionaryOfVariableBindings(_imageScrollView)]];
	}

	// Segmented Control
	//self.segmentedControl = [[UISegmentedControl alloc] init];
	self.segmentedControl.frame = CGRectMake(8.0, self.imagePageControl.frame.origin.y + self.imagePageControl.frame.size.height + 8.0, self.view.frame.size.width-8.0, 29.0);
	
    CGRect detailsViewFrame, synopsisViewFrame, screeningViewFrame;
	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		CGFloat width = self.view.frame.size.width;
		CGFloat top = self.segmentedControl.frame.origin.y + self.segmentedControl.frame.size.height + 8.0;
		
		// Individual Content Views
		detailsViewFrame = CGRectMake(8.0, top, width-8.0, 600.0);
		_detailsView = [[UIWebView alloc] initWithFrame:detailsViewFrame];
		_detailsView.delegate = self;
		_detailsView.backgroundColor = UI_COLOR_05;
		_detailsView.scrollView.scrollEnabled = NO;
		_detailsView.scrollView.bounces = NO;
		[_detailsView setHidden:FALSE];
		[_scrollView addSubview:_detailsView];
		
		synopsisViewFrame = CGRectMake(8.0, top, width-8.0, 600.0);
		_synopsisView = [[UIWebView alloc] initWithFrame:synopsisViewFrame];
		_synopsisView.delegate = self;
		_synopsisView.backgroundColor = UI_COLOR_05;
		_synopsisView.scrollView.scrollEnabled = NO;
		_synopsisView.scrollView.bounces = NO;
		[_synopsisView setHidden:TRUE];
		[_scrollView addSubview:_synopsisView];

		NSString *synopsisStyles = kiPhoneSynopsisStyles;
		NSString *synopsisHtml = @"";
		if ([[_currentFilm synopsisOriginal] isEqualToString:@""]) {
			synopsisHtml = [NSString stringWithFormat:@"%@ %@",synopsisStyles,[_currentFilm synopsisLong]];
		} else {
			synopsisHtml = [NSString stringWithFormat:@"%@ %@<br><br>%@",synopsisStyles,[_currentFilm synopsisLong],[_currentFilm synopsisOriginal]];
		}
		[_synopsisView loadHTMLString:synopsisHtml baseURL:[NSURL URLWithString:kBaseWebURL]];

		screeningViewFrame = CGRectMake(8.0, top, width-8.0, 600.0);
		_screeningView = [[UIWebView alloc] initWithFrame:screeningViewFrame];
		_screeningView.delegate = self;
		_screeningView.backgroundColor = UI_COLOR_05;
		_screeningView.scrollView.scrollEnabled = NO;
		_screeningView.scrollView.bounces = NO;
		[_screeningView setHidden:TRUE];
		[_scrollView addSubview:_screeningView];
	} else {
		NSString *synopsisStyles = kiPhoneSynopsisStyles;
		NSMutableString *synopsisHtml = [NSMutableString stringWithString:@""];
		[synopsisHtml appendString:@"<h3>Film Synopsis</h3>"];

		if ([[_currentFilm synopsisOriginal] isEqualToString:@""]) {
			[synopsisHtml appendString:[NSString stringWithFormat:@"%@ %@",synopsisStyles,[_currentFilm synopsisLong]]];
		} else {
			[synopsisHtml appendString:[NSString stringWithFormat:@"%@ %@<br><br>%@",synopsisStyles,[_currentFilm synopsisLong],[_currentFilm synopsisOriginal]]];
		}
		[synopsisHtml appendString:@"<p>&nbsp;</p>"];
		[_synopsisView loadHTMLString:synopsisHtml baseURL:[NSURL URLWithString:kBaseWebURL]];
		
		if (IS_LANDSCAPE) {
			_imageScrollView.frame = CGRectMake(0.0, 0.0, 704.0, 402.0);
			_pageControlView.frame = CGRectMake(0.0, 402.0, 704.0, 30.0);
			_imagePageControl.frame = CGRectMake(0.0, 0.0, 704.0, 30.0);
			NSArray *imageViews = [_imageScrollView subviews];
			for (int i=0; i < [imageViews count]; i++) {
				if ([imageViews[i] isMemberOfClass:[UIWebView class]]) {
					UIWebView *trailerView = imageViews[i];
					trailerView.frame = CGRectMake(0.0, 0.0, 704.0, 402.0);
				}
				if ([imageViews[i] isMemberOfClass:[UIImageView class]]) {
					UIImageView *photoView = imageViews[i];
					photoView.frame = CGRectMake(704.0 * i, 0.0, 704.0, 402.0);
				}
			}
			_detailsView.frame = CGRectMake(0.0, 432.0, 352.0, _detailsView.frame.size.height);
			_screeningView.frame = CGRectMake(352.0, 432.0, 352.0, _screeningView.frame.size.height);
			_synopsisView.frame = CGRectMake(0.0, (_detailsView.frame.origin.y + _detailsView.frame.size.height), 704.0, _synopsisView.frame.size.height);
		}
		
		screeningViewFrame = _screeningView.frame;
		[_detailsView setHidden:FALSE];
		[_synopsisView setHidden:FALSE];
		[_screeningView setHidden:FALSE];
	}
	
	/* Details View - Film Labels */
	NSMutableString *detailViewHtml = [NSMutableString stringWithString:@""];
	NSString *detailStyles = kiPhoneDetailsStyles;
	[detailViewHtml appendString:detailStyles];

	if (![[_currentFilm titleRomanized] isEqualToString:@""]) {
		[detailViewHtml appendFormat:@"<p class='title'>%@</p>", [NSString stringWithFormat:@"%@ (%@)",self.title,[_currentFilm titleRomanized]]];
	} else {
		[detailViewHtml appendFormat:@"<p class='title'>%@</p>", [NSString stringWithFormat:@"%@",self.title]];
	}
	[detailViewHtml appendFormat:@"<p>%@</p>", [_currentFilm section]];
	[detailViewHtml appendFormat:@"<p>%@</p>", [NSString stringWithFormat:@"%@ %@ | %@ min",[_currentFilm countries], [_currentFilm year], [_currentFilm runtime]]];
	if (![[_currentFilm languages] isEqual: @""]) {
		[detailViewHtml appendFormat:@"<p>%@</p>", [_currentFilm languages]];
	}
	if (![[_currentFilm genres] isEqual: @""]) {
		[detailViewHtml appendFormat:@"<p>%@</p>", [_currentFilm genres]];
	}
	[detailViewHtml appendFormat:@"<p>%@</p>", [_currentFilm premiereType]];
	
	/* Details View - Sponsors */
	NSSet *sponsors = [_currentFilm films_sponsors];
	if ([sponsors count]) {
		[detailViewHtml appendString:@"<h3>Sponsored by:</h3><div class='table'>"];

		NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"sponsorName" ascending:YES];
		NSArray *sortedSponsors = [sponsors sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
		int i = 0;
		
		for (Sponsors *thisSponsor in sortedSponsors) {
			if ([[thisSponsor film_id] intValue] == [[_currentFilm film_id] intValue]) {
				if (i % 2 == 0) { [detailViewHtml appendString:@"<div class='row'>"]; }
				if (![[thisSponsor sponsorLogoUrl] isEqualToString:@""]) {
					[detailViewHtml appendFormat:@"<div><a href='%@'><img class='sponsorLogo' src='%@' width='135'></a></div>", [thisSponsor sponsorSiteUrl], [thisSponsor sponsorLogoUrl]];
				} else {
					if (![[thisSponsor sponsorSiteUrl] isEqualToString:@""]) {
						[detailViewHtml appendFormat:@"<div><a href='%@'>%@</a></div>", [thisSponsor sponsorSiteUrl], [thisSponsor sponsorName]];
					} else {
						[detailViewHtml appendFormat:@"<div>%@</div>", [thisSponsor sponsorName]];
					}
				}
				i++;
				if (i % 2 == 0) { [detailViewHtml appendString:@"</div>"]; }
			}
		}
		if (i % 2 != 0) { [detailViewHtml appendString:@"</div>"]; }
		[detailViewHtml appendString:@"</div>"];
	}
	
	/* Details View - Personnel Labels & Content */
	NSSet *personnel = [_currentFilm films_personnel];
	if (![personnel count]) {
		[detailViewHtml appendString:@"<h3>Cast &amp; Crew</h4>"];
		[detailViewHtml appendString:@"<p>There is no cast/crew info available for this film.</p>"];
	} else {
		[detailViewHtml appendString:@"<h3>Cast &amp; Crew</h4>"];
		NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES];
		NSArray *sortedPersonnel = [personnel sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];

		NSString *directorList = @"";
		NSString *writerList = @"";
		NSString *producerList = @"";
		NSString *cintogList = @"";
		NSString *castList = @"";

		for (Personnel *thisPerson in sortedPersonnel) {
			NSArray *myRoles = [[thisPerson role] componentsSeparatedByString:@", "];
			NSString *firstName = [[thisPerson firstName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
			NSString *lastName = [[thisPerson lastName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
			NSString *personName = [NSString stringWithFormat:@"%@ %@",firstName,lastName];

			for (NSString *thisRole in myRoles) {
				if([thisRole isEqualToString:@"Director"]) {
					directorList = [NSString stringWithFormat:@"%@%@, ",directorList,personName];
				}
				else if([thisRole isEqualToString:@"Screenwriter"]) {
					writerList = [NSString stringWithFormat:@"%@%@, ",writerList,personName];
				}
				else if([thisRole isEqualToString:@"Producer"]) {
					producerList = [NSString stringWithFormat:@"%@%@, ",producerList,personName];
				}
				else if([thisRole isEqualToString:@"Cinematographer"]) {
					cintogList = [NSString stringWithFormat:@"%@%@, ",cintogList,personName];
				}
				else if([thisRole isEqualToString:@"Cast"]) {
					castList = [NSString stringWithFormat:@"%@%@, ",castList,personName];
				}
			}
		}
	
		// Trim the commas and space from the end of the string
		directorList = [directorList stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]];
		writerList = [writerList stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]];
		producerList = [producerList stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]];
		cintogList = [cintogList stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]];
		castList = [castList stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]];
		
		if ([directorList length] > 0) {
			[detailViewHtml appendFormat:@"<p class='personnel'><span>Director:</span><br>%@</p>", directorList];
		}
		if ([writerList length] > 0) {
			[detailViewHtml appendFormat:@"<p class='personnel'><span>Screenwriter:</span><br>%@</p>", writerList];
		}
		if ([producerList length] > 0) {
			[detailViewHtml appendFormat:@"<p class='personnel'><span>Producer:</span><br>%@</p>", producerList];
		}
		if ([cintogList length] > 0) {
			[detailViewHtml appendFormat:@"<p class='personnel'><span>Cinematographer:</span><br>%@</p>", cintogList];
		}
		if ([castList length] > 0) {
			[detailViewHtml appendFormat:@"<p class='personnel'><span>Cast:</span><br>%@</p>", castList];
		}

	}
	
	[_detailsView loadHTMLString:detailViewHtml baseURL:[NSURL URLWithString:kBaseWebURL]];


	/* Screening View */
	_screenings = [_currentFilm films_screenings];
	NSMutableSet *screenings2 = [[NSMutableSet alloc] init];
	if ([_screenings count] == 0) {
		// test to see if this is part of a film program
		NSMutableArray *results = [[NSMutableArray alloc] initWithArray:self.fetchedResultsController.fetchedObjects];
		for (Screenings *item in results) {
			NSArray *filmIds = [item.film_ids componentsSeparatedByString:@","];
			for (id film_id in filmIds) {
				NSNumber *thisFilmId = [_currentFilm film_id];
				NSNumber *testFilmId = [[NSNumber alloc] initWithInt: [film_id intValue]];
				if ([testFilmId isEqualToNumber:thisFilmId]) {
					[screenings2 addObject:item];
				}
			}
		}
	}
	
	if ([_screenings count] == 0 && [screenings2 count] != 0) {
		_screenings = screenings2;
	}
	
	NSMutableString *screeningViewHtml = [NSMutableString stringWithString:@""];
	NSString *screeningStyles = kiPhoneScreeningStyles;
	[screeningViewHtml appendString:screeningStyles];
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		[screeningViewHtml appendString:@"<h3>Screenings</h3>"];
	}
	
	if ([_screenings count] == 0) {
		//[screeningViewHtml appendString:@"<p>There are no screenings available for this film.</p>"];
		[screeningViewHtml appendString:@"<p>The festival schedule will be released on or around April 2015. Please check for updates at that time.</p>"];
	} else {
		NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:YES];
		NSArray *sorted = [_screenings sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];

		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		NSMutableArray *myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];

		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		NSTimeZone *currentTimeZone = [NSTimeZone timeZoneWithName: kBaseTimezone];
		[dateFormatter setTimeZone:currentTimeZone];

		for (Screenings *thisScreening in sorted) {
			[dateFormatter setDateFormat:@"EEEE, MMMM d, yyyy"];
			NSString *dateText = [dateFormatter stringFromDate:[thisScreening dateTime]];
			[screeningViewHtml appendFormat:@"<h4>%@</h4>",dateText];

			NSTimeInterval filmRuntime = [[_currentFilm runtime] intValue]*60.0;
			NSDate *filmEndtime = [NSDate dateWithTimeInterval:filmRuntime sinceDate:[thisScreening dateTime]];
			[dateFormatter setDateFormat:@"h:mm a"];
			NSString *timeLocFormat = [NSString stringWithFormat:@"%@ - %@ | %@", [dateFormatter stringFromDate:[thisScreening dateTime]], [dateFormatter stringFromDate:filmEndtime], [thisScreening location]];
			[screeningViewHtml appendFormat:@"<p>%@</p>",timeLocFormat];
			[screeningViewHtml appendString:@"<div class=\"clearfix\" style=\"margin:5px 0 20px 0;\">"];

			BOOL alreadyAdded = FALSE;
			for (NSNumber *screening_id in myFestArray) {
				if ([screening_id isEqualToNumber:[thisScreening screening_id]]) {
					alreadyAdded = TRUE;
				}
			}
			if ([myFestArray count] == 0 || alreadyAdded == FALSE) {
				[screeningViewHtml appendFormat:@"<div class=\"btn add-myfest\"><a href=\"myfest://%@\" onClick=\"toggleMyFest(this);\">Add to MyFest</a></div>",[thisScreening screening_id]];
			} else {
				[screeningViewHtml appendFormat:@"<div class=\"btn rem-myfest\"><a href=\"myfest://%@\" onClick=\"toggleMyFest(this);\">Remove from MyFest</a></div>",[thisScreening screening_id]];
			}
			
			if ([[thisScreening free] isEqualToNumber:[[NSNumber alloc] initWithInt:1]]) {
				[screeningViewHtml appendString:@"<div class=\"btn free-scrn\"><a href=\"javascript:void(0);\">Free Screening</a></div>"];
			} else if (![[thisScreening ticketLink] isEqualToString:@""]) {
				if ([[thisScreening rush] isEqualToNumber:[[NSNumber alloc] initWithInt:1]]) {
					[screeningViewHtml appendFormat:@"<div class=\"btn standby-line\"><a href=\"%@\">Standby Line</a></div>",[thisScreening ticketLink]];
				} else {
					[screeningViewHtml appendFormat:@"<div class=\"btn buy-tickets\"><a href=\"%@\">Buy Tickets</a></div>",[thisScreening ticketLink]];                        
				}
			}                
			[screeningViewHtml appendString:@"</div>"];
		}
		if (![kShoppingCartURL isEqualToString:@""]) {
			[screeningViewHtml appendFormat:@"<div class=\"btn shop-cart\"><a href=\"%@\">Shopping Cart</a></div>", kShoppingCartURL];
		}
	}
	[_screeningView loadHTMLString:screeningViewHtml baseURL:[[NSBundle mainBundle] bundleURL]];

	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		_screeningView.frame = screeningViewFrame;

		// Set Scroll View Content Size to 250 + Details View Size
		//[self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, _detailsView.frame.size.height + 250)];
	}
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		// Segmented Control
		[_segmentedControl addTarget:self
							  action:@selector(changeSubview:)
					forControlEvents:UIControlEventValueChanged];
		//[_segmentedControl setSelectedSegmentIndex:0];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoStarted:) name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
		/*  Bug: these notifications are never called when launching a movie player from a UIWebView
		 Solution: put the ending code in viewDidAppear:animated instead.
		 
		 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoFinished:) name:@"UIMoviePlayerControllerWillExitFullscreenNotification" object:nil];
		 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoFinished:) name:@"UIMoviePlayerControllerDidExitFullscreenNotification" object:nil];
		 */
	}

    if (_hasVideo == TRUE) {
        [_trailerView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_videoUrl]]];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		if (appDelegate.fullScreenVideoIsPlaying == YES) {
			appDelegate.fullScreenVideoIsPlaying = NO;
			
			// CODE BELOW FORCES APP BACK TO PORTRAIT ORIENTATION ONCE YOU LEAVE VIDEO. - DEPRECATED IN IOS9
			//[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
			
			// present/dismiss viewcontroller in order to activate rotating.
			UIViewController *mVC = [[UIViewController alloc] init];
			[self presentViewController:mVC animated:NO completion:^{
				[self dismissViewControllerAnimated:NO completion:^{
				}];
			}];
			
		}
	}

    if (_currentFilm == nil) {
        //NSLog(@"No Film Defined");
    } else {
        // Record mobile view for this film
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			NSString *appView = [NSString stringWithFormat:@"%@%@%@",kBaseWebURL,@"/films/appview/",[_currentFilm slug]];
			NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:appView]];
			
			[[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
				_receivedData = [NSMutableData data];
			}];
        });
    }

}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return YES;
	} else {
		return NO;
	}
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return (UIInterfaceOrientationMaskPortrait |
				UIInterfaceOrientationMaskLandscapeLeft |
				UIInterfaceOrientationMaskLandscapeRight);
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	[super preferredInterfaceOrientationForPresentation];
	return UIInterfaceOrientationPortrait;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
			// Rotating from Landscape to Portrait
			_imageScrollView.frame = CGRectMake(0.0, 0.0, 768.0, 439.0);
			_pageControlView.frame = CGRectMake(0.0, 439.0, 768.0, 30.0);
			_imagePageControl.frame = CGRectMake(0.0, 0.0, 768.0, 30.0);
			NSArray *imageViews = [_imageScrollView subviews];
			int subviewCount = 0;
			for (int i=0; i<[imageViews count]; i++) {
				if ([imageViews[i] isMemberOfClass:[UIWebView class]]) {
					UIWebView *trailerView = imageViews[i];
					trailerView.frame = CGRectMake(0.0, 0.0, 768.0, 439.0);
					subviewCount++;
				}
				if ([imageViews[i] isMemberOfClass:[UIImageView class]]) {
					UIImageView *photoView = imageViews[i];
					photoView.frame = CGRectMake(768.0 * i, 0.0, 768.0, 439.0);
					subviewCount++;
				}
			}
			self.imageScrollView.contentSize = CGSizeMake(self.imageScrollView.frame.size.width * subviewCount, self.imageScrollView.frame.size.height);
			
			NSString *output = [_synopsisView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"];
			CGFloat height = [output floatValue] + 40.0f;
			_detailsView.frame = CGRectMake(0.0, 469.0, 384.0, _detailsView.frame.size.height);
			_screeningView.frame = CGRectMake(384.0, 469.0, 384.0, _screeningView.frame.size.height);
			_synopsisView.frame = CGRectMake(0.0, (_detailsView.frame.origin.y + _detailsView.frame.size.height), 768.0, height);
			self.scrollView.contentSize = CGSizeMake(768.0, (_synopsisView.frame.origin.y + _synopsisView.frame.size.height));
		} else {
			// Rotating from Portrait to Landscape
			_imageScrollView.frame = CGRectMake(0.0, 0.0, 704.0, 402.0);
			_pageControlView.frame = CGRectMake(0.0, 402.0, 704.0, 30.0);
			_imagePageControl.frame = CGRectMake(0.0, 0.0, 704.0, 30.0);
			NSArray *imageViews = [_imageScrollView subviews];
			int subviewCount = 0;
			for (int i=0; i<[imageViews count]; i++) {
				if ([imageViews[i] isMemberOfClass:[UIWebView class]]) {
					UIWebView *trailerView = imageViews[i];
					trailerView.frame = CGRectMake(0.0, 0.0, 704.0, 402.0);
					subviewCount++;
				}
				if ([imageViews[i] isMemberOfClass:[UIImageView class]]) {
					UIImageView *photoView = imageViews[i];
					photoView.frame = CGRectMake(704.0 * i, 0.0, 704.0, 402.0);
					subviewCount++;
				}
			}
			self.imageScrollView.contentSize = CGSizeMake(704.0 * subviewCount, 402.0);
			
			NSString *output = [_synopsisView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"];
			CGFloat height = [output floatValue] + 40.0f;
			_detailsView.frame = CGRectMake(0.0, 432.0, 352.0, _detailsView.frame.size.height);
			_screeningView.frame = CGRectMake(352.0, 432.0, 352.0, _screeningView.frame.size.height);
			_synopsisView.frame = CGRectMake(0.0, (_detailsView.frame.origin.y + _detailsView.frame.size.height), 704.0, height);
			self.scrollView.contentSize = CGSizeMake(704.0, (_synopsisView.frame.origin.y + _synopsisView.frame.size.height));
		}
	}
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

-(void)videoStarted:(NSNotification *)notification{
	// Video entered full screen mode
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	appDelegate.fullScreenVideoIsPlaying = YES;
}

-(void)videoFinished:(NSNotification *)notification{
	// Video left fullscreen mode
	AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	appDelegate.fullScreenVideoIsPlaying = NO;

	// CODE BELOW FORCES APP BACK TO PORTRAIT ORIENTATION ONCE YOU LEAVE VIDEO. - DEPRECATED IN IOS9
	//[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];

    // present/dismiss viewcontroller in order to activate rotating.
    UIViewController *mVC = [[UIViewController alloc] init];
	[self presentViewController:mVC animated:NO completion:^{
		[self dismissViewControllerAnimated:NO completion:^{
		}];
	}];
}


#pragma mark - FilmDetailViewController Methods

//- (void)retrieveFirstPhoto:(NSNotification *)notification {
//    NSDictionary *imageCache = notification.userInfo;
//    if (_firstPhoto == nil) {
//        _firstPhoto = [imageCache objectForKey:AsyncImageImageKey];
//    }
//}

- (IBAction)changePage:(id)sender
{
    // update the scroll view to the appropriate page
    CGRect frame;
	
	if (IS_LANDSCAPE) {
		frame.origin.x = 704.0 * self.imagePageControl.currentPage;
		frame.origin.y = 0;
		frame.size = CGSizeMake(704.0, 402.0);
	} else {
		frame.origin.x = self.imageScrollView.frame.size.width * self.imagePageControl.currentPage;
		frame.origin.y = 0;
		frame.size = self.imageScrollView.frame.size;
	}
	[self.imageScrollView scrollRectToVisible:frame animated:YES];
    
	// Keep track of when scrolls happen in response to the page control
	// value changing. If we don't do this, a noticeable "flashing" occurs
	// as the the scroll delegate will temporarily switch back the page
	// number.
    _imagePageControlBeingUsed = YES;
}

- (NSString *)processTitle:(NSString *)title
{
    NSString *newTitle = title;
    if ([title length] >= 3) {
        if ([[title substringFromIndex:[title length] - 3] isEqualToString:@", A"]) {
            newTitle = [NSString stringWithFormat:@"%@%@",@"A ",[title substringToIndex:[title length] - 3]];
        }
    }
    if ([title length] >= 5) {
        if ([[title substringFromIndex:[title length] - 5] isEqualToString:@", THE"]) {
            newTitle = [NSString stringWithFormat:@"%@%@",@"THE ",[title substringToIndex:[title length] - 5]];
        }
    }    
    return newTitle;    
}

- (IBAction)openShareView:(id)sender
{
	NSString *message = [NSString stringWithFormat:@"%@%@",[self processTitle:[_currentFilm titleEnglish]],@" "];
	NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@%@", kBaseWebURL, @"/films/detail/",[_currentFilm slug]] ];
	NSMutableArray *postItems = [NSMutableArray arrayWithObjects:message,url,nil];

	NSSet *photos = [_currentFilm films_photos]; // This includes both photo and video entries
	if (![photos count]) {
		[postItems addObject: [UIImage imageNamed:@"default-photo.png"]];
	} else {
		// Try to use cached image, otherwise retrieve first image (will cause a delay in share view opening)
		if (_firstPhoto != nil) {
			[postItems addObject: _firstPhoto];
		} else {
			NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES];
			NSArray *sortedPhotos = [photos sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
			
			NSString *imageUrl = nil;
			for (PhotoVideo *thisPhoto in sortedPhotos) {
				if ([thisPhoto.type isEqualToString:@"photo"]) {
					if ([thisPhoto.photoXLargeUrl isEqualToString:@""]) {
						imageUrl = thisPhoto.photoLargeUrl;
					} else {
						imageUrl = thisPhoto.photoXLargeUrl;
					}
					break;
				}
			}
			if ([AFNetworkReachabilityManager sharedManager].reachable) {
				if (imageUrl != nil) {
					NSURL *url = [NSURL URLWithString:imageUrl];
					NSData *data = [NSData dataWithContentsOfURL:url];
					UIImage *img = [[UIImage alloc] initWithData:data scale:1.0];
					[postItems addObject: img];
				}
			}
		}
	}
	NSArray *finalPostItems = postItems;
	UIActivityViewController *activityVC = [[UIActivityViewController alloc]
                                                initWithActivityItems:finalPostItems
                                                applicationActivities:nil];
	activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo];
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		UIView *tappedView = (UIView *)[sender view];
		CGRect convertedFrame = [self.navigationController.navigationBar convertRect:tappedView.frame toView:self.view];
		[self presentViewController:activityVC animated:YES completion:nil];
		
		UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
		presentationController.sourceView = self.view;
		presentationController.sourceRect = convertedFrame;
	} else {
		[self presentViewController:activityVC animated:YES completion:nil];
	}
}

- (void) changeSubview:(id)sender
{
    int selectedIndex = (uint32_t)[_segmentedControl selectedSegmentIndex];
    switch (selectedIndex) {
        case 0:
            [_detailsView setHidden:FALSE];
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, _detailsView.frame.size.height + 150)];
            [_synopsisView setHidden:TRUE];
            [_screeningView setHidden:TRUE];
            break;
            
        case 1:
            [_detailsView setHidden:TRUE];
            [_synopsisView setHidden:FALSE];
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, _synopsisView.frame.size.height + 150)];
            [_screeningView setHidden:TRUE];
            break;
            
        case 2:
            [_detailsView setHidden:TRUE];
            [_synopsisView setHidden:TRUE];
            [_screeningView setHidden:FALSE];
            [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, _screeningView.frame.size.height + 150)];
            break;
            
        default:
            break;
    }
} 


#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    if (!_imagePageControlBeingUsed) {
		CGFloat pageWidth;
		if (IS_LANDSCAPE) {
			pageWidth = 704.0;
		} else {
			pageWidth = self.imageScrollView.frame.size.width;
		}
		int page = floor((self.imageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.imagePageControl.currentPage = page;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    _imagePageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _imagePageControlBeingUsed = NO;
}


#pragma mark - NSFetchedResultsControllerDelegate Methods
- (NSFetchedResultsController *)fetchedResultsController
{
    if (__fetchedResultsController != nil) {
        return __fetchedResultsController;
    }
    
    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Screenings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateTime" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"dateString" cacheName:@"dateString"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	     */
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return __fetchedResultsController;
}

#pragma mark - UIWebViewDelegate Methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *screening_id = @"";
    if ([[[request URL] scheme] isEqualToString:@"http"] || [[[request URL] scheme] isEqualToString:@"https"]) {
        // Open WebViewController if a sponsor logo is clicked.
        if ( navigationType == UIWebViewNavigationTypeLinkClicked ) {
            _ticketsUrl = [[request URL] absoluteString];
            [self performSegueWithIdentifier: @"ticketsWebView" sender:self];
            return NO;
        }
    } else if ([[[request URL] scheme] isEqualToString:@"myfest"]) {
        screening_id = [[request URL] host];
    }
    
    if (![screening_id isEqualToString:@""]) {
        int index = 0; int i = 0;
        NSNumber *screeningIdNum = [[NSNumber alloc] initWithInt:[screening_id intValue]];
        NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"dateTime" ascending:YES];
        NSArray *sorted = [_screenings sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
        
        for (Screenings *thisScreening in sorted) {
            if ([[thisScreening screening_id] isEqualToNumber:screeningIdNum]) { index = i; }
            i++;
        }
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSMutableArray *myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];
        NSMutableArray *myNewFestArray = [[NSMutableArray alloc] init];
        Screenings *selectedScreening = [sorted objectAtIndex:index];
        
        BOOL alreadyAdded = FALSE;
        if([myFestArray count] == 0) {
            // Empty array, just add it.
            [myFestArray addObject:selectedScreening.screening_id];
        } else {
            // Check to see if screening already exists to determine if it should be added or removed
            for (NSNumber *screening_id in myFestArray) {
                // Both film id & datetime match = remove existing screening
                if ([screening_id isEqualToNumber:selectedScreening.screening_id]) {
                    alreadyAdded = TRUE;
                    [myNewFestArray addObject:screening_id];
                }
            }
            
            // We scanned through the existing array and didn't find this screening - add it.
            if (alreadyAdded == FALSE) {
                [myFestArray addObject:selectedScreening.screening_id];
            }
        }
        for (NSNumber *screening_id in myNewFestArray) { [myFestArray removeObject:screening_id]; }
		
        // Update "MyFest" array with new version
        NSArray *myFestArrayPrefs = myFestArray;
        [prefs setObject:myFestArrayPrefs forKey:@"myFestScreenings"];
        [prefs synchronize];
        
        return NO;
    }
    return YES;
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		CGFloat top = self.segmentedControl.frame.origin.y + self.segmentedControl.frame.size.height + 8.0;
		CGRect viewFrame = CGRectMake(8.0, top, self.view.frame.size.width-8.0, 300.0);
		viewFrame.size.height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue] + 100.0;
		if (webView == _detailsView) {
			[_detailsView setFrame:viewFrame];
		}
		if (webView == _synopsisView) {
			[_synopsisView setFrame:viewFrame];
		}
		if (webView == _screeningView) {
			[_screeningView setFrame:viewFrame];
		}
		
		CGFloat scrollViewHeight = 0.0f;
		for (UIView* view in self.scrollView.subviews) {
			scrollViewHeight += view.frame.size.height;
		}
		[self.scrollView setContentSize:(CGSizeMake(320, scrollViewHeight))];
	} else {
		CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue] + 15.0;
		CGRect existingFrame = webView.frame;

		if ([webView isEqual:_detailsView]) {
			_detailsView.frame = CGRectMake(existingFrame.origin.x, existingFrame.origin.y, existingFrame.size.width, height);
			_screeningView.frame = CGRectMake( (_detailsView.frame.origin.x + _detailsView.frame.size.width), _detailsView.frame.origin.y, _detailsView.frame.size.width, _detailsView.frame.size.height);
			_synopsisView.frame = CGRectMake(_detailsView.frame.origin.x, (_detailsView.frame.origin.y + _detailsView.frame.size.height + 20.0), _synopsisView.frame.size.width, _synopsisView.frame.size.height);
			
		} else if ([webView isEqual:_synopsisView]) {
			_synopsisView.frame = CGRectMake(existingFrame.origin.x, existingFrame.origin.y, existingFrame.size.width, height + 20.0);
			
		} else if ([webView isEqual:_screeningView]) {
			CGFloat synopsisY = 0.0;
			if (_detailsView.frame.size.height > height) {
				synopsisY = (_detailsView.frame.origin.y + _detailsView.frame.size.height + 20.0);
				height = _detailsView.frame.size.height;
			} else {
				synopsisY = (_detailsView.frame.origin.y + height + 20.0);
			}
			_detailsView.frame = CGRectMake(_detailsView.frame.origin.x, _detailsView.frame.origin.y, _detailsView.frame.size.width, height + 20.0);
			_screeningView.frame = CGRectMake(existingFrame.origin.x, existingFrame.origin.y, existingFrame.size.width, height + 20.0);
			_synopsisView.frame = CGRectMake(_synopsisView.frame.origin.x, synopsisY, _synopsisView.frame.size.width, _synopsisView.frame.size.height);			
		}
		self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, (_synopsisView.frame.origin.y + _synopsisView.frame.size.height));
	}
}

- (void) dismissWebView
{
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

#pragma mark - MFMailComposeViewControllerDelegate Methods
- (IBAction)launchEmailView
{
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    [controller setSubject:@"Check out this Film!"];
    [controller setToRecipients:[NSArray arrayWithObject:@"youremail@address.com"]];
    NSString *message = [NSString stringWithFormat:@"%@%@%@%@%@", [self processTitle:[_currentFilm titleEnglish]], @"\n",kBaseWebURL, @"/films/detail/",[_currentFilm slug]];
    [controller setMessageBody:message isHTML:NO];
    [self presentViewController:controller animated:YES completion:^(void){}];
    controller.mailComposeDelegate = self;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}

#pragma mark - NSURLConnectionDelegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    [_receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    [_receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // inform the user
    /*
    NSLog(@"Connection failed! Error - %@ %@", [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    */
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    //NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{    
    if ([[segue identifier] isEqualToString:@"ticketsWebView"]) {
        WebViewController *webViewController = segue.destinationViewController;
		webViewController.delegate = self;
        webViewController.urlToLoad = _ticketsUrl;
    }
}

@end
