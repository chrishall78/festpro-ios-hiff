//
//  MPParallaxCollectionViewCell.m
//  MPPercentDriven
//
//  Created by Alex Manzella on 27/05/14.
//  Copyright (c) 2014 mpow. All rights reserved.
//

#import "MPParallaxCollectionViewCell.h"

@implementation MPParallaxCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.clipsToBounds=YES;
        //self.imageView=[[UIImageView alloc] initWithFrame:CGRectInset(self.bounds, 0, -MAX_VERTICAL_PARALLAX)];
        self.imageView.contentMode=UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds=YES;
        self.imageView.autoresizingMask=UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        //[self.contentView addSubview:self.imageView];
		
		//self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(15.0, 10.0, 300.0, 160.0)];
		self.textLabel.text = @"";
		//[self.contentView addSubview:self.textLabel];
		
    }
    return self;
}



- (void)setParallaxValue:(CGFloat)parallaxValue{
    _parallaxValue = parallaxValue;
    
    CGRect frame = self.imageView.frame;
    frame.origin.y = -MAX_VERTICAL_PARALLAX + parallaxValue*MAX_VERTICAL_PARALLAX;
    self.imageView.frame = frame;
	//NSLog(@"Y: %f", frame.origin.y);
    
    if ([[self delegate] respondsToSelector:@selector(cell:changeParallaxValueTo:)]) {
        [[self delegate] cell:self changeParallaxValueTo:parallaxValue];
    }
}

- (void)dealloc {
    self.delegate = nil;
}

- (UIImage *)image {
    return self.imageView.image;
}

- (void)setImage:(UIImage *)image {
    self.imageView.image = image;
}

@end
