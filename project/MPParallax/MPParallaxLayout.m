//
//  MPParallaxLayout.m
//  MPPercentDriven
//
//  Created by Alex Manzella on 27/05/14.
//  Copyright (c) 2014 mpow. All rights reserved.
//

#import "MPParallaxLayout.h"
#import "MPParallaxCollectionViewCell.h"

@implementation MPParallaxLayout

- (id)init{
    
    if (self=[super init]) {
        
        self.itemSize = CGSizeMake(320.0, 153.0);
        self.minimumLineSpacing = 0.0;
        self.minimumInteritemSpacing = 0.0;
        self.scrollDirection=UICollectionViewScrollDirectionVertical;
        
    }
    
    return self;
}

-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    
    NSMutableArray *attributes=[[super layoutAttributesForElementsInRect:rect] mutableCopy];

    [attributes enumerateObjectsUsingBlock:^(UICollectionViewLayoutAttributes* obj, NSUInteger idx, BOOL *stop) {
        
        MPParallaxCollectionViewCell *cell=(MPParallaxCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:obj.indexPath];
        
        if (cell) {
            CGFloat position=self.collectionView.contentOffset.y;
            CGFloat final=self.itemSize.height*obj.indexPath.item;
            CGFloat missing=final-position;
            CGFloat parallaxValue=missing/self.collectionView.frame.size.height;
            
            cell.parallaxValue=parallaxValue;
			
			NSLog(@"position: %f final: %f parallaxValue: %f", position, final, parallaxValue);
        }
    }];

    return attributes;
}

- (CGSize)itemSize{
    return CGSizeMake(320.0, 153.0);
}


- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}

@end
