//
//  Genres.h
//  FestPro
//
//  Created by Christopher Hall on 1/17/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Films;

@interface Genres : NSManagedObject

@property (nonatomic, retain) NSNumber * genreId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *genres_films;

@end

@interface Genres (CoreDataGeneratedAccessors)

- (void)addGenres_filmsObject:(Films *)value;
- (void)removeGenres_filmsObject:(Films *)value;
- (void)addGenres_films:(NSSet *)values;
- (void)removeGenres_films:(NSSet *)values;

@end
