//
//  FilterMenuViewController.m
//  FestPro
//
//  Created by Christopher Hall on 1/19/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "FilterMenuViewController.h"
#import "ActionSheetPicker.h"
#import "FestProAppDataObject.h"
#import "AppDelegateProtocol.h"
#import "CustomColoredAccessory.h"
#import "Films.h"


@implementation FilterMenuViewController

@synthesize managedObjectContext;


- (FestProAppDataObject *) theAppDataObject;
{
	id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
	FestProAppDataObject *theDataObject;
	theDataObject = (FestProAppDataObject *) theDelegate.theAppDataObject;
	return theDataObject;
}

- (id)initWithStyle:(UITableViewStyle)style
{
	self = [super initWithStyle:style];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	_sectionArray = [prefs arrayForKey:@"sectionList"];
	_eventTypeArray = [prefs arrayForKey:@"eventTypeList"];
	_countryArray = [prefs arrayForKey:@"countriesList"];
	_languageArray = [prefs arrayForKey:@"languagesList"];
	_genreArray = [prefs arrayForKey:@"genresList"];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return YES;
	} else {
		return NO;
	}
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return (UIInterfaceOrientationMaskPortrait |
				UIInterfaceOrientationMaskLandscapeLeft |
				UIInterfaceOrientationMaskLandscapeRight);
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	[super preferredInterfaceOrientationForPresentation];
	return UIInterfaceOrientationPortrait;
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell"];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
	}
	
	// Configure the cell...
	
	cell.accessoryType = UITableViewCellAccessoryNone;
	CustomColoredAccessory *accessory = [CustomColoredAccessory accessoryWithColor:UI_COLOR_03];
	accessory.highlightedColor = UI_COLOR_01;
	cell.accessoryView = accessory;
	
	cell.backgroundColor = [UIColor whiteColor];
	cell.detailTextLabel.textColor = [UIColor blackColor];
	
	switch (indexPath.row) {
		case 0:
			cell.textLabel.text = @"Section";
			cell.detailTextLabel.text = _sectionLabel.text;
			break;
		case 1:
			cell.textLabel.text = @"Country";
			cell.detailTextLabel.text = _countryLabel.text;
			break;
		case 2:
			cell.textLabel.text = @"Language";
			cell.detailTextLabel.text = _languageLabel.text;
			break;
		case 3:
			cell.textLabel.text = @"Genre";
			cell.detailTextLabel.text = _genreLabel.text;
			break;
		case 4:
			cell.textLabel.text = @"Event Type";
			cell.detailTextLabel.text = _eventTypeLabel.text;
			break;
			
		default:
			break;
	}    

	// kill insets for iOS 8
	if ([[UIDevice currentDevice].systemVersion floatValue] >= 8) {
		cell.preservesSuperviewLayoutMargins = NO;
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
	// iOS 7 and earlier
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}
	
	return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
	
	switch (indexPath.row) {
		case 0:
			[self selectFilter:cell withIndexPath:indexPath.row andTitle:@"Select Section" andRows:self.sectionArray andSelIndex:self.sectionSelIndex];
			break;
		case 1:
			[self selectFilter:cell withIndexPath:indexPath.row andTitle:@"Select Country" andRows:self.countryArray andSelIndex:self.countrySelIndex];
			break;
		case 2:
			[self selectFilter:cell withIndexPath:indexPath.row andTitle:@"Select Language" andRows:self.languageArray andSelIndex:self.languageSelIndex];
			break;
		case 3:
			[self selectFilter:cell withIndexPath:indexPath.row andTitle:@"Select Genre" andRows:self.genreArray andSelIndex:self.genreSelIndex];
			break;
		case 4:
			[self selectFilter:cell withIndexPath:indexPath.row andTitle:@"Select Event Type" andRows:self.eventTypeArray andSelIndex:self.eventTypeSelIndex];
			break;
			
		default:
			break;
	}
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Action Sheet
- (IBAction)selectFilter:(UITableViewCell *)sender withIndexPath:(NSInteger)indexPath andTitle:(NSString *)title andRows:(NSArray *)array andSelIndex: (NSInteger)index
{
	[ActionSheetStringPicker showPickerWithTitle:title rows:array initialSelection:index doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
		switch (indexPath) {
			case 0:
				self.sectionSelIndex = selectedIndex;
				_sectionLabel.text = [self.sectionArray objectAtIndex:self.sectionSelIndex];
				break;
			case 1:
				self.countrySelIndex = selectedIndex;
				_countryLabel.text = [self.countryArray objectAtIndex:self.countrySelIndex];
				break;
			case 2:
				self.languageSelIndex = selectedIndex;
				_languageLabel.text = [self.languageArray objectAtIndex:self.languageSelIndex];
				break;
			case 3:
				self.genreSelIndex = selectedIndex;
				_genreLabel.text = [self.genreArray objectAtIndex:self.genreSelIndex];
				break;
			case 4:
				self.eventTypeSelIndex = selectedIndex;
				_eventTypeLabel.text = [self.eventTypeArray objectAtIndex:self.eventTypeSelIndex];
				break;
			default:
				break;
		}
		[self.tableView reloadData];
	} cancelBlock:^(ActionSheetStringPicker *picker) {
		[self actionPickerCancelled:picker];
	} origin:sender];
}

#pragma mark - Implementation

- (void)actionPickerCancelled:(id)sender {
#ifdef DEBUG
	NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
#endif
}



#pragma mark - FilterMenuViewControllerDelegate
- (IBAction)cancel:(id)sender
{
	[self.delegate filterMenuViewControllerDidCancel:self];
}

- (IBAction)done:(id)sender
{    
	FestProAppDataObject *theDataObject = [self theAppDataObject];
	theDataObject.sectionFilterValue = _sectionLabel.text;
	theDataObject.countryFilterValue = _countryLabel.text;
	theDataObject.languageFilterValue = _languageLabel.text;
	theDataObject.genreFilterValue = _genreLabel.text;
	theDataObject.eventTypeFilterValue = _eventTypeLabel.text;
	
	[self.delegate filterMenuViewControllerDidSave:self];
}


@end
