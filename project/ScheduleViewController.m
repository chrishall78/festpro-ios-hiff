//
//  ScheduleViewController.m
//  FestPro
//
//  Created by Christopher Hall on 1/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "ScheduleViewController.h"
#import "FPNavigationController.h"
#import "FilmDetailViewController.h"
#import "HomeViewController.h"
#import "BrowseFilmsViewController.h"
#import "GridScheduleViewController.h"
#import "Screenings.h"
#import "CustomColoredAccessory.h"
#import "ActionSheetPicker.h"

@implementation ScheduleViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Gesture actions

- (IBAction)moveRightTab:(id)sender
{
    NSUInteger selectedIndex = [self.tabBarController selectedIndex];
    [self.tabBarController setSelectedIndex:selectedIndex + 1];
}

- (IBAction)moveLeftTab:(id)sender
{
    NSUInteger selectedIndex = [self.tabBarController selectedIndex];
    [self.tabBarController setSelectedIndex:selectedIndex - 1];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
		[self.tableView setSeparatorInset:UIEdgeInsetsZero];
	}
    self.fetchedResultsController.delegate = self;
    
	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeftTab:)];
		[swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
		[self.view addGestureRecognizer:swipeLeft];
		
		UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveRightTab:)];
		[swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
		[self.view addGestureRecognizer:swipeRight];
	}

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *startDateString = [NSString stringWithFormat:@"%@ %@ %@",[prefs objectForKey:@"festivalStartDate"], @"00:00:00", [prefs objectForKey:@"festivalTimeZone"]];
    NSString *endDateString = [NSString stringWithFormat:@"%@ %@ %@",[prefs objectForKey:@"festivalEndDate"], @"00:00:00", [prefs objectForKey:@"festivalTimeZone"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:kBaseTimezone]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZ"];
    NSDate *startDate = [formatter dateFromString:startDateString];
    NSDate *endDate = [formatter dateFromString:endDateString];
    NSDate *currentDate = startDate;
    [formatter setDateFormat:@"MMMM d, yyyy"];
    
    NSArray *test = [self.fetchedResultsController sections];
    
    _festivalDates = [[NSMutableArray alloc] init];
    int day = 1; int addedDay = 1;
    while (!([currentDate compare:endDate] == NSOrderedDescending)) {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZ"];
        int total = (uint32_t)[test count];
        if (addedDay-1 < total) {
            BOOL added = false;
            NSDate *sectionDate = [formatter dateFromString:[[test objectAtIndex:(addedDay-1)] name]];
            /* Eliminate dates between start and end which have no screenings. This is to keep the date picker in sync with the available sections. If there are more dates than sections, the extra ones will generate an out of bounds crash when selected, and the scrolling can become inaccurate.
             */
            if ([currentDate isEqualToDate:sectionDate]) {
                [formatter setDateFormat:@"EEE, MMMM d, yyyy"];
                [_festivalDates addObject:[formatter stringFromDate:currentDate]];
                added = true;
            }
            currentDate = [self getForDays:day fromDate:startDate];
            day++;

            if (added) { addedDay++; }
        } else {
            currentDate = [self getForDays:day fromDate:startDate];
            day++;            
        }
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.fetchedResultsController = nil;
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	     */
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    [self.tableView reloadData];
    
    NSInteger count = [[self.fetchedResultsController sections] count];
    if (count == 0) { [_dateButton setEnabled:FALSE]; }

}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return YES;
	} else {
		return NO;
	}
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return (UIInterfaceOrientationMaskPortrait |
				UIInterfaceOrientationMaskLandscapeLeft |
				UIInterfaceOrientationMaskLandscapeRight);
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	[super preferredInterfaceOrientationForPresentation];
	return UIInterfaceOrientationPortrait;
}

#pragma mark - Action Sheet
- (IBAction)selectFestivalDate:(UIControl *)sender {
	ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:@"Select Date" rows:self.festivalDates initialSelection:self.selectedIndex target:self successAction:@selector(actionPickerDateSelected:) cancelAction:@selector(actionPickerCancelled:) origin:sender];
	[picker showActionSheetPicker];
}

- (IBAction)dateButtonTapped:(UIBarButtonItem *)sender {
    [self selectFestivalDate:sender];
}

- (void)actionPickerDateSelected:(id)sender {
    //NSLog(@"Delegate has been informed that date was selected. Sender: %@", sender);
    self.selectedIndex = [sender intValue];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:[sender intValue]];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)actionPickerCancelled:(id)sender {
    //NSLog(@"Delegate has been informed that ActionSheetPicker was cancelled");
}



#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    _filmProgram = nil;
    static NSString *CellIdentifier = @"scheduleCell";
    ScheduleCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ScheduleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];

	// kill insets for iOS 8
	if ([[UIDevice currentDevice].systemVersion floatValue] >= 8) {
		cell.preservesSuperviewLayoutMargins = NO;
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
	// iOS 7 and earlier
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}
	
    return cell;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[cell setBackgroundColor:UI_COLOR_05];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSInteger count = [[self.fetchedResultsController sections] count];
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
	
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    
    return numberOfRows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	// Display the dates of the films as section headings.
	if (self.fetchedResultsController.sections.count > 0) {
		id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo name];
	} else {
        return nil;
    }
}

/*
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	//The array of strings to use as indices
	NSLog(@"Index Titles: %@", [self.fetchedResultsController sectionIndexTitles]);
	
	return [self.fetchedResultsController sectionIndexTitles];
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
	//The index (integer) of the current section that the user is touching
	NSLog(@"This Index: %@ | %ld", title, (long)index);
	return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}
*/


#pragma mark - Table view delegate

- (void)configureCell:(ScheduleCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Screenings *currentCell = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSArray *myFestArray = [[NSArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];
    BOOL inMyFest = FALSE;
    
    _filmProgram = [[NSArray alloc] init];
    _filmProgram = [[currentCell film_ids] componentsSeparatedByString:@","];
    
    if([_filmProgram count] > 1) {
        // Distinguish programs of films from single films
		[cell setAccessoryType:UITableViewCellAccessoryNone];
		CustomColoredAccessory *accessory = [CustomColoredAccessory accessoryWithColor:UI_COLOR_01];
		accessory.highlightedColor = UI_COLOR_03;
		cell.accessoryView = accessory;
    } else {
		[cell setAccessoryType:UITableViewCellAccessoryNone];
		CustomColoredAccessory *accessory = [CustomColoredAccessory accessoryWithColor:UI_COLOR_03];
		accessory.highlightedColor = UI_COLOR_01;
		cell.accessoryView = accessory;
    }
    
    if ([[currentCell programName] isEqualToString:@""]) {
        cell.filmTitleLabel.text = [currentCell films];
    } else {
        cell.filmTitleLabel.text = [currentCell programName];
    }
    cell.venueLabel.text = [currentCell location];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *currentTimeZone = [NSTimeZone timeZoneWithName:kBaseTimezone];
    [dateFormatter setTimeZone:currentTimeZone];
    [dateFormatter setDateFormat: @"h:mm a"];
    cell.timeLabel.text = [dateFormatter stringFromDate:[currentCell dateTime]];
    
    for (NSNumber *screening_id in myFestArray) {
        if ([currentCell screening_id] != nil) {
            if ([screening_id isEqualToNumber:[currentCell screening_id]]) { inMyFest = TRUE; }
        }
    }
    if (inMyFest) {
        [cell.myFestButton setImage:[UIImage imageNamed:@"icon-myfest-remove.png"] forState:UIControlStateNormal];
    } else {
        [cell.myFestButton setImage:[UIImage imageNamed:@"icon-myfest-add.png"] forState:UIControlStateNormal];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Screenings *currentCell = [self.fetchedResultsController objectAtIndexPath:indexPath];
    _filmProgram = [[NSArray alloc] init];
    _filmProgram = [[currentCell film_ids] componentsSeparatedByString:@","];
    
    if ([[currentCell programName] isEqualToString:@""]) {
        _filmProgramName = [currentCell films];
    } else {
        _filmProgramName = [currentCell programName];
    }
    
    // Send user to Film Detail if it is a single film, or to Browse Films if there are multiple.
    if([_filmProgram count] > 1) {
        [self performSegueWithIdentifier: @"FilmProgram" sender:self];
    } else {
        [self performSegueWithIdentifier: @"FilmDetail" sender:self];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    Screenings *currentCell = [self.fetchedResultsController objectAtIndexPath:indexPath];
    _filmProgram = [[NSArray alloc] init];
    _filmProgram = [[currentCell film_ids] componentsSeparatedByString:@","];
    
    if ([[currentCell programName] isEqualToString:@""]) {
        _filmProgramName = [currentCell films];
    } else {
        _filmProgramName = [currentCell programName];
    }
    
    // Send user to Film Detail if it is a single film, or to Browse Films if there are multiple.
    if([_filmProgram count] > 1) {
        [self performSegueWithIdentifier: @"FilmProgram" sender:self];
    } else {
        [self performSegueWithIdentifier: @"FilmDetail" sender:self];
    }
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSInteger sectionCount = [[self.fetchedResultsController sections] count];
    if (section != sectionCount-1) {
        return [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, 0.0)];
    } else {
        CGRect myRect = CGRectMake(0.0f, 0.0f, 320.0f, 22.0f);
        CGRect myLabelRect = CGRectMake(10.0f, 0.0f, 300.0f, 22.0f);
        
        UIView *footerView = [[UIView alloc] initWithFrame:myRect];
        UILabel * label1 = [[UILabel alloc] initWithFrame:myLabelRect];
        label1.backgroundColor = [UIColor clearColor];
        label1.textColor = UI_COLOR_05;
        label1.text = @"Films are unrated. Please use your own discretion.";
        label1.font = [UIFont fontWithName:@"Helvetica" size:13];
        
        [footerView setBackgroundColor:UI_COLOR_02];
        [footerView addSubview:label1];
        
        return footerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //NSInteger sectionCount = [[self.fetchedResultsController sections] count];
    //if (section != sectionCount-1) {
    return 0.0;
    //} else {
    //    return 20.0;
    //}
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect myRect = CGRectMake(0.0, 0.0, 320.0, 22.0);
    CGRect myLabelRect = CGRectMake(10.0, 0.0, 300.0, 22.0);
    
    UIView *headerView = [[UIView alloc] initWithFrame:myRect];
    [headerView setBackgroundColor:UI_COLOR_02];
    
    UILabel * label1 = [[UILabel alloc] initWithFrame:myLabelRect];
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = UI_COLOR_05;
    
    if (self.fetchedResultsController.sections.count > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        NSString *rawDateStr = [sectionInfo name];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:kBaseTimezone]];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZ"];
        NSDate *date = [formatter dateFromString:rawDateStr];
        [formatter setDateFormat:@"EEEE, MMMM d, yyyy"];
        
        label1.text = [formatter stringFromDate:date];
        label1.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        [headerView addSubview:label1];
    } else {
        [headerView setFrame:CGRectMake(0.0, 0.0, 320.0, 66.0)];
        [label1 setFrame:CGRectMake(10.0, 0.0, 300.0, 66.0)];
        label1.text = @"The festival schedule will be released on or around September 1, 2014. Please check for updates at that time.";
        label1.font = [UIFont fontWithName:@"Helvetica" size:14];
        label1.lineBreakMode = NSLineBreakByWordWrapping;
        label1.numberOfLines = 0;
        [headerView addSubview:label1];
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	if (self.fetchedResultsController.sections.count > 0) {
        return 22.0;
    } else {
        return 66.0;
    }
}


#pragma mark - ScheduleViewController Methods

- (NSDate *)getForDays:(int)days fromDate:(NSDate *)date
{
    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setDay:days];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:components toDate:date options:0];
}

- (IBAction)toggleMyFestButton:(id)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSMutableArray *myFestArray = [[NSMutableArray alloc] initWithArray:[prefs objectForKey:@"myFestScreenings"]];
    NSMutableArray *myNewFestArray = [[NSMutableArray alloc] init];
    BOOL alreadyAdded = FALSE;
	
	ScheduleCell *clickedCell;

#ifdef __IPHONE_6_0
	clickedCell = (ScheduleCell *)[[sender superview] superview];
#else
	clickedCell = (ScheduleCell *)[[[sender superview] superview] superview];
#endif
    NSIndexPath *clickedButtonPath = [self.tableView indexPathForCell:clickedCell];
    Screenings *currentCell = [self.fetchedResultsController objectAtIndexPath:clickedButtonPath];
    
    if([myFestArray count] == 0) {
        // Empty array, just add to MyFest.
        if ([currentCell screening_id] != nil) {
            [myFestArray addObject:[currentCell screening_id]];
            [clickedCell.myFestButton setImage:[UIImage imageNamed:@"icon-myfest-remove.png"] forState:UIControlStateNormal];
        }
    } else {
        // Check to see if screening already exists to determine if it should be added or removed
        for (NSNumber *screening_id in myFestArray) {
            // Both film id & datetime match = remove existing screening
            if ([currentCell screening_id] != nil) {
                if ([screening_id isEqualToNumber:[currentCell screening_id]]) {
                    alreadyAdded = TRUE;
                    [myNewFestArray addObject:screening_id];
                    [clickedCell.myFestButton setImage:[UIImage imageNamed:@"icon-myfest-add.png"] forState:UIControlStateNormal];
                }
            }
        }
        
        // We scanned through the existing array and didn't find this screening - add to MyFest.
        if (alreadyAdded == FALSE) {
            if ([currentCell screening_id] != nil) {
                [myFestArray addObject:[currentCell screening_id]];
                [clickedCell.myFestButton setImage:[UIImage imageNamed:@"icon-myfest-remove.png"] forState:UIControlStateNormal];
            }
        }
    }
    
    for (NSNumber *screening_id in myNewFestArray) { [myFestArray removeObject:screening_id]; }
    
    // Update "MyFest" array with new version
    NSArray *myFestArrayPrefs = myFestArray;
    [prefs setObject:myFestArrayPrefs forKey:@"myFestScreenings"];
    [prefs synchronize];
}


#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Screenings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateTime" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"dateString" cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	     */
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    
    return _fetchedResultsController;
}   

/*
- (NSString *)sectionIndexTitleForSectionName:(NSString *)sectionName {
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setTimeZone:[NSTimeZone timeZoneWithName:kBaseTimezone]];
	[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZ"];
	NSDate *date = [formatter dateFromString:sectionName];
	[formatter setDateFormat:@"MMM d"];
	NSString *abrSectionName = [formatter stringFromDate:date];
	NSLog(@"%@ | %@", sectionName, abrSectionName);
	return abrSectionName;
}
*/

#pragma mark - GridScheduleWebViewController

- (void) dismissGridView {
    [self dismissViewControllerAnimated:YES completion:^(void){}];
}


#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{    
    if ([[segue identifier] isEqualToString:@"FilmDetail"]) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            FPNavigationController *navigationController = segue.destinationViewController;
            FilmDetailViewController *filmDetail = [[navigationController viewControllers] objectAtIndex:0];
            filmDetail.managedObjectContext = _managedObjectContext;
            Screenings *curScreening = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
            NSSet *screeningFilms = [curScreening screenings_films];
            filmDetail.currentFilm = [[screeningFilms allObjects] objectAtIndex:0];

			HomeViewController *homeViewController;
			if ([[self.splitViewController.viewControllers lastObject] isKindOfClass:[UINavigationController class]]) {
				homeViewController = (HomeViewController *) ((UINavigationController *)[self.splitViewController.viewControllers lastObject]).topViewController;
			} else {
				homeViewController = [self.splitViewController.viewControllers lastObject];
			}
			if (homeViewController.masterPopoverController != nil) {
				[homeViewController.masterPopoverController dismissPopoverAnimated:YES];
			}
			if ([filmDetail conformsToProtocol:@protocol(UISplitViewControllerDelegate)]) {
                self.splitViewController.delegate = filmDetail;
				filmDetail.popoverButton = homeViewController.popoverButton;
				filmDetail.masterPopoverController = homeViewController.masterPopoverController;
            } else {
                self.splitViewController.delegate = nil;
            }
        } else {
            FilmDetailViewController *filmDetail = (FilmDetailViewController *)[segue destinationViewController];
            filmDetail.managedObjectContext = _managedObjectContext;
            Screenings *curScreening = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
            NSSet *screeningFilms = [curScreening screenings_films];
            filmDetail.currentFilm = [[screeningFilms allObjects] objectAtIndex:0];
        }
    }
    
    if ([[segue identifier] isEqualToString:@"FilmProgram"]) {
		BrowseFilmsViewController *browseFilms = (BrowseFilmsViewController *)[segue destinationViewController];
		browseFilms.managedObjectContext = _managedObjectContext;
		browseFilms.filmProgram = _filmProgram;
		browseFilms.filmProgramName = _filmProgramName;
    }
    
    if ([[segue identifier] isEqualToString:@"GridSchedule"]) {
		FPNavigationController *navigationController = segue.destinationViewController;
        GridScheduleViewController *gridSchedule = [[navigationController viewControllers] objectAtIndex:0];
		gridSchedule.managedObjectContext = _managedObjectContext;
		gridSchedule.delegate = self;
    }
}

- (IBAction)showFilmProgram:(id)sender {
    [self performSegueWithIdentifier: @"FilmProgram" sender:self];
}

- (IBAction)showGridSchedule:(id)sender {
    [self performSegueWithIdentifier: @"GridSchedule" sender:self];
}


@end