//
//  AppConstants.h
//  FestPro
//
//  Created by Christopher Hall on 2/6/14.
//  Copyright (c) 2014 FestPro LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppConstants : NSObject

extern NSString* const kBaseDataURL;
extern NSString* const kBaseWebURL;
extern NSString* const kBaseDefaultPhoto;
extern NSString* const kBaseTimezone;
extern NSString* const kShoppingCartURL;

extern NSString* const kiPhoneDetailsStyles;
extern NSString* const kiPhoneSynopsisStyles;
extern NSString* const kiPhoneScreeningStyles;
extern NSString* const kiPadDetailsStyles;
extern NSString* const kiPadSynopsisStyles;
extern NSString* const kiPadScreeningStyles;

@end
