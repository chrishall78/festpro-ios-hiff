//
//  WebViewController.m
//  iBoom
//
//  Created by Brian Stormont on 1/30/09.
//  Copyright 2009 Stormy Productions. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController

- (void) viewDidLoad {
	[super viewDidLoad];
    _webView.scrollView.bounces = NO;
	_toolBar.clipsToBounds = YES;
	[[AFNetworkReachabilityManager sharedManager] startMonitoring];
	
    NSURL *url = [NSURL URLWithString:_urlToLoad];
    [self loadBrowser:url];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void) loadBrowser: (NSURL *) url
{   
    // Check for network connectivity
	__unsafe_unretained typeof(self) weakSelf = self;

	if ([AFNetworkReachabilityManager sharedManager].reachable) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[weakSelf.webView setDelegate:weakSelf];
			
			if (weakSelf.urlToLoad == (id)[NSNull null] || weakSelf.urlToLoad.length == 0) {
				NSString *path = [[NSBundle mainBundle] bundlePath];
				NSURL *baseURL = [NSURL fileURLWithPath:path];
				[weakSelf.webView loadHTMLString:weakSelf.htmlToLoad baseURL:baseURL];
			} else {
				[weakSelf.webView loadRequest:[NSURLRequest requestWithURL:url]];
			}
			
			// Disable forward button
			weakSelf.webForward.enabled = NO;
		});
	} else {
		dispatch_async(dispatch_get_main_queue(), ^{
			[weakSelf showNoNetworkAlert];
		});
	};
}

- (IBAction) browseBack: (id) sender
{
	if ([_webView canGoBack]){
		[_webView goBack];
		// Enable the forward button
		_webForward.enabled = YES;
	}else{
		[_delegate dismissWebView];
	}
	
}

- (IBAction) browseForward: (id) sender
{
	if ([_webView canGoForward]){
		[_webView goForward];
		
		if (![_webView canGoForward]){
			// disable the forward button
			_webForward.enabled = NO;
		}
	}else{
		_webForward.enabled = NO;
	}	
	
}

- (IBAction) stopOrReLoadWeb: (id) sender
{
	// NOTE: stop is not implemented.  Only reload is implemented.
	/*
	 if ([_webView isLoading]){
	 [_webView stopLoading];
	 }else*/{
		 [_webView reload];
	 }
}

/*
 - (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
 {
	if (buttonIndex == 0){
 [[UIApplication sharedApplication] openURL:[[_webView request] URL]];
	}
 }
 */

- (IBAction) launchSafari: (id) sender{
	// Updated for iOS 9.0
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
																   message:nil
															preferredStyle:UIAlertControllerStyleActionSheet];
	
	UIAlertAction *openInSafari = [UIAlertAction actionWithTitle:@"Open in Safari" style:UIAlertActionStyleDefault
														 handler:^(UIAlertAction * action) {
															 // Launch Safari
															 [[UIApplication sharedApplication] openURL:[[_webView request] URL]];
														 }];
	UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
												   handler:^(UIAlertAction * action) {
													   [self dismissViewControllerAnimated:YES completion:nil];
												   }];
	[alert addAction:openInSafari];
	[alert addAction:cancel];
	[self presentViewController:alert animated:YES completion:nil];
}

- (void) showNoNetworkAlert
{
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Network"
																   message:@"A network connection is required.  Please verify your network settings and try again."
															preferredStyle:UIAlertControllerStyleAlert];
	[alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
											handler:^(UIAlertAction *action) {}]];
	[self presentViewController:alert animated:YES completion:nil];
}


#pragma mark UIWebView delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
	[_busyWebIcon startAnimating];
	
}

- (void)webViewDidFinishLoad:(UIWebView *)webView1
{
    // finished loading, hide the activity indicator
 	[_busyWebIcon stopAnimating];
	
	if (![webView1 canGoForward]){
		// disable the forward button
		_webForward.enabled = NO;
	}
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // load error, hide the activity indicator in the status bar
	[_busyWebIcon stopAnimating];
}

- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
	// Check for network connectivity
	if ([AFNetworkReachabilityManager sharedManager].reachable) {
		return YES;
	} else {
		[self showNoNetworkAlert];		
		return NO;
	}
}

-(IBAction)back: (id)sender
{
	//  If we go back past the first web page in the cache, then dismiss the web view
	
	if ([_webView canGoBack]){
		[_webView goBack];
	}else{	
		[_delegate dismissWebView];
	}
}

-(IBAction)done: (id)sender
{
	[_delegate dismissWebView];
}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return UIInterfaceOrientationMaskAll;
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//	[super preferredInterfaceOrientationForPresentation];
//	return UIInterfaceOrientationPortrait;
//}


@end
