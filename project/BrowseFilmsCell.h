//
//  BrowseFilmsCell.h
//  FestPro
//
//  Created by Christopher Hall on 1/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowseFilmsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *filmTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *filmDetailsLabel;
@property (nonatomic, strong) IBOutlet UIImageView *filmImageView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *cellSpinner;

@end
