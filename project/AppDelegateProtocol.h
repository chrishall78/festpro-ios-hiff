//
//  AppDelegateProtocol.h
//  FestPro
//
//  Created by Christopher Hall on 7/23/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDataObject;

@protocol AppDelegateProtocol

- (AppDataObject *) theAppDataObject;

@end
