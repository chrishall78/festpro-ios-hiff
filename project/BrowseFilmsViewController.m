//
//  BrowseFilmsViewController.m
//  FestPro
//
//  Created by Christopher Hall on 1/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "BrowseFilmsViewController.h"
#import "FPNavigationController.h"
#import "HomeViewController.h"
#import "FilmDetailViewController.h"
#import "Films.h"
#import "FestProAppDataObject.h"
#import "AppDelegateProtocol.h"
#import "CustomColoredAccessory.h"
#import "UIImageView+AFNetworking.h"

@implementation BrowseFilmsViewController

- (FestProAppDataObject *) theAppDataObject;
{
	id<AppDelegateProtocol> theDelegate = (id<AppDelegateProtocol>) [UIApplication sharedApplication].delegate;
	FestProAppDataObject *theDataObject;
	theDataObject = (FestProAppDataObject *) theDelegate.theAppDataObject;
	return theDataObject;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.fetchedResultsController.delegate = self;
	self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];

	if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPad) {
		UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeftTab:)];
		[swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
		[self.view addGestureRecognizer:swipeLeft];

		UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveRightTab:)];
		[swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
		[self.view addGestureRecognizer:swipeRight];
	}
    
    if (_filmProgram == nil) {
        _filmProgram = [[NSArray alloc] init];
    }
    if (_filmProgramName != nil) {
        self.title = @"Film Program";
        [_filterButton setEnabled:FALSE];
		self.navigationItem.leftBarButtonItem = nil;
    } else if (_filmSectionName != nil) {
		self.title = _filmSectionName;
        [_filterButton setEnabled:FALSE];
		self.navigationItem.leftBarButtonItem = nil;
	} else {
        [_filterButton setEnabled:TRUE];
    }
    
    [self addOrRemoveFilterView];
    _imageUrlBySection = [self setImageUrlBySection];
//	NSLog(@"Images: %@",_imageUrlBySection);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.fetchedResultsController = nil;
    NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	     */
	    //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    [self.tableView reloadData];
}

#pragma mark - View orientation

- (BOOL)shouldAutorotate {
	[super shouldAutorotate];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return YES;
	} else {
		return NO;
	}
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	[super supportedInterfaceOrientations];
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		return (UIInterfaceOrientationMaskPortrait |
				UIInterfaceOrientationMaskLandscapeLeft |
				UIInterfaceOrientationMaskLandscapeRight);
	} else {
		return UIInterfaceOrientationMaskPortrait;
	}
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
	[super preferredInterfaceOrientationForPresentation];
	return UIInterfaceOrientationPortrait;
}

#pragma mark - BrowseFilmsViewController Methods

- (void)addOrRemoveFilterView {
    FestProAppDataObject *theDataObject = [self theAppDataObject];
    if (![theDataObject.sectionFilterValue isEqualToString:@"ALL"] ||
        ![theDataObject.countryFilterValue isEqualToString:@"ALL"] ||
        ![theDataObject.languageFilterValue isEqualToString:@"ALL"] ||
        ![theDataObject.genreFilterValue isEqualToString: @"ALL"] ||
        ![theDataObject.eventTypeFilterValue isEqualToString:@"ALL"]) {
        
        NSString *filterValues = @"";
        if (![theDataObject.sectionFilterValue isEqualToString:@"ALL"]) {
            filterValues = [NSString stringWithFormat:@"%@, %@",filterValues,theDataObject.sectionFilterValue];
        }
        if (![theDataObject.countryFilterValue isEqualToString:@"ALL"]) {
            filterValues = [NSString stringWithFormat:@"%@, %@",filterValues,theDataObject.countryFilterValue];
        }
        if (![theDataObject.languageFilterValue isEqualToString:@"ALL"]) {
            filterValues = [NSString stringWithFormat:@"%@, %@",filterValues,theDataObject.languageFilterValue];
        }
        if (![theDataObject.genreFilterValue isEqualToString: @"ALL"]) {
            filterValues = [NSString stringWithFormat:@"%@, %@",filterValues,theDataObject.genreFilterValue];
        }
        if (![theDataObject.eventTypeFilterValue isEqualToString:@"ALL"]) {
            filterValues = [NSString stringWithFormat:@"%@, %@",filterValues,theDataObject.eventTypeFilterValue];
        }
        filterValues = [filterValues stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
        
        CGRect myRect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 22.0f);
        CGRect myLabelRect = CGRectMake(5.0f, 0.0f, self.view.frame.size.width-10.0f, 18.0f);
        UIView *headerView = [[UIView alloc] initWithFrame:myRect];
        [headerView setBackgroundColor:UI_COLOR_04];
        [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        [headerView autoresizesSubviews];
        UILabel * label1 = [[UILabel alloc] initWithFrame:myLabelRect];
        label1.backgroundColor = [UIColor clearColor];
        label1.textColor = [UIColor whiteColor];
        label1.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        label1.text = filterValues;
        [headerView addSubview:label1];
        [self.filterView addSubview:headerView];
    } else {
        CGRect myRect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 22.0f);
        CGRect myLabelRect = CGRectMake(5.0f, 0.0f, self.view.frame.size.width-10.0f, 18.0f);
        UIView *headerView = [[UIView alloc] initWithFrame:myRect];
        [headerView setBackgroundColor:UI_COLOR_04];
        [headerView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        [headerView autoresizesSubviews];
        UILabel * label1 = [[UILabel alloc] initWithFrame:myLabelRect];
        label1.backgroundColor = [UIColor clearColor];
        label1.textColor = [UIColor whiteColor];
        label1.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
		label1.text = @"All Films";
        [headerView addSubview:label1];
        [self.filterView addSubview:headerView];
    }
}

- (NSString *)processTitle:(NSString *)title {
    NSString *newTitle = title;
    if ([title length] >= 3) {
        if ([[title substringFromIndex:[title length] - 3] isEqualToString:@", A"]) {
            newTitle = [NSString stringWithFormat:@"%@%@",@"A ",[title substringToIndex:[title length] - 3]];
        }
    }
    if ([title length] >= 5) {
        if ([[title substringFromIndex:[title length] - 5] isEqualToString:@", THE"]) {
            newTitle = [NSString stringWithFormat:@"%@%@",@"THE ",[title substringToIndex:[title length] - 5]];
        }
    }
    return newTitle;
}

- (NSMutableArray *)setImageUrlBySection {
    NSMutableArray *results = [[NSMutableArray alloc] initWithArray:self.fetchedResultsController.fetchedObjects];
    
    _imageUrlList = [[NSMutableArray alloc] init];
    for (Films *item in results) {
		NSURLComponents *urlString = [[NSURLComponents alloc] initWithString: item.imageUrl];
		//[NSString stringWithFormat:@"%@%@%@", @"http://", urlString.percentEncodedHost, urlString.percentEncodedPath];
		
		if (urlString.URL) {
			[_imageUrlList addObject:urlString.URL];
		} else {
			NSString *encodedString = [item.imageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			NSURL *encodedURL = [NSURL URLWithString:encodedString];
			if (encodedURL) {
				[_imageUrlList addObject:encodedURL];
			} else {
				NSLog(@"'%@' is not a valid URL", item.imageUrl);
			}
		}
    }
	
    NSMutableArray *imageUrlSection = [[NSMutableArray alloc] init];
    NSInteger sections = [self numberOfSectionsInTableView:self.tableView];
    int count = 0;
    for (NSInteger x=0; x < sections; x++) {
        NSInteger rows = [self tableView:self.tableView numberOfRowsInSection:x];
        NSMutableArray *imageUrlByRow = [[NSMutableArray alloc] init];
        for (NSInteger y=0; y < rows; y++) {
            [imageUrlByRow addObject:[_imageUrlList objectAtIndex:count]];
            count++;
        }
        [imageUrlSection addObject:imageUrlByRow];
    }
    return imageUrlSection;
}

#pragma mark - Gesture actions

- (IBAction)moveRightTab:(id)sender
{
    NSUInteger selectedIndex = [self.tabBarController selectedIndex];
    [self.tabBarController setSelectedIndex:selectedIndex + 1];
}

- (IBAction)moveLeftTab:(id)sender
{
    NSUInteger selectedIndex = [self.tabBarController selectedIndex];
    [self.tabBarController setSelectedIndex:selectedIndex - 1];
}


#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"filmCell";
    
    BrowseFilmsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[BrowseFilmsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];

	// kill insets for iOS 8
	if ([[UIDevice currentDevice].systemVersion floatValue] >= 8) {
		cell.preservesSuperviewLayoutMargins = NO;
		[cell setLayoutMargins:UIEdgeInsetsZero];
	}
	// iOS 7 and earlier
	if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
		[cell setSeparatorInset:UIEdgeInsetsZero];
	}
	
    return cell;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
	[cell setBackgroundColor:UI_COLOR_05];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSInteger count = [[self.fetchedResultsController sections] count];    
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger numberOfRows = 0;
    
    if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }    
    return numberOfRows;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (_filmProgramName == nil) {
        return [self.fetchedResultsController sectionIndexTitles];
        //The array of strings to use as indices
    } else {
        return nil;
    }
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    if (_filmProgramName == nil) {
        return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
        //The index (integer) of the current section that the user is touching
    } else {
        return 0;
    }
}

#pragma mark - Table view delegate

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect myRect = CGRectMake(0.0f, 0.0f, 320.0f, 22.0f);
    CGRect myLabelRect = CGRectMake(10.0f, 0.0f, 300.0f, 22.0f);
    
    UIView *headerView = [[UIView alloc] initWithFrame:myRect];
    [headerView setBackgroundColor:UI_COLOR_02];
    
    UILabel * label1 = [[UILabel alloc] initWithFrame:myLabelRect];
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = UI_COLOR_05;
    label1.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    
    if (_filmProgramName == (id)[NSNull null] || _filmProgramName.length == 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        label1.text = [sectionInfo name];
    } else {
        label1.text = _filmProgramName;
    }
    [headerView addSubview:label1];
    
    return headerView;
}

- (UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSInteger sectionCount = [[self.fetchedResultsController sections] count];
    if (section != sectionCount-1) {
        return [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, 0.0)];
    } else {
        CGRect myRect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 22.0f);
        CGRect myLabelRect = CGRectMake(10.0f, 0.0f, self.view.frame.size.width-20.0f, 22.0f);
        
        UIView *footerView = [[UIView alloc] initWithFrame:myRect];
        UILabel * label1 = [[UILabel alloc] initWithFrame:myLabelRect];
        label1.backgroundColor = [UIColor clearColor];
        label1.textColor = [UIColor blackColor];
        label1.text = @"Films are unrated. Please use your own discretion.";
        label1.font = [UIFont fontWithName:@"Helvetica" size:13];
        
        [footerView setBackgroundColor:UI_COLOR_02];
        [footerView addSubview:label1];
        
        return footerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //NSInteger sectionCount = [[self.fetchedResultsController sections] count];
    //if (section != sectionCount-1) {
    return 0.0;
    //} else {
    //    return 20.0;
    //}
}

- (void)configureCell:(BrowseFilmsCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Films *currentCell = [self.fetchedResultsController objectAtIndexPath:indexPath];

    // Process film title to include A or THE at the front
    NSString *rawTitle = [currentCell titleEnglish];
    NSString *newTitle = [self processTitle:rawTitle];
    NSString *origTitle = [currentCell titleRomanized];
    
    if ([origTitle isEqualToString:@""]) {
        cell.filmTitleLabel.text = newTitle;
    } else {
        cell.filmTitleLabel.text = [NSString stringWithFormat:@"%@ (%@)", newTitle, origTitle];
    }
    cell.filmDetailsLabel.text = [NSString stringWithFormat:@"%@ %@ | %@M", [currentCell countries], [currentCell year], [currentCell runtime]];
    
    if (_filmProgramName == nil) {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        cell.accessoryView = nil;
    } else {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        CustomColoredAccessory *accessory = [CustomColoredAccessory accessoryWithColor:UI_COLOR_01];
        accessory.highlightedColor = UI_COLOR_04;
        cell.accessoryView = accessory;
    }
	
	//set placeholder image or cell won't update when image is loaded
	[cell.filmImageView setImage:[UIImage imageNamed:@"default-photo.png"]];
	
	//load the image
	if ([_imageUrlBySection count] > 0) {
		[cell.filmImageView setImageWithURL: [[_imageUrlBySection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
	}
	//NSLog(@"Image: %@", [_imageUrlBySection objectAtIndex:indexPath.section]);
}



#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    FestProAppDataObject *theDataObject = [self theAppDataObject];
    
    
    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Films" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // If this is a film program, add a Predicate to filter the film results.
    if ([_filmProgram count] > 1) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"film_id IN(%@)", _filmProgram];
        [fetchRequest setPredicate:predicate];
	} else if (![_filmSectionName isEqualToString:@""] && _filmSectionName != nil) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"section == %@", _filmSectionName];
        [fetchRequest setPredicate:predicate];
    } else {
        // Do additional filtering here, combine all predicates into a compound predicate joined by 'AND'.
        NSMutableArray *predicateArray = [NSMutableArray array];
        if (![theDataObject.sectionFilterValue isEqualToString:@"ALL"] ) {
            NSPredicate *sectionPredicate = [NSPredicate predicateWithFormat:@"%K CONTAINS %@", @"section", theDataObject.sectionFilterValue];
            [predicateArray addObject:sectionPredicate];
        }
        if (![theDataObject.countryFilterValue isEqualToString:@"ALL"] ) {
            NSPredicate *countryPredicate = [NSPredicate predicateWithFormat:@"%K CONTAINS %@", @"countries", theDataObject.countryFilterValue];
            [predicateArray addObject:countryPredicate];
        }
        if (![theDataObject.languageFilterValue isEqualToString:@"ALL"] ) {
            NSPredicate *languagePredicate = [NSPredicate predicateWithFormat:@"%K CONTAINS %@", @"languages", theDataObject.languageFilterValue];
            [predicateArray addObject:languagePredicate];
        }
        if (![theDataObject.genreFilterValue isEqualToString:@"ALL"] ) {
            NSPredicate *genrePredicate = [NSPredicate predicateWithFormat:@"%K CONTAINS %@", @"genres", theDataObject.genreFilterValue];
            [predicateArray addObject:genrePredicate];
        }
        if (![theDataObject.eventTypeFilterValue isEqualToString:@"ALL"] ) {
            NSPredicate *eventTypePredicate = [NSPredicate predicateWithFormat:@"%K CONTAINS %@", @"eventType", theDataObject.eventTypeFilterValue];
            [predicateArray addObject:eventTypePredicate];
        }
        
        if ([predicateArray count] > 0) {
            NSPredicate *cpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
            [fetchRequest setPredicate:cpred];
        }
    }
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"titleEnglish" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    if ([_filmProgram count] > 1) {
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
	} else if (![_filmSectionName isEqualToString:@""] && _filmSectionName != nil) {
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"titleAlpha" cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
    } else {
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"titleAlpha" cacheName:nil];        
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
    }
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    /*
	     Replace this implementation with code to handle the error appropriately.
         
	     abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	     */
	    //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    //abort();
	}
    
    return _fetchedResultsController;
}   

#pragma mark - Fetched results controller delegate methods

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
			
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
			
		default:
			break;
    }
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(BrowseFilmsCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}


#pragma mark - FilterMenuViewControllerDelegate

- (void)filterMenuViewControllerDidCancel: (FilterMenuViewController *)controller {
	[self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)filterMenuViewControllerDidSave: (FilterMenuViewController *)controller {    
    self.fetchedResultsController = nil;
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        //NSLog(@"Error in refetch: %@",[error localizedDescription]);
        //abort();
    }
    _imageUrlBySection = [self setImageUrlBySection];
    
    [self.tableView reloadData];
    [self addOrRemoveFilterView];    
	[self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"FilterMenu"])
	{
		FPNavigationController *navigationController = [segue destinationViewController];
		FilterMenuViewController *filterMenuViewController = [[navigationController viewControllers] objectAtIndex:0];
		filterMenuViewController.delegate = self;
	}
    
    if ([[segue identifier] isEqualToString:@"FilmDetail"]) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            FPNavigationController *navigationController = [segue destinationViewController];
            FilmDetailViewController *filmDetail = [[navigationController viewControllers] objectAtIndex:0];
            filmDetail.managedObjectContext = _managedObjectContext;
            filmDetail.currentFilm = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];

			HomeViewController *homeViewController;
			if ([[self.splitViewController.viewControllers lastObject] isKindOfClass:[UINavigationController class]]) {
				homeViewController = (HomeViewController *) ((UINavigationController *)[self.splitViewController.viewControllers lastObject]).topViewController;
			} else {
				homeViewController = [self.splitViewController.viewControllers lastObject];
			}
			if (homeViewController.masterPopoverController != nil) {
				[homeViewController.masterPopoverController dismissPopoverAnimated:YES];
			}
			if ([filmDetail conformsToProtocol:@protocol(UISplitViewControllerDelegate)]) {
                self.splitViewController.delegate = filmDetail;
				filmDetail.popoverButton = homeViewController.popoverButton;
				filmDetail.masterPopoverController = homeViewController.masterPopoverController;
            } else {
                self.splitViewController.delegate = nil;
            }
        } else {
            FilmDetailViewController *filmDetail = (FilmDetailViewController *)[segue destinationViewController];
            filmDetail.managedObjectContext = _managedObjectContext;
            filmDetail.currentFilm = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
        }
    }

	if ([segue.identifier isEqualToString:@"CheckForUpdates"])
	{
		FPNavigationController *navigationController = [segue destinationViewController];
		HomeViewController *homeViewController = [[navigationController viewControllers] objectAtIndex:0];
		homeViewController.managedObjectContext = _managedObjectContext;
		if ([homeViewController conformsToProtocol:@protocol(UISplitViewControllerDelegate)]) {
			self.splitViewController.delegate = homeViewController;
		} else {
			self.splitViewController.delegate = nil;
		}

		[homeViewController tryCheckingForUpdates:sender];
//		[homeViewController tryDownloadingData:sender];
	}

}

@end
