//
//  Screenings.m
//  FestPro
//
//  Created by Christopher Hall on 2/18/12.
//  Copyright (c) 2012 FestPro LLC. All rights reserved.
//

#import "Screenings.h"
#import "Films.h"

@implementation Screenings

@dynamic dateString;
@dynamic dateTime;
@dynamic film_id;
@dynamic film_ids;
@dynamic films;
@dynamic free;
@dynamic location;
@dynamic private;
@dynamic programName;
@dynamic rush;
@dynamic screening_id;
@dynamic ticketLink;
@dynamic totalRuntime;
@dynamic screenings_films;

@end
